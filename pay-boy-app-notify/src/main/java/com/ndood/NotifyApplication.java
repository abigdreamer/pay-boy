package com.ndood;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * polling启动类
 * @author ndood
 */
@SpringBootApplication
@EnableCaching
@EnableTransactionManagement
public class NotifyApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(NotifyApplication.class, args);
	}
	
}