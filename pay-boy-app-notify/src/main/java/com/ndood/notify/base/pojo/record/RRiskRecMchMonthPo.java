package com.ndood.notify.base.pojo.record;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_risk_rec_mch_month")
public class RRiskRecMchMonthPo extends Model<RRiskRecMchMonthPo> {

    private static final long serialVersionUID = 1L;

    private String recordNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 月份代码 201801
     */
    private String month;

    /**
     * 本月提现额度
     */
    private BigDecimal withdrawAmount;

    /**
     * 本月提现笔数
     */
    private Integer withdrawCount;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public Integer getWithdrawCount() {
        return withdrawCount;
    }

    public void setWithdrawCount(Integer withdrawCount) {
        this.withdrawCount = withdrawCount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.recordNo;
    }

    @Override
    public String toString() {
        return "RRiskRecMchMonthPo{" +
        ", recordNo=" + recordNo +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", month=" + month +
        ", withdrawAmount=" + withdrawAmount +
        ", withdrawCount=" + withdrawCount +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
