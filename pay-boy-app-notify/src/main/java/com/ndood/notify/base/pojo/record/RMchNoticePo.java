package com.ndood.notify.base.pojo.record;

import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_mch_notice")
public class RMchNoticePo extends Model<RMchNoticePo> {

    private static final long serialVersionUID = 1L;

    private String recordNo;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 通知类别：1 系统消息 2 推送消息
     */
    private Integer type;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 状态：0 未读 1 已读
     */
    private Integer status;

    /**
     * 逻辑删除：0 未删除 1 已删除
     */
    private Integer logicDelete;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Integer logicDelete) {
        this.logicDelete = logicDelete;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.recordNo;
    }

    @Override
    public String toString() {
        return "RMchNoticePo{" +
        ", recordNo=" + recordNo +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", type=" + type +
        ", title=" + title +
        ", content=" + content +
        ", status=" + status +
        ", logicDelete=" + logicDelete +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
