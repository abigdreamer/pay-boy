package com.ndood.notify.base.dao.record;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ndood.notify.base.pojo.record.RMchRecCheckPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
public interface RMchRecCheckMapper extends BaseMapper<RMchRecCheckPo> {

}
