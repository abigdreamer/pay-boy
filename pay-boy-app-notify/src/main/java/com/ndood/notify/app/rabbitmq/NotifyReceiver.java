package com.ndood.notify.app.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "notify")
public class NotifyReceiver {
 
    @RabbitHandler
    public void process(String msg) {
        System.out.println("Receiver  : " + msg);
    }
 
}
