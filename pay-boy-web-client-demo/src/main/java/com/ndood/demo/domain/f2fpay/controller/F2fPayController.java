package com.ndood.demo.domain.f2fpay.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *  模拟当面付跳转
 */
@Controller
public class F2fPayController {

	/**
	 * 跳转到当面付主页
	 */
	@RequestMapping("/f2fpay/index")
	public String toIndex() {
		return "f2fpay/index";
	}
	
}
