package com.ndood.demo.app.properties;

/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "ndood.demo")
@Getter @Setter
public class DemoProperties {
	
	private String domain = "127.0.0.1:84";
	
	private Boolean isDevelop = true;
}