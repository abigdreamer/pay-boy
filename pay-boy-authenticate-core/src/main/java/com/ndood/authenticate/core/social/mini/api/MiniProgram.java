package com.ndood.authenticate.core.social.mini.api;
/**
 * 微信API调用接口
 */
public interface MiniProgram {

	MiniUserInfo getUserInfo(String openId);
	
}