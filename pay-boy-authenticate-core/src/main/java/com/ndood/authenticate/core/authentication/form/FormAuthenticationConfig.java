package com.ndood.authenticate.core.authentication.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.ndood.authenticate.core.properties.SecurityProperties;

/**
 * 表单登录配置
 */
@Component
public class FormAuthenticationConfig {

	@Autowired
	protected AuthenticationSuccessHandler authenticationSuccessHandler;
	
	@Autowired
	protected AuthenticationFailureHandler authenticationFailureHandler;
	
	@Autowired
	private SecurityProperties securityProperties;
	
	public void configure(HttpSecurity http) throws Exception {
		http.formLogin()
			.loginPage(securityProperties.getLoginPage()) // 自定义登录跳转
			.loginProcessingUrl(securityProperties.getLoginProcessUrl()) // 自定义登录验证
			.successHandler(authenticationSuccessHandler)
			.failureHandler(authenticationFailureHandler);
	}
	
}