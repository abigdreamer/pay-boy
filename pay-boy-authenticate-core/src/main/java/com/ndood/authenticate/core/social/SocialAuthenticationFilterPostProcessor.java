package com.ndood.authenticate.core.social;

import org.springframework.social.security.SocialAuthenticationFilter;

/**
 * 授权码模式-使社交登录成功后app和浏览器做出不同的反应
 * SocialAuthenticationFilter后处理器，在浏览器环境下为空。在app环境下实现
 */
public interface SocialAuthenticationFilterPostProcessor {
	
	/**
	 * @param socialAuthenticationFilter
	 */
	void process(SocialAuthenticationFilter socialAuthenticationFilter);

}