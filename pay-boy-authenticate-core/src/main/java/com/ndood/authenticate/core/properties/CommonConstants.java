package com.ndood.authenticate.core.properties;

/**
 * Payment常量
 * @author ndood
 */
public interface CommonConstants {
	/**
	 * 返回码
	 */
	String SUCCESS = "10000";
	String FAILUE = "-1";
	
	/**
	 * 验证码前缀
	 */
	String SESSION_KEY_PREFIX = "SESSION_KEY_";
	
	/**
	 * 记住我
	 */
	String REMEMBER_ME_KEY = "BoSk70Yer38~veg91DoCKs=sLaIn!metE55bURgs71rug;ILEa=Ikon79sept+ree$Fuel99baKER;wOe43JackS=TinS79babA73taLmibs10bIsE*";
	
}
