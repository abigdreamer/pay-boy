package com.ndood.authenticate.core.properties;

/**
 * Oauth2相关配置
 */
public class Oauth2Properties {
	private Oauth2ClientProperties[] clients = {};
	
	private String jwtSignKey = "ndood_default_key";
	
	private String storeType = "";

	public Oauth2ClientProperties[] getClients() {
		return clients;
	}

	public void setClients(Oauth2ClientProperties[] clients) {
		this.clients = clients;
	}

	public String getJwtSignKey() {
		return jwtSignKey;
	}

	public void setJwtSignKey(String jwtSignKey) {
		this.jwtSignKey = jwtSignKey;
	}

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	} 
}
