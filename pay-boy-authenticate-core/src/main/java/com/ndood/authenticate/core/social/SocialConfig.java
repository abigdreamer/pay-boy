package com.ndood.authenticate.core.social;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.security.AuthenticationNameUserIdSource;

/**
 * 添加Social配置类
 * 社交登录拦截器：SocialAuthenticationFilter 登录的时候先经过这个拦截器，如果没code跳转到微信授权页，如果有code则跳转到qq链接进行登录后处理。
 * SocialAuthenticationFilter 
 * -> SocialAuthenticationService(Oauth2SocialAuthenticationService) 
 * -> ConnectionFactory 
 * -> Authentication(SocialAuthenticationToken) 
 * -> AuthenticationManager(ProviderManager)
 * -> AuthenticationProvider(SocialAuthenticationProvider)
 * -> UserConnectionRepository(JdbcUserConnectionRepository)
 * -> SocialUserDetailsService
 * -> SocialUserDetails
 */
/**
 * https://blog.csdn.net/newhanzhe/article/details/81276375
 * 解决社交登录的时候 在这里SocialConfig中明明配置了Jdbc的，但是上图中总是走InMemoryUsersConnectionRepository的问题
 */
@Order(Integer.MIN_VALUE)
@Configuration
@EnableSocial
public class SocialConfig extends SocialConfigurerAdapter{
	
	@Autowired
	private DataSource dataSource;
	
	/**
	 * UsersConnectionRepository包附近
	 * 建表 /org/springframework/social/connect/jdbc/JdbcUsersConnectionRepository.sql
	 */
	
	@Autowired(required = false)
	private ConnectionSignUp connectionSignUp;
	
	/**
	 * 获取用户Social持久化对象，并制定加密规则
	 */
	@Override
	public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
		// TODO 修改加解密工具
		JdbcUsersConnectionRepository repository = new JdbcUsersConnectionRepository(dataSource, connectionFactoryLocator, Encryptors.noOpText());
		repository.setTablePrefix("auth_");
		
		// 使第一次社交登陆时，默默注册一个用户，而不跳转到注册页
		// imooc_userconnection会使用displayName作为Id插一条记录
		if(connectionSignUp!=null){
			repository.setConnectionSignUp(connectionSignUp);
		}
		return repository;
	}
	
	/**
	 * 声明社交登录注册工具类
	 */
	@Bean
	public ProviderSignInUtils providerSignInUtils(ConnectionFactoryLocator connectionFactoryLocator){
		return new ProviderSignInUtils(connectionFactoryLocator,getUsersConnectionRepository(connectionFactoryLocator));
	}

	/**
	 * https://www.programcreek.com/java-api-examples/index.php?api=org.springframework.social.UserIdSource
	 */
	@Override
	public UserIdSource getUserIdSource() {
		return new AuthenticationNameUserIdSource();
	}
}