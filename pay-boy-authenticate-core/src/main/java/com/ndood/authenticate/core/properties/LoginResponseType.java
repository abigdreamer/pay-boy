package com.ndood.authenticate.core.properties;
/**
 * 自定义登录类型类
 * @author ndood
 *
 */
public enum LoginResponseType {
	
	REDIRECT,
	
	JSON
}