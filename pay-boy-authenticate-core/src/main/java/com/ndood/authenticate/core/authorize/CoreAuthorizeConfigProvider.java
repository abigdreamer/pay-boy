package com.ndood.authenticate.core.authorize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

import com.ndood.authenticate.core.properties.SecurityProperties;

/**
 * 实现AuthorizeConfigProvider
 * 确保基本配置在最开始生效
 */
@Component
@Order(Integer.MIN_VALUE)
public class CoreAuthorizeConfigProvider implements AuthorizeConfigProvider{

	@Autowired
	private SecurityProperties securityProperties;
	
	/**
	 * 抽取BrowserSecurityConfig ImoocResourceServerConfig的通用授权配置
	 */
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		//config.antMatchers("*/**").permitAll();
		
		config.antMatchers(
				"/authentication/require",
				"/authentication/mobile",
				"/authentication/openid",
				securityProperties.getSocial().getFilterProcessesUrl()+"/*",
				securityProperties.getBrowser().getSignInPage(),
				"/code/*",
				securityProperties.getBrowser().getSignUpPage(),
				securityProperties.getBrowser().getSession().getSessionInvalidUrl())
			.permitAll();
	}
}
