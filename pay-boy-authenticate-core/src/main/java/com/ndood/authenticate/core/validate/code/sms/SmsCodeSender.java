package com.ndood.authenticate.core.validate.code.sms;
/**
 * 模拟短信验证码发送接口
 * @author ndood
 */
public interface SmsCodeSender {

	void send(String mobile, String code);
}