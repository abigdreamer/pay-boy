package com.ndood.rpc.product.entity.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RpcProductQuery implements Serializable{
	private static final long serialVersionUID = 996862021205502582L;

	private Integer id;
	
	private String keywords;
	
	private Integer offset;
	
	private Integer pageSize;
	
	public Integer getPageNo() {
		return offset / pageSize;
	}
}
