package com.ndood.gateway.base.dao.account_center;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.gateway.domain.account_center.entity.dto.AccountCenterAccountDto;

public interface ExtendAccountCenterAccountDao {

	/**
	 * 1 根据手机查找收银员信息
	 */
	@Select("SELECT `id`,`agent_id`,`mch_id`,`status`,`email`,`email_status`,`head_img_url`,`mobile`,`nick_name`,`password`,`trade_password`,`is_cashier`,`shop_id` "
			+ "FROM m_staff WHERE `is_cashier` = 1 AND shop_id IS NOT NULL AND email = #{email} AND email_status = 1 AND status = 1")
	AccountCenterAccountDto extendFindAccountByEmail(@Param("email") String email);

	/**
	 * 2  根据邮箱查找收银员信息
	 */
	@Select("SELECT `id`,`agent_id`,`mch_id`,`status`,`email`,`email_status`,`head_img_url`,`mobile`,`nick_name`,`password`,`trade_password`,`is_cashier`,`shop_id` "
			+ "FROM m_staff WHERE `is_cashier` = 1 AND shop_id IS NOT NULL AND mobile = #{mobile} AND status = 1")
	AccountCenterAccountDto extendFindAccountByMobile(@Param("mobile") String mobile);

	/**
	 * 3 根据ID查找收银员
	 */
	@Select("SELECT `id`,`agent_id`,`mch_id`,`status`,`email`,`email_status`,`head_img_url`,`mobile`,`nick_name`,`password`,`trade_password`,`is_cashier`,`shop_id` "
			+ "FROM m_staff WHERE `is_cashier` = 1 AND shop_id IS NOT NULL AND id = #{id} AND status = 1")
	AccountCenterAccountDto extendFindAccountById(@Param("id") Integer id);

}
