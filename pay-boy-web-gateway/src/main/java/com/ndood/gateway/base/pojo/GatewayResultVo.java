package com.ndood.gateway.base.pojo;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.ndood.api.base.constaints.ApiCode;

/**
 * 通用DTO对象
 */
public class GatewayResultVo implements Serializable{
	private static final long serialVersionUID = -4774146968082169416L;

	/**
	 * 请求成功
	 */
	public final static GatewayResultVo ok() {
		return new GatewayResultVo(ApiCode.SUCCESS);
	}
	
	public final static GatewayResultVo error() {
		return new GatewayResultVo(ApiCode.ERR_SERVER);
	}
	
	private ApiCode code;
	private String msg;
	private String error;
	private String next;
	private Object data;
	private String encryptAesKey;
	private String encryptData;
	
	private GatewayResultVo(ApiCode code) {
		this.code = code;
		this.msg = code.getValue();
	}

	@JSONField(name = "code")
	public int getCode() {
		return code.getCode();
	}
	
	@JSONField(name = "code")
	public GatewayResultVo setCode(int code) {
		this.code = ApiCode.getEnum(code);
		return this;
	}

	public String getEncryptAesKey() {
		return encryptAesKey;
	}

	public GatewayResultVo setEncryptAesKey(String encryptAesKey) {
		this.encryptAesKey = encryptAesKey;
		return this;
	}

	public String getEncryptData() {
		return encryptData;
	}

	public GatewayResultVo setEncryptData(String encryptData) {
		this.encryptData = encryptData;
		return this;
	}

	public String getMsg() {
		return msg;
	}
	
	public GatewayResultVo setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public Object getData() {
		return data;
	}

	public GatewayResultVo setData(Object data) {
		this.data = data;
		return this;
	}

	public String getError() {
		return error;
	}

	public GatewayResultVo setError(String error) {
		this.error = error;
		return this;
	}
	
	public String getNext() {
		return next;
	}
	
	public GatewayResultVo setNext(String next) {
		this.next = next;
		return this;
	}

	public String toContent() {
		final StringBuilder sb = new StringBuilder("PaymentResult{");
        sb.append("code='").append(code.getCode()).append('\'');
        sb.append(", msg='").append(msg).append('\'');
        sb.append(", error='").append(error).append('\'');
        sb.append(", data=").append(JSON.toJSON(data));
        sb.append(", next='").append(next).append('\'');
        sb.append('}');
        return sb.toString();
	}
	
}
