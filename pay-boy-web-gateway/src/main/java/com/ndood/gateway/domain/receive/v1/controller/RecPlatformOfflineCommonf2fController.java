package com.ndood.gateway.domain.receive.v1.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndood.api.base.constaints.ApiCode;
import com.ndood.api.base.exception.ApiException;
import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecOfflineOrderDto;
import com.ndood.api.domain.router.receive.v1.service.RouteRecPlatformOfflineCommonf2fService;
import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecChannelDto;
import com.ndood.api.domain.support.staff.entity.dto.SupportStaffDto;
import com.ndood.api.domain.support.staff.service.SupportStaffService;
import com.ndood.common.base.constaints.CommonConstaints;
import com.ndood.gateway.base.pojo.GatewayResultVo;
import com.ndood.gateway.base.utils.HttpUtil;
import com.ndood.gateway.domain.receive.v1.service.RecPlatformOfflineOfficialAlif2fService;

/**
 * 收款功能v1-平台收款-支付宝当面付
 */
@RestController
@RequestMapping("/receive/v1/platform/offline/comm")
public class RecPlatformOfflineCommonf2fController {

	@Autowired
	private RouteRecPlatformOfflineCommonf2fService routeRecPlatformCommonf2fService;
	
	@Autowired
	private RecPlatformOfflineOfficialAlif2fService recPlatformOfficialAlif2fService;
	
	
	
	@Autowired
	private SupportStaffService supportStaffService;
	
	/**
	 * 1 扫码支付创建预支付订单
	 */
	@RequestMapping("/bar_code")
	public GatewayResultVo preCreate(String amount, String payRemark, String username, String deviceNo, String barCode, HttpServletRequest request) throws ApiException {

		// Step1: 参数校验
		Assert.notNull(username, "收银员账号不能为空！");
		Assert.notNull(amount, "金额不能为空！");
		Assert.notNull(deviceNo, "设备编码不能为空");
		Assert.notNull(barCode, "支付授权码不能为空！");
		
		String ip = HttpUtil.getIpAddress(request);

		// Step1: 获取支付信息，包含商户信息，支付信息，门店信息
		SupportStaffDto staff = supportStaffService.getStaffFullPayInfo(username);
		Assert.notNull(staff, "员工信息不存在!");
		
		// Step2: 渠道路由
		SupportMchRecChannelDto channel = routeRecPlatformCommonf2fService.routeBarCode(amount, barCode, staff);

		// Step3: 路由跳转
		if(CommonConstaints.CN_GF_ALI_BAR.equals(channel.getChannelNo())){
			RecoRecOfflineOrderDto order = recPlatformOfficialAlif2fService.barCode(amount, barCode, payRemark, deviceNo, ip, staff, channel);
			return GatewayResultVo.ok().setData(order);
		}

		throw new ApiException(ApiCode.ERR_SERVER, "不支持的支付渠道->" + channel.getChannelNo());

	}

}
