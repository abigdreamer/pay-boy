package com.ndood.gateway.domain.account_center.entity.dto;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.ndood.gateway.domain.account_center.entity.AccountCenterAccountDo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountCenterAccountDto extends AccountCenterAccountDo implements UserDetails{

	private static final long serialVersionUID = -6657686675742954790L;

    /**
     * 代理商ID
     */
    private Integer agentId;

    private String agentName;
    
    /**
     * 商户ID
     */
    private Integer mchId;
    
    private String mchName;

    /**
     * 门店ID
     */
    private Integer shopId;
    
    private String shopName;
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getUsername() {
		return String.valueOf(super.getId());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
