package com.ndood.gateway.app.rpc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImplExporter;

/**
 * JsonRpc服务端配置
 */
@Configuration
public class RpcServerConfiguration {

	@Bean
	public AutoJsonRpcServiceImplExporter rpcServiceImplExporter() {
		return new AutoJsonRpcServiceImplExporter();
	}
	
}
