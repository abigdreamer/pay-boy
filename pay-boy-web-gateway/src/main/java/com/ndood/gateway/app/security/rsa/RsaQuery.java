package com.ndood.gateway.app.security.rsa;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
@Getter @Setter
public abstract class RsaQuery implements Serializable{
	private static final long serialVersionUID = 1026057943682355268L;
	/**
	 * 加密的aesKey，需要通过rsa解密得到aesKey
	 */
	private String encryptAesKey;
	/**
	 * 加密后的Data，需要通过aes解密得到Data，并作RSA验签
	 */
	private String encryptData;
	/**
	 * 解密后使用反射设置到requestBody字段属性中
	 */
	public abstract void setAttrs(Map<String,Object> map);
}
