package com.ndood.gateway.app.mongo;

import java.util.Date;

import org.springframework.data.annotation.Id;

/**
 * https://blog.csdn.net/ln152315/article/details/78608499
 */
public class MongoLog {
	@Id
	private String id;
	private Date ts;
	private String level;
	private String msg;
	private String thread;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date date) {
		this.ts = date;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getThread() {
		return thread;
	}

	public void setThread(String thread) {
		this.thread = thread;
	}
}
