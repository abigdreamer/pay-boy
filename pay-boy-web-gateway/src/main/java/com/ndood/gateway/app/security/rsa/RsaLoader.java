package com.ndood.gateway.app.security.rsa;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * 根据authId获取对应的RSA公钥
 * TODO 从数据库中读取publicKeys，并设置到springCache
 */
@Service
public class RsaLoader {
    
	/**
	 * 模拟平台公钥
	 */
	private static Map<String,String> PUBLIC_KEYS = new HashMap<>();
	/**
	 * 模拟平台私钥
	 */
	private static Map<String,String> PRIVATE_KEYS = new HashMap<>();
	/**
	 * 模拟第三方公钥
	 */
	private static Map<String,String> THIRD_PUBLIC_KEYS = new HashMap<>();
    
    static {
        PUBLIC_KEYS.put("1000","MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDN3F0n9qwMQsOzbJxlJwMApXNM" + 
    			"R+N+LMpBxcDyKeiBalVAFzrU4q1HsTJIisJOBbL0WHlp2X/eeBFHJstVQCk+qddj" + 
    			"R1gKXiJfpU9Q/WnsvQPE4MBK64GG8T1S3Ri57EkR9TE5DwE0zzl+z95Emi26j1Nm" + 
    			"Pc3I9kYqfr/dCoj9mwIDAQAB");
    }
    
    static {
        PRIVATE_KEYS.put("1000","MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAM3cXSf2rAxCw7Ns" + 
    			"nGUnAwClc0xH434sykHFwPIp6IFqVUAXOtTirUexMkiKwk4FsvRYeWnZf954EUcm" + 
    			"y1VAKT6p12NHWApeIl+lT1D9aey9A8TgwErrgYbxPVLdGLnsSRH1MTkPATTPOX7P" + 
    			"3kSaLbqPU2Y9zcj2Rip+v90KiP2bAgMBAAECgYBO/kxQ1XrXiZcG9ppcxkeEq/g+" + 
    			"QSeudwl1i6iqRCKP5nmoCkHtBr5vUDN3WqeMwOsWkAym7Wr/txsKLny/zcsFLFBT" + 
    			"8CNIKfU53SjVxES4yX5fROsHd5zJFTMWprcGgjOKMmih8pPT3nB8wwcSWuvQYuTY" + 
    			"3IIr830hCq9tgHvSoQJBAPBwBvR3MtjTRV47kuSkZGd+NlsAPHxUXEIZGK/MyTVn" + 
    			"lVzNoDbVpStTAFqPm5WDlBf4GdDHvCcGqJ8fI0KRNDUCQQDbL2kF99hSjx1ZFpAn" + 
    			"kWbMwV12nr6OuyWIicglQgh7VDAa8iaDf1X1icj6lQIsI9NU0H16eIGdhO1+3/XX" + 
    			"rASPAkARGocEIO6XCgBnQamjZiZWTl4jfxLObVnawdpFtzWg/OtdHKuG+w+y00a1" + 
    			"Kn4Q1rlUMyvy9CJoTEr2dsqVU6r5AkA25VLqeb6mPs3c6DfGkTYsBioAcZXMbbbi" + 
    			"0Y9dNYKmUNmThh57RMMkshOyHuviXj6puWYT7GaaKxbIdhM+pwilAkEA3ODIk6wn" + 
    			"Flz5IcO7auXJCtiujXceU3t57fUPTRyDwJ7RiNFNrHnNZqih1ZUShDVbwHFbOKO2" + 
    			"56/Xt2X3oPX+dQ==");
    }
    
    static {
        THIRD_PUBLIC_KEYS.put("1000","MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDN3F0n9qwMQsOzbJxlJwMApXNM" + 
    			"R+N+LMpBxcDyKeiBalVAFzrU4q1HsTJIisJOBbL0WHlp2X/eeBFHJstVQCk+qddj" + 
    			"R1gKXiJfpU9Q/WnsvQPE4MBK64GG8T1S3Ri57EkR9TE5DwE0zzl+z95Emi26j1Nm" + 
    			"Pc3I9kYqfr/dCoj9mwIDAQAB");
    }
    

    /**
     * 根据授权编号获取公钥
     */
    public RsaKeyPair getKeyPairByAuthId(String authId){
    	RsaKeyPair keyPair = new RsaKeyPair();
    	keyPair.setPublicKey(PUBLIC_KEYS.get(authId));
    	keyPair.setPrivateKey(PRIVATE_KEYS.get(authId));
    	keyPair.setThirdPublicKey(THIRD_PUBLIC_KEYS.get(authId));
    	return keyPair;
    }
    
}