package com.ndood.gateway.app.security.rsa;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RsaKeyPair {
	/**
	 * 平台方公钥
	 */
	private String publicKey;
	/**
	 * 平台方私钥
	 */
	private String privateKey;
	/**
	 * 第三方公钥
	 */
	private String thirdPublicKey;
}
