package com.ndood.gateway.app.security.authorize;

import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

import com.ndood.authenticate.core.authorize.AuthorizeConfigProvider;

/**
 * 实现AuthorizeConfigProvider
 * 确保基本配置在最开始生效
 */
@Component
@Order(Integer.MAX_VALUE)
public class ApiAuthorizeConfigProvider implements AuthorizeConfigProvider{
	
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		config.antMatchers("/receive/v1/platform/offline/**").permitAll();
		config.antMatchers("/authentication/form**").permitAll();
		config.antMatchers("/account_center").permitAll();
		config.antMatchers("/websocket").permitAll();
		config.anyRequest().authenticated();
	}
}
