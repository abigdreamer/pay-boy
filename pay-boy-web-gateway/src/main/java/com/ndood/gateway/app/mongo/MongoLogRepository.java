package com.ndood.gateway.app.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoLogRepository extends MongoRepository<MongoLog, String> {
	
}