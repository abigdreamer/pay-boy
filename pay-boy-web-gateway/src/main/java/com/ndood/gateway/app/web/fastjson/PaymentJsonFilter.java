package com.ndood.gateway.app.web.fastjson;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.serializer.BeanContext;
import com.alibaba.fastjson.serializer.ContextValueFilter;

@Component
public class PaymentJsonFilter implements ContextValueFilter {

	@Override
	public Object process(BeanContext context, Object object, String name, Object value) {

		if (context == null) {
			return value;
		}

		/**
		 * 将其它类型序列化成String
		 */
		StringField stringField = context.getAnnation(StringField.class);
		if (stringField != null) {
			return value == null ? "0" : value.toString();
		}

		/**
		 * 将Date类型序列化成timestamp字符串
		 */
		TimeField timeField = context.getAnnation(TimeField.class);
		if (timeField != null) {
			long timestamp = value == null ? new Date().getTime() : ((Date) value).getTime();
			return String.valueOf(timestamp / 1000);
		}

		return value;

	}

}