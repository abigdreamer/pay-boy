package com.ndood.gateway.app.init;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.ndood.api.app.properties.ApiProperties;

/**
 * 初始化的时候配置默认的sessionid名称
 */
@Component
public class CookieNameInitializer {
	
	@Autowired
	private ApiProperties apiProperties;
	
	@Bean
	public ServletContextInitializer servletContextInitializer() {
	    return new ServletContextInitializer() {
	        @Override
	        public void onStartup(ServletContext servletContext) throws ServletException {
	            servletContext.getSessionCookieConfig().setName(apiProperties.getDefaultCookieName());
	        }
	    };
	}
}

