package com.ndood;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * 支付测试1： http://localhost:83/receive/v1/platform/offline/alif2f/pre_create
 * 2 查看预支付日志，复制返回的qrcode，用草料二维码生成器生成二维码，并使用沙箱版支付宝进行扫描
 * 3 查看通知日志
 */
/**
 * 模拟收银员登陆：1 POST http://127.0.0.1:83/authentication/form
 * HEADERS 
 * Authorization Basic cGF5LWJveS1wYXNzd29yZDpwYXktYm95LXBhc3N3b3JkLXNlY3JldA==
 * Content-Type application/x-www-form-urlencoded
 * BODY
 * username 13333333333
 * password 123456
 * 
 */
/**
 * Payment API启动类
 * @author ndood
 */
@SpringBootApplication
@EnableWebMvc
@EnableSwagger2
@EnableCaching
@EnableTransactionManagement
@MapperScan({"com.ndood.*.base.dao"})
public class GatewayApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}
	
}