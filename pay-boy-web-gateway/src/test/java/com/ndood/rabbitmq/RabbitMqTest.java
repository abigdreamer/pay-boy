package com.ndood.rabbitmq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ndood.gateway.app.rabbitmq.NotifySender;
import com.ndood.gateway.app.rabbitmq.PollingSender;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitMqTest {
 
    @Autowired
    private PollingSender pollingSender;
    
    @Autowired
    private NotifySender notifySender;
 
    @Test
    public void testPolling() throws Exception {
    	pollingSender.send("sss");
    }

    @Test
    public void testNotify() throws Exception {
    	notifySender.send();
    }
    
}
