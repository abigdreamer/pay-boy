//package com.ndood.payment.dao;
//
//import java.util.List;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.google.gson.Gson;
//import com.ndood.api.base.dao.extend.support_staff.StaffDao;
//import com.ndood.api.base.pojo.merchant.StaffPo;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class TestDao {
//	
//	@Autowired
//	private StaffDao userDao;
//	
//	@Test
//	public void select() {
//		List<StaffPo> list = userDao.selectListBySQL();
//		System.out.println(new Gson().toJson(list));
//	}
//	
//	@Test
//	public void selectPage() {
//		StaffPo po = new StaffPo();
//		po.setNickName("aaa");
//		IPage<StaffPo> page = userDao.selectPage(new Page<StaffPo>(0, 10), new QueryWrapper<StaffPo>().setEntity(po));
//		System.out.println(new Gson().toJson(page));
//	}
//	
//	@Test
//	public void selectById() {
//		StaffPo po = userDao.selectById(83);
//		System.out.println(new Gson().toJson(po));
//	}
//	
//	@Test
//	public void insert() {
//		StaffPo po = new StaffPo();
//		po.setNickName("222");
//		userDao.insert(po);
//	}
//	
//	@Test
//	public void update() {
//		StaffPo po = new StaffPo();
//		po.setNickName("");
//		po.setId(84);
//		userDao.updateById(po);
//	}
//	
//	@Test
//	public void delete() {
//		userDao.deleteById(84);
//	}
//	
//}
