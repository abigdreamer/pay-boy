package com.ndood.common.base.dao.agent;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ndood.common.base.pojo.agent.AAgentPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
public interface AAgentMapper extends BaseMapper<AAgentPo> {

}
