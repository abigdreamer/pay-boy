package com.ndood.common.base.pojo.record;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-06-15
 */
@TableName("r_rec_pay")
public class RRecPayPo extends Model<RRecPayPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 支付订单ID
     */
    private String payOrderNo;

    /**
     * 交易号
     */
    private String tradeNo;

    private Integer mchId;

    private String mchName;

    private Integer agentId;

    private String agentName;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 支付方式： 1 支付宝 2 微信
     */
    private Integer payWay;

    /**
     * 支付类型：1 扫码 2 被扫 3门店码
     */
    private Integer payType;

    private Integer contractId;

    /**
     * 合同类型：1直清 2 二清 3 子商户
     */
    private Integer contractType;

    private String contractNo;

    private String contractName;

    /**
     * 支付产品编码
     */
    private String productNo;

    /**
     * 支付产品名称
     */
    private String productName;

    /**
     * 支付渠道编码
     */
    private Integer channelId;

    /**
     * 渠道编码
     */
    private String channelNo;

    /**
     * 支付渠道名称
     */
    private String channelName;

    /**
     * 总金额
     */
    private BigDecimal totalAmount;

    /**
     * 商家优惠金额
     */
    private BigDecimal freeAmount;

    /**
     * 商家实收金额
     */
    private BigDecimal receiptAmount;

    /**
     * 三方平台优惠金额
     */
    private BigDecimal thirdFreeAmount;

    /**
     * 买家付款金额
     */
    private BigDecimal buyerPayAmount;

    /**
     * 手续费
     */
    private BigDecimal fee;

    /**
     * 支付时间
     */
    private String payTime;

    /**
     * 买家支付账号
     */
    private String buyerLogonId;

    /**
     * 买家支付ID
     */
    private String buyerUserId;

    /**
     * 是否人工处理
     */
    private Integer isManual;

    /**
     * 三方APPID
     */
    private String appId;

    /**
     * 状态：1 创建 2 成功 3 失败
     */
    private Integer status;

    private String param1;

    private String param2;

    private String param3;

    private String param4;

    private String param5;

    private String param6;

    private Date createTime;

    private Date updateTime;

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayOrderNo() {
        return payOrderNo;
    }

    public void setPayOrderNo(String payOrderNo) {
        this.payOrderNo = payOrderNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getContractType() {
        return contractType;
    }

    public void setContractType(Integer contractType) {
        this.contractType = contractType;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getFreeAmount() {
        return freeAmount;
    }

    public void setFreeAmount(BigDecimal freeAmount) {
        this.freeAmount = freeAmount;
    }

    public BigDecimal getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(BigDecimal receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public BigDecimal getThirdFreeAmount() {
        return thirdFreeAmount;
    }

    public void setThirdFreeAmount(BigDecimal thirdFreeAmount) {
        this.thirdFreeAmount = thirdFreeAmount;
    }

    public BigDecimal getBuyerPayAmount() {
        return buyerPayAmount;
    }

    public void setBuyerPayAmount(BigDecimal buyerPayAmount) {
        this.buyerPayAmount = buyerPayAmount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getBuyerLogonId() {
        return buyerLogonId;
    }

    public void setBuyerLogonId(String buyerLogonId) {
        this.buyerLogonId = buyerLogonId;
    }

    public String getBuyerUserId() {
        return buyerUserId;
    }

    public void setBuyerUserId(String buyerUserId) {
        this.buyerUserId = buyerUserId;
    }

    public Integer getIsManual() {
        return isManual;
    }

    public void setIsManual(Integer isManual) {
        this.isManual = isManual;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public String getParam4() {
        return param4;
    }

    public void setParam4(String param4) {
        this.param4 = param4;
    }

    public String getParam5() {
        return param5;
    }

    public void setParam5(String param5) {
        this.param5 = param5;
    }

    public String getParam6() {
        return param6;
    }

    public void setParam6(String param6) {
        this.param6 = param6;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RRecPayPo{" +
        "id=" + id +
        ", payOrderNo=" + payOrderNo +
        ", tradeNo=" + tradeNo +
        ", mchId=" + mchId +
        ", mchName=" + mchName +
        ", agentId=" + agentId +
        ", agentName=" + agentName +
        ", orderNo=" + orderNo +
        ", payWay=" + payWay +
        ", payType=" + payType +
        ", contractId=" + contractId +
        ", contractType=" + contractType +
        ", contractNo=" + contractNo +
        ", contractName=" + contractName +
        ", productNo=" + productNo +
        ", productName=" + productName +
        ", channelId=" + channelId +
        ", channelNo=" + channelNo +
        ", channelName=" + channelName +
        ", totalAmount=" + totalAmount +
        ", freeAmount=" + freeAmount +
        ", receiptAmount=" + receiptAmount +
        ", thirdFreeAmount=" + thirdFreeAmount +
        ", buyerPayAmount=" + buyerPayAmount +
        ", fee=" + fee +
        ", payTime=" + payTime +
        ", buyerLogonId=" + buyerLogonId +
        ", buyerUserId=" + buyerUserId +
        ", isManual=" + isManual +
        ", appId=" + appId +
        ", status=" + status +
        ", param1=" + param1 +
        ", param2=" + param2 +
        ", param3=" + param3 +
        ", param4=" + param4 +
        ", param5=" + param5 +
        ", param6=" + param6 +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
