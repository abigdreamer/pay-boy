package com.ndood.common.base.util;

public class StringUtil {

	/**
	 * 截取字符串
	 */
	public static String substring(String str, int maxLen) {
		if(str==null) {
			return str;
		}
		if(str.length()<=maxLen) {
			return str;
		}
		return str.substring(0, maxLen);
	}
}
