package com.ndood.common.base.pojo.config;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("c_bank")
public class CBankPo extends Model<CBankPo> {

    private static final long serialVersionUID = 1L;

    /**
     * 银行编码
     */
    private Integer codeid;

    private String name;


    public Integer getCodeid() {
        return codeid;
    }

    public void setCodeid(Integer codeid) {
        this.codeid = codeid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Serializable pkVal() {
        return this.codeid;
    }

    @Override
    public String toString() {
        return "CBankPo{" +
        ", codeid=" + codeid +
        ", name=" + name +
        "}";
    }
}
