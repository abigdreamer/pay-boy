package com.ndood.common.base.pojo.statistic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("s_rec_shop")
public class SRecShopPo extends Model<SRecShopPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商户ID
     */
    private Integer shopId;

    /**
     * 门店全称
     */
    private String shopName;

    /**
     * 日期
     */
    private LocalDate day;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 商户类型：1 平台二清 2 商户直清 3 子商户
     */
    private Integer mchType;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户名称
     */
    private String agentName;

    /**
     * 交易成功订单数
     */
    private Long orderCount;

    /**
     * 交易成功订单金额
     */
    private BigDecimal orderAmount;

    /**
     * 退款数
     */
    private Long refundCount;

    /**
     * 退款交易金额
     */
    private BigDecimal refundAmount;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public Integer getMchType() {
        return mchType;
    }

    public void setMchType(Integer mchType) {
        this.mchType = mchType;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Long getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(Long refundCount) {
        this.refundCount = refundCount;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SRecShopPo{" +
        ", id=" + id +
        ", shopId=" + shopId +
        ", shopName=" + shopName +
        ", day=" + day +
        ", mchId=" + mchId +
        ", mchType=" + mchType +
        ", mchName=" + mchName +
        ", agentId=" + agentId +
        ", agentName=" + agentName +
        ", orderCount=" + orderCount +
        ", orderAmount=" + orderAmount +
        ", refundCount=" + refundCount +
        ", refundAmount=" + refundAmount +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
