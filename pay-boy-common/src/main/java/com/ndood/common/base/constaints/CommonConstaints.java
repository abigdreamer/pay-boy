package com.ndood.common.base.constaints;

/**
 * 支付宝沙箱配置
 * https://openhome.alipay.com/platform/appDaily.htm?tab=info
 */
public interface CommonConstaints {
	
	String ERROR_POLLING_PROCESS = "订单异常，走人工处理";
	
	/**
	 * 支付方式
	 */
	Integer REC_PAY_WAY_ALI = 1;
	Integer REC_PAY_WAY_WX = 2;
	Integer REC_PAY_WAY_UNION = 3;
	
	/**
	 * 支付类型
	 */
	Integer REC_PAY_TYPE_SCAN = 1; // 扫码
	Integer REC_PAY_TYPE_BAR = 2; // 条码
	Integer REC_PAY_TYPE_JSAPI = 3; // 公众号/服务窗
	Integer REC_PAY_TYPE_PC = 4; // 电脑网站
	Integer REC_PAY_TYPE_H5 = 5; // 手机网站
	Integer REC_PAY_TYPE_APP = 6; // APP
	Integer REC_PAY_TYPE_MINI = 7; // 小程序
	Integer REC_PAY_TYPE_AUTH = 8; // 预授权
	
	/**
	 * 订单状态
	 */
	Integer REC_ORDER_STATUS_PAYING = 1; // 支付中
	Integer REC_ORDER_STATUS_SUCCESS = 2; // 订单交易成功
	Integer REC_ORDER_STATUS_FAILED = 3; // 订单交易失败
	Integer REC_ORDER_STATUS_REFUND = 4; // 全额退款
	Integer REC_ORDER_STATUS_PART_REFUND = 5; // 局部退款
	Integer REC_ORDER_STATUS_CANCEL = 6; // 订单撤销
	
	/**
	 * 通道状态
	 */
	Integer REC_CHANNEL_PAY_STATUS_CREATE = 1; // 已创建
	Integer REC_CHANNEL_PAY_STATUS_SUCCESS = 2; // 支付成功
	Integer REC_CHANNEL_PAY_STATUS_FAILED = 3; // 支付失败
	Integer REC_CHANNEL_PAY_STATUS_CANCEL = 4; // 已撤销
	
	/**
	 * 通道退款状态
	 */
	Integer REC_CHANNEL_REFUND_STATUS_SUCCESS = 1; // 退款成功
	Integer REC_CHANNEL_REFUND_STATUS_FAILED = 2; // 退款失败
	
	/**
	 * 交易状态
	 */
	Integer REC_SETTLE_STATUS_NOT = 1; // 未结算
	Integer REC_SETTLE_STATUS_OK = 2; // 未结算

	/**
	 * 合同类型
	 */
	Integer CONTRACT_TYPE_SECOND = 1;
	Integer CONTRACT_TYPE_DIRECT = 2;
	Integer CONTRACT_TYPE_ISV = 3;
	
	/**
	 * 合同编码
	 */
	String C_ALI_F2F = "C_ALI_F2F";
	String C_ALI_PC = "C_ALI_PC";
	String C_ALI_H5 = "C_ALI_H5";
	String C_ALI_APP = "C_ALI_APP";
	String C_ALI_AUTH = "C_ALI_AUTH";
	String C_WX_PAY = "C_WX_PAY";
	String C_WX_MINI = "C_WX_MINI";
	
	/**
	 * 支付通道
	 */
	String CN_GF_ALI_SCAN = "CN_GF_ALI_SCAN";
	String CN_PA_ALI_SCAN = "CN_PA_ALI_SCAN";
	String CN_GF_ALI_BAR = "CN_GF_ALI_BAR";
	String CN_GF_ALI_JSAPI = "CN_GF_ALI_JSAPI";
	String CN_GF_ALI_PC = "CN_GF_ALI_PC";
	String CN_GF_ALI_H5 = "CN_GF_ALI_H5";
	String CN_GF_ALI_APP = "CN_GF_ALI_APP";
	String CN_GF_ALI_AUTH = "CN_GF_ALI_AUTH";
	String CN_GF_WX_SCAN = "CN_GF_WX_SCAN";
	String CN_GF_WX_BAR = "CN_GF_WX_BAR";
	String CN_GF_WX_JSAPI = "CN_GF_WX_JSAPI";
	String CN_GF_WX_H5 = "CN_GF_WX_H5";
	String CN_GF_WX_APP = "CN_GF_WX_APP";
	String CN_GF_WX_MINI = "CN_GF_WX_MINI";
	
	/**
	 * 支付产品，注意别乱改，跟数据库配置保持一致（数据库的只读）
	 */
	String P_ALI_SCAN = "P_ALI_SCAN";
	String P_ALI_BAR = "P_ALI_BAR";
	String P_ALI_JSAPI = "P_ALI_JSAPI";
	String P_ALI_PC = "P_ALI_PC";
	String P_ALI_H5 = "P_ALI_H5";
	String P_ALI_APP = "P_ALI_APP";
	String P_ALI_AUTH = "P_ALI_AUTH";
	String P_WX_SCAN = "P_WX_SCAN";
	String P_WX_BAR = "P_WX_BAR";
	String P_WX_JSAPI = "P_WX_JSAPI";
	String P_WX_H5 = "P_WX_H5";
	String P_WX_APP = "P_WX_APP";
	String P_WX_MINI = "P_WX_MINI";
	
	/**
	 * 交易类型
	 */
	Integer TRADE_TYPE_REC_PAY = 1;
	Integer TRADE_TYPE_REC_REFUND = 2;
	
	/**
	 * 结算状态
	 */
	Integer SETTLE_STATUS_INIT = 0;
	Integer SETTLE_STATUS_COVER = 1;
	
	/**
	 * 结算类型 1 固定金额 2 费率
	 */
	Integer SETTLE_TYPE_SOLID = 1;
	Integer SETTLE_TYPE_RATE = 2;

	/**
	 * 对账类型 1 离线对账 2 在线对账
	 */
	Integer RECON_TYPE_OFFLINE = 1;
	Integer RECON_TYPE_ONLINE = 2;
	
	/**
	 * 对账状态：1 待对账 2 对账成功 3 对账失败
	 */
	Integer RECON_BATCH_STATUS_CREATED = 1;
	Integer RECON_BATCH_STATUS_RECONED = 2;
	Integer RECON_BATCH_STATUS_CHECKED = 3;
	
	/**
	 * 交易状态：1 创建 2 成功 3 失败
	 */
	Integer REC_TRADE_STATUS_CREATE = 1;
	Integer REC_TRADE_STATUS_SUCCESS = 2;
	Integer REC_TRADE_STATUS_FAILED = 3;
}
