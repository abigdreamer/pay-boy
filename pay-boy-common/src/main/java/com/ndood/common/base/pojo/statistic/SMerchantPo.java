package com.ndood.common.base.pojo.statistic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("s_merchant")
public class SMerchantPo extends Model<SMerchantPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 日期
     */
    private LocalDate day;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户名称
     */
    private String agentName;

    /**
     * 收入笔数
     */
    private Long incomeCount;

    /**
     * 收入总金额
     */
    private BigDecimal incomeAmount;

    /**
     * 支出笔数
     */
    private Long outcomeCount;

    /**
     * 支出总金额
     */
    private BigDecimal outcomeAmount;

    /**
     * 交易成功订单数
     */
    private Long orderCount;

    /**
     * 交易成功订单金额
     */
    private BigDecimal orderAmount;

    /**
     * 退款数
     */
    private Long refundCount;

    /**
     * 退款交易金额
     */
    private BigDecimal refundAmount;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Long getIncomeCount() {
        return incomeCount;
    }

    public void setIncomeCount(Long incomeCount) {
        this.incomeCount = incomeCount;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public Long getOutcomeCount() {
        return outcomeCount;
    }

    public void setOutcomeCount(Long outcomeCount) {
        this.outcomeCount = outcomeCount;
    }

    public BigDecimal getOutcomeAmount() {
        return outcomeAmount;
    }

    public void setOutcomeAmount(BigDecimal outcomeAmount) {
        this.outcomeAmount = outcomeAmount;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Long getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(Long refundCount) {
        this.refundCount = refundCount;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SMerchantPo{" +
        ", id=" + id +
        ", mchId=" + mchId +
        ", day=" + day +
        ", mchName=" + mchName +
        ", agentId=" + agentId +
        ", agentName=" + agentName +
        ", incomeCount=" + incomeCount +
        ", incomeAmount=" + incomeAmount +
        ", outcomeCount=" + outcomeCount +
        ", outcomeAmount=" + outcomeAmount +
        ", orderCount=" + orderCount +
        ", orderAmount=" + orderAmount +
        ", refundCount=" + refundCount +
        ", refundAmount=" + refundAmount +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
