package com.ndood.common.base.pojo.agent;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("a_staff_role")
public class AStaffRolePo extends Model<AStaffRolePo> {

    private static final long serialVersionUID = 1L;

    private Integer roleId;

    private Integer staffId;


    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "AStaffRolePo{" +
        ", roleId=" + roleId +
        ", staffId=" + staffId +
        "}";
    }
}
