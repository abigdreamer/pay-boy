package com.ndood.common.base.pojo.record;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_rec_notify_history")
public class RRecNotifyHistoryPo extends Model<RRecNotifyHistoryPo> {

    private static final long serialVersionUID = 1L;

    @TableId(type=IdType.AUTO)
    private Long id;
    
    private String recordNo;

    /**
     * 代理商ID
     */
    private String notifyOrderNo;

    private String orderNo;

    /**
     * 通知地址
     */
    private String notifyUrl;

    /**
     * 通知报文内容
     */
    private String notifyContent;

    /**
     * 批次
     */
    private Integer batch;

    /**
     * 通知结果： 0 通知中 2 通知成功 3通知失败
     */
    private Integer status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public String getNotifyOrderNo() {
        return notifyOrderNo;
    }

    public void setNotifyOrderNo(String notifyOrderNo) {
        this.notifyOrderNo = notifyOrderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyContent() {
        return notifyContent;
    }

    public void setNotifyContent(String notifyContent) {
        this.notifyContent = notifyContent;
    }

    public Integer getBatch() {
        return batch;
    }

    public void setBatch(Integer batch) {
        this.batch = batch;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
    public String toString() {
        return "RRecNotifyHistoryPo{" +
        ", recordNo=" + recordNo +
        ", notifyOrderNo=" + notifyOrderNo +
        ", orderNo=" + orderNo +
        ", notifyUrl=" + notifyUrl +
        ", notifyContent=" + notifyContent +
        ", batch=" + batch +
        ", status=" + status +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
