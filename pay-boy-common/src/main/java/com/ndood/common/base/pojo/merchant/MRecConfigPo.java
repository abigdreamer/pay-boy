package com.ndood.common.base.pojo.merchant;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("m_rec_config")
public class MRecConfigPo extends Model<MRecConfigPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 微信商户号
     */
    private String wxMchId;

    /**
     * 微信app_id
     */
    private String wxAppId;

    /**
     * 微信app_secret
     */
    private String wxAppSecret;

    private String wxApiSecret;

    private String wxCertStream;

    private String aliAppId;

    private String aliAlipayPublicRsa2Secret;

    private String aliMchPrivateRsa2Secret;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getWxMchId() {
        return wxMchId;
    }

    public void setWxMchId(String wxMchId) {
        this.wxMchId = wxMchId;
    }

    public String getWxAppId() {
        return wxAppId;
    }

    public void setWxAppId(String wxAppId) {
        this.wxAppId = wxAppId;
    }

    public String getWxAppSecret() {
        return wxAppSecret;
    }

    public void setWxAppSecret(String wxAppSecret) {
        this.wxAppSecret = wxAppSecret;
    }

    public String getWxApiSecret() {
        return wxApiSecret;
    }

    public void setWxApiSecret(String wxApiSecret) {
        this.wxApiSecret = wxApiSecret;
    }

    public String getWxCertStream() {
        return wxCertStream;
    }

    public void setWxCertStream(String wxCertStream) {
        this.wxCertStream = wxCertStream;
    }

    public String getAliAppId() {
        return aliAppId;
    }

    public void setAliAppId(String aliAppId) {
        this.aliAppId = aliAppId;
    }

    public String getAliAlipayPublicRsa2Secret() {
        return aliAlipayPublicRsa2Secret;
    }

    public void setAliAlipayPublicRsa2Secret(String aliAlipayPublicRsa2Secret) {
        this.aliAlipayPublicRsa2Secret = aliAlipayPublicRsa2Secret;
    }

    public String getAliMchPrivateRsa2Secret() {
        return aliMchPrivateRsa2Secret;
    }

    public void setAliMchPrivateRsa2Secret(String aliMchPrivateRsa2Secret) {
        this.aliMchPrivateRsa2Secret = aliMchPrivateRsa2Secret;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MRecConfigPo{" +
        ", id=" + id +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", wxMchId=" + wxMchId +
        ", wxAppId=" + wxAppId +
        ", wxAppSecret=" + wxAppSecret +
        ", wxApiSecret=" + wxApiSecret +
        ", wxCertStream=" + wxCertStream +
        ", aliAppId=" + aliAppId +
        ", aliAlipayPublicRsa2Secret=" + aliAlipayPublicRsa2Secret +
        ", aliMchPrivateRsa2Secret=" + aliMchPrivateRsa2Secret +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
