package com.ndood.common.base.dao.statistic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ndood.common.base.pojo.statistic.SRecPayWayPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
public interface SRecPayWayMapper extends BaseMapper<SRecPayWayPo> {

}
