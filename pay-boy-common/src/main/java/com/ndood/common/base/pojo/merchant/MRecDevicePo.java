package com.ndood.common.base.pojo.merchant;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("m_rec_device")
public class MRecDevicePo extends Model<MRecDevicePo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    private Integer shopId;

    /**
     * 设备类型 1 pos机 2 打印机
     */
    private Integer deviceType;

    /**
     * 设备ID，用于支付时校验
     */
    private String deviceNo;

    /**
     * 机器型号
     */
    private String machineNo;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备描述
     */
    private String desc;

    /**
     * 设备厂家
     */
    private String factory;

    /**
     * 厂家电话
     */
    private String factoryTelephone;

    /**
     * 购买日期
     */
    private LocalDate purchaseDate;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getMachineNo() {
        return machineNo;
    }

    public void setMachineNo(String machineNo) {
        this.machineNo = machineNo;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getFactoryTelephone() {
        return factoryTelephone;
    }

    public void setFactoryTelephone(String factoryTelephone) {
        this.factoryTelephone = factoryTelephone;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MRecDevicePo{" +
        ", id=" + id +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", shopId=" + shopId +
        ", deviceType=" + deviceType +
        ", deviceNo=" + deviceNo +
        ", machineNo=" + machineNo +
        ", deviceName=" + deviceName +
        ", desc=" + desc +
        ", factory=" + factory +
        ", factoryTelephone=" + factoryTelephone +
        ", purchaseDate=" + purchaseDate +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
