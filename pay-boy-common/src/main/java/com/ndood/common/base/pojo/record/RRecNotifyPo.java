package com.ndood.common.base.pojo.record;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("r_rec_notify")
public class RRecNotifyPo extends Model<RRecNotifyPo> {

    private static final long serialVersionUID = 1L;

    @TableId(type=IdType.AUTO)
    private Long id;
    
    private String notifyOrderNo;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 通知地址
     */
    private String notifyUrl;

    /**
     * 通知次数
     */
    private Integer notifyCount;

    /**
     * 通知报文内容
     */
    private String notifyContent;

    /**
     * 最后一次通知时间
     */
    private LocalDateTime lastTime;

    /**
     * 通知状态： 0 未通知 1 已通知
     */
    private Integer status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getNotifyOrderNo() {
        return notifyOrderNo;
    }

    public void setNotifyOrderNo(String notifyOrderNo) {
        this.notifyOrderNo = notifyOrderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public Integer getNotifyCount() {
        return notifyCount;
    }

    public void setNotifyCount(Integer notifyCount) {
        this.notifyCount = notifyCount;
    }

    public String getNotifyContent() {
        return notifyContent;
    }

    public void setNotifyContent(String notifyContent) {
        this.notifyContent = notifyContent;
    }

    public LocalDateTime getLastTime() {
        return lastTime;
    }

    public void setLastTime(LocalDateTime lastTime) {
        this.lastTime = lastTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
    public String toString() {
        return "RRecNotifyPo{" +
        ", notifyOrderNo=" + notifyOrderNo +
        ", orderNo=" + orderNo +
        ", notifyUrl=" + notifyUrl +
        ", notifyCount=" + notifyCount +
        ", notifyContent=" + notifyContent +
        ", lastTime=" + lastTime +
        ", status=" + status +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
