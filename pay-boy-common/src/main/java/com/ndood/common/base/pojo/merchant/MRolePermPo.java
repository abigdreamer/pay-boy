package com.ndood.common.base.pojo.merchant;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("m_role_perm")
public class MRolePermPo extends Model<MRolePermPo> {

    private static final long serialVersionUID = 1L;

    private Integer roleId;

    private Integer permId;


    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPermId() {
        return permId;
    }

    public void setPermId(Integer permId) {
        this.permId = permId;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "MRolePermPo{" +
        ", roleId=" + roleId +
        ", permId=" + permId +
        "}";
    }
}
