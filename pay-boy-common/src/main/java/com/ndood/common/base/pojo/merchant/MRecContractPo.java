package com.ndood.common.base.pojo.merchant;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("m_rec_contract")
public class MRecContractPo extends Model<MRecContractPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 合同类型： 1 商户直清 2 平台二清 3 ISV子商户
     */
    private Integer contractType;

    /**
     * 合同编码
     */
    private String contractNo;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 支付方式
     */
    private Integer payWay;

    /**
     * 结算方式：1 按费率 2 按固定金额
     */
    private Integer settleType;

    /**
     * 合同名称
     */
    private String name;

    /**
     * 签约费率，签约后不可修改。影响支付结算
     */
    private BigDecimal rate;

    /**
     * 平台分润费率
     */
    private BigDecimal platformRate;

    /**
     * 平台每笔收费
     */
    private BigDecimal platformFee;

    /**
     * 代理分润费率
     */
    private BigDecimal agentRate;

    /**
     * 代理每笔收费
     */
    private BigDecimal agentFee;

    /**
     * 合同到期时间
     */
    private LocalDate endTime;

    /**
     * 状态：0 待审核 1 可用 2 禁用
     */
    private Integer status;

    /**
     * 是否已设置密钥：0 否 1 是
     */
    private Integer isSetPaySecret;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public Integer getContractType() {
        return contractType;
    }

    public void setContractType(Integer contractType) {
        this.contractType = contractType;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public Integer getSettleType() {
        return settleType;
    }

    public void setSettleType(Integer settleType) {
        this.settleType = settleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getPlatformRate() {
        return platformRate;
    }

    public void setPlatformRate(BigDecimal platformRate) {
        this.platformRate = platformRate;
    }

    public BigDecimal getPlatformFee() {
        return platformFee;
    }

    public void setPlatformFee(BigDecimal platformFee) {
        this.platformFee = platformFee;
    }

    public BigDecimal getAgentRate() {
        return agentRate;
    }

    public void setAgentRate(BigDecimal agentRate) {
        this.agentRate = agentRate;
    }

    public BigDecimal getAgentFee() {
        return agentFee;
    }

    public void setAgentFee(BigDecimal agentFee) {
        this.agentFee = agentFee;
    }

    public LocalDate getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDate endTime) {
        this.endTime = endTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsSetPaySecret() {
        return isSetPaySecret;
    }

    public void setIsSetPaySecret(Integer isSetPaySecret) {
        this.isSetPaySecret = isSetPaySecret;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MRecContractPo{" +
        ", id=" + id +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", contractType=" + contractType +
        ", contractNo=" + contractNo +
        ", contractName=" + contractName +
        ", payWay=" + payWay +
        ", settleType=" + settleType +
        ", name=" + name +
        ", rate=" + rate +
        ", platformRate=" + platformRate +
        ", platformFee=" + platformFee +
        ", agentRate=" + agentRate +
        ", agentFee=" + agentFee +
        ", endTime=" + endTime +
        ", status=" + status +
        ", isSetPaySecret=" + isSetPaySecret +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
