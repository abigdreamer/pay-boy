package com.ndood.common.base.pojo.config;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("c_rec_contract")
public class CRecContractPo extends Model<CRecContractPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 合同编码
     */
    private String contractNo;

    /**
     * 合同类型：1 商户直清 2 平台二清 3 服务商子商户
     */
    private Integer type;

    /**
     * 合同名称
     */
    private String name;

    /**
     * 支付方式：支付宝，微信，银联，平安
     */
    private Integer payWay;

    /**
     * 三方结算类型： 1 D0 2 D1 3 T0 4 T1
     */
    private String thirdSettleDesc;

    /**
     * 合同最大费率，比如支付宝0.055
     */
    private BigDecimal maxRate;

    /**
     * 合同签约指引
     */
    private String principle;

    /**
     * 状态：0 禁用 1 启用
     */
    private Integer status;

    /**
     * 排序字段
     */
    private Integer sort;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public String getThirdSettleDesc() {
        return thirdSettleDesc;
    }

    public void setThirdSettleDesc(String thirdSettleDesc) {
        this.thirdSettleDesc = thirdSettleDesc;
    }

    public BigDecimal getMaxRate() {
        return maxRate;
    }

    public void setMaxRate(BigDecimal maxRate) {
        this.maxRate = maxRate;
    }

    public String getPrinciple() {
        return principle;
    }

    public void setPrinciple(String principle) {
        this.principle = principle;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CRecContractPo{" +
        ", id=" + id +
        ", contractNo=" + contractNo +
        ", type=" + type +
        ", name=" + name +
        ", payWay=" + payWay +
        ", thirdSettleDesc=" + thirdSettleDesc +
        ", maxRate=" + maxRate +
        ", principle=" + principle +
        ", status=" + status +
        ", sort=" + sort +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
