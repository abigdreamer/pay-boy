package com.ndood.common.base.pojo.operator;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("o_config")
public class OConfigPo extends Model<OConfigPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 网站名称
     */
    private String websiteName;

    /**
     * 网站logo
     */
    private String websiteLogo;

    /**
     * 网站域名
     */
    private String websiteDomain;

    /**
     * 公司
     */
    private String company;

    /**
     * 联系邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 联系QQ
     */
    private String qq;

    /**
     * 微信商户号
     */
    private String wxMchId;

    /**
     * 微信app_id
     */
    private String wxAppId;

    /**
     * 微信app_secret
     */
    private String wxAppSecret;

    private String wxApiSecret;

    private String wxCertStream;

    private String aliAppId;

    private String aliAlipayPublicRsa2Secret;

    private String aliMchPrivateRsa2Secret;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    public String getWebsiteLogo() {
        return websiteLogo;
    }

    public void setWebsiteLogo(String websiteLogo) {
        this.websiteLogo = websiteLogo;
    }

    public String getWebsiteDomain() {
        return websiteDomain;
    }

    public void setWebsiteDomain(String websiteDomain) {
        this.websiteDomain = websiteDomain;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWxMchId() {
        return wxMchId;
    }

    public void setWxMchId(String wxMchId) {
        this.wxMchId = wxMchId;
    }

    public String getWxAppId() {
        return wxAppId;
    }

    public void setWxAppId(String wxAppId) {
        this.wxAppId = wxAppId;
    }

    public String getWxAppSecret() {
        return wxAppSecret;
    }

    public void setWxAppSecret(String wxAppSecret) {
        this.wxAppSecret = wxAppSecret;
    }

    public String getWxApiSecret() {
        return wxApiSecret;
    }

    public void setWxApiSecret(String wxApiSecret) {
        this.wxApiSecret = wxApiSecret;
    }

    public String getWxCertStream() {
        return wxCertStream;
    }

    public void setWxCertStream(String wxCertStream) {
        this.wxCertStream = wxCertStream;
    }

    public String getAliAppId() {
        return aliAppId;
    }

    public void setAliAppId(String aliAppId) {
        this.aliAppId = aliAppId;
    }

    public String getAliAlipayPublicRsa2Secret() {
        return aliAlipayPublicRsa2Secret;
    }

    public void setAliAlipayPublicRsa2Secret(String aliAlipayPublicRsa2Secret) {
        this.aliAlipayPublicRsa2Secret = aliAlipayPublicRsa2Secret;
    }

    public String getAliMchPrivateRsa2Secret() {
        return aliMchPrivateRsa2Secret;
    }

    public void setAliMchPrivateRsa2Secret(String aliMchPrivateRsa2Secret) {
        this.aliMchPrivateRsa2Secret = aliMchPrivateRsa2Secret;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OConfigPo{" +
        ", id=" + id +
        ", websiteName=" + websiteName +
        ", websiteLogo=" + websiteLogo +
        ", websiteDomain=" + websiteDomain +
        ", company=" + company +
        ", email=" + email +
        ", telephone=" + telephone +
        ", qq=" + qq +
        ", wxMchId=" + wxMchId +
        ", wxAppId=" + wxAppId +
        ", wxAppSecret=" + wxAppSecret +
        ", wxApiSecret=" + wxApiSecret +
        ", wxCertStream=" + wxCertStream +
        ", aliAppId=" + aliAppId +
        ", aliAlipayPublicRsa2Secret=" + aliAlipayPublicRsa2Secret +
        ", aliMchPrivateRsa2Secret=" + aliMchPrivateRsa2Secret +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
