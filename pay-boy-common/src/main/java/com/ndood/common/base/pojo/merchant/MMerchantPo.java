package com.ndood.common.base.pojo.merchant;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("m_merchant")
public class MMerchantPo extends Model<MMerchantPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户名称
     */
    private String name;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 企业QQ
     */
    private String qq;

    /**
     * 商户备注
     */
    private String remark;

    /**
     * 经营类目
     */
    private String industryId;

    /**
     * 网站名称
     */
    private String websiteName;

    /**
     * 网站地址
     */
    private String websiteUrl;

    /**
     * 营业执照类型：1 三证合一 2 非三证合一,
     */
    private Integer businessLicenseType;

    /**
     * 营业执照号码
     */
    private String businessLicenseNo;

    /**
     * 营业执照生效时间
     */
    private LocalDate businessLicenseNoStart;

    /**
     * 营业执照失效时间
     */
    private LocalDate businessLicenseNoEnd;

    /**
     * 税务登记号
     */
    private String taxNo;

    /**
     * 税务登记号生效时间
     */
    private LocalDate taxNoStart;

    /**
     * 税务登记号失效时间
     */
    private LocalDate taxNoEnd;

    /**
     * 组织机构代码
     */
    private String orgNo;

    /**
     * 组织机构代码生效时间
     */
    private LocalDate orgNoStart;

    /**
     * 组织机构代码失效时间
     */
    private LocalDate orgNoEnd;

    /**
     * 法人姓名
     */
    private String legalPersonName;

    /**
     * 法人性别：1 男 2 女
     */
    private Integer legalPersonSex;

    /**
     * 法人证件类型
     */
    private Integer legalPersonCertType;

    /**
     * 法人证件号码
     */
    private String legalPersonCertNo;

    private String legalPersonCertRegion;

    /**
     * 法人证件生效时间
     */
    private Date legalPersonCertStart;

    /**
     * 法人证件失效时间
     */
    private Date legalPersonCertEnd;

    private String legalPersonMobile;

    private String legalPersonEmail;

    /**
     * 法人职业
     */
    private Integer legalPersonJob;

    private String legalPersonAddress;

    /**
     * 签约账户类型：1 对私 2 对公
     */
    private Integer accountType;

    /**
     * 账户人身份 1 法人 2 法人亲属
     */
    private Integer accountBossType;

    /**
     * 开户银行
     */
    private String accountBankName;

    /**
     * 开户人姓名
     */
    private String accountName;

    /**
     * 收款账号
     */
    private String accountNo;

    /**
     * 收款人手机
     */
    private String accountMobile;

    /**
     * 清算联行号
     */
    private String accountCleanBankNo;

    /**
     * 营业执照图片
     */
    private String licenceImgUrl;

    /**
     * 开户许可证图片
     */
    private String openingPermitImgUrl;

    /**
     * 身份证正面图片
     */
    private String idImgUrl;

    /**
     * 身份证反面图片
     */
    private String idReverseImgUrl;

    /**
     * 税务登记证正面图片
     */
    private String taxImgUrl;

    /**
     * 组织机构代码图片
     */
    private String orgImgUrl;

    /**
     * 授权文件图片
     */
    private String authorizeImgUrl;

    /**
     * 状态: 0 待审核 1 启用 3 禁用
     */
    private Integer status;

    /**
     * 逻辑删除: 0 未删除 1 删除
     */
    private Integer logicDelete;

    private Date createTime;

    private Date updateTime;
    
    private Integer riskDay;

    public Integer getRiskDay() {
		return riskDay;
	}

	public void setRiskDay(Integer riskDay) {
		this.riskDay = riskDay;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public Integer getBusinessLicenseType() {
        return businessLicenseType;
    }

    public void setBusinessLicenseType(Integer businessLicenseType) {
        this.businessLicenseType = businessLicenseType;
    }

    public String getBusinessLicenseNo() {
        return businessLicenseNo;
    }

    public void setBusinessLicenseNo(String businessLicenseNo) {
        this.businessLicenseNo = businessLicenseNo;
    }

    public LocalDate getBusinessLicenseNoStart() {
        return businessLicenseNoStart;
    }

    public void setBusinessLicenseNoStart(LocalDate businessLicenseNoStart) {
        this.businessLicenseNoStart = businessLicenseNoStart;
    }

    public LocalDate getBusinessLicenseNoEnd() {
        return businessLicenseNoEnd;
    }

    public void setBusinessLicenseNoEnd(LocalDate businessLicenseNoEnd) {
        this.businessLicenseNoEnd = businessLicenseNoEnd;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public LocalDate getTaxNoStart() {
        return taxNoStart;
    }

    public void setTaxNoStart(LocalDate taxNoStart) {
        this.taxNoStart = taxNoStart;
    }

    public LocalDate getTaxNoEnd() {
        return taxNoEnd;
    }

    public void setTaxNoEnd(LocalDate taxNoEnd) {
        this.taxNoEnd = taxNoEnd;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public LocalDate getOrgNoStart() {
        return orgNoStart;
    }

    public void setOrgNoStart(LocalDate orgNoStart) {
        this.orgNoStart = orgNoStart;
    }

    public LocalDate getOrgNoEnd() {
        return orgNoEnd;
    }

    public void setOrgNoEnd(LocalDate orgNoEnd) {
        this.orgNoEnd = orgNoEnd;
    }

    public String getLegalPersonName() {
        return legalPersonName;
    }

    public void setLegalPersonName(String legalPersonName) {
        this.legalPersonName = legalPersonName;
    }

    public Integer getLegalPersonSex() {
        return legalPersonSex;
    }

    public void setLegalPersonSex(Integer legalPersonSex) {
        this.legalPersonSex = legalPersonSex;
    }

    public Integer getLegalPersonCertType() {
        return legalPersonCertType;
    }

    public void setLegalPersonCertType(Integer legalPersonCertType) {
        this.legalPersonCertType = legalPersonCertType;
    }

    public String getLegalPersonCertNo() {
        return legalPersonCertNo;
    }

    public void setLegalPersonCertNo(String legalPersonCertNo) {
        this.legalPersonCertNo = legalPersonCertNo;
    }

    public String getLegalPersonCertRegion() {
        return legalPersonCertRegion;
    }

    public void setLegalPersonCertRegion(String legalPersonCertRegion) {
        this.legalPersonCertRegion = legalPersonCertRegion;
    }

    public Date getLegalPersonCertStart() {
        return legalPersonCertStart;
    }

    public void setLegalPersonCertStart(Date legalPersonCertStart) {
        this.legalPersonCertStart = legalPersonCertStart;
    }

    public Date getLegalPersonCertEnd() {
        return legalPersonCertEnd;
    }

    public void setLegalPersonCertEnd(Date legalPersonCertEnd) {
        this.legalPersonCertEnd = legalPersonCertEnd;
    }

    public String getLegalPersonMobile() {
        return legalPersonMobile;
    }

    public void setLegalPersonMobile(String legalPersonMobile) {
        this.legalPersonMobile = legalPersonMobile;
    }

    public String getLegalPersonEmail() {
        return legalPersonEmail;
    }

    public void setLegalPersonEmail(String legalPersonEmail) {
        this.legalPersonEmail = legalPersonEmail;
    }

    public Integer getLegalPersonJob() {
        return legalPersonJob;
    }

    public void setLegalPersonJob(Integer legalPersonJob) {
        this.legalPersonJob = legalPersonJob;
    }

    public String getLegalPersonAddress() {
        return legalPersonAddress;
    }

    public void setLegalPersonAddress(String legalPersonAddress) {
        this.legalPersonAddress = legalPersonAddress;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public Integer getAccountBossType() {
        return accountBossType;
    }

    public void setAccountBossType(Integer accountBossType) {
        this.accountBossType = accountBossType;
    }

    public String getAccountBankName() {
        return accountBankName;
    }

    public void setAccountBankName(String accountBankName) {
        this.accountBankName = accountBankName;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountMobile() {
        return accountMobile;
    }

    public void setAccountMobile(String accountMobile) {
        this.accountMobile = accountMobile;
    }

    public String getAccountCleanBankNo() {
        return accountCleanBankNo;
    }

    public void setAccountCleanBankNo(String accountCleanBankNo) {
        this.accountCleanBankNo = accountCleanBankNo;
    }

    public String getLicenceImgUrl() {
        return licenceImgUrl;
    }

    public void setLicenceImgUrl(String licenceImgUrl) {
        this.licenceImgUrl = licenceImgUrl;
    }

    public String getOpeningPermitImgUrl() {
        return openingPermitImgUrl;
    }

    public void setOpeningPermitImgUrl(String openingPermitImgUrl) {
        this.openingPermitImgUrl = openingPermitImgUrl;
    }

    public String getIdImgUrl() {
        return idImgUrl;
    }

    public void setIdImgUrl(String idImgUrl) {
        this.idImgUrl = idImgUrl;
    }

    public String getIdReverseImgUrl() {
        return idReverseImgUrl;
    }

    public void setIdReverseImgUrl(String idReverseImgUrl) {
        this.idReverseImgUrl = idReverseImgUrl;
    }

    public String getTaxImgUrl() {
        return taxImgUrl;
    }

    public void setTaxImgUrl(String taxImgUrl) {
        this.taxImgUrl = taxImgUrl;
    }

    public String getOrgImgUrl() {
        return orgImgUrl;
    }

    public void setOrgImgUrl(String orgImgUrl) {
        this.orgImgUrl = orgImgUrl;
    }

    public String getAuthorizeImgUrl() {
        return authorizeImgUrl;
    }

    public void setAuthorizeImgUrl(String authorizeImgUrl) {
        this.authorizeImgUrl = authorizeImgUrl;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Integer logicDelete) {
        this.logicDelete = logicDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MMerchantPo{" +
        ", id=" + id +
        ", agentId=" + agentId +
        ", name=" + name +
        ", mobile=" + mobile +
        ", email=" + email +
        ", qq=" + qq +
        ", remark=" + remark +
        ", industryId=" + industryId +
        ", websiteName=" + websiteName +
        ", websiteUrl=" + websiteUrl +
        ", businessLicenseType=" + businessLicenseType +
        ", businessLicenseNo=" + businessLicenseNo +
        ", businessLicenseNoStart=" + businessLicenseNoStart +
        ", businessLicenseNoEnd=" + businessLicenseNoEnd +
        ", taxNo=" + taxNo +
        ", taxNoStart=" + taxNoStart +
        ", taxNoEnd=" + taxNoEnd +
        ", orgNo=" + orgNo +
        ", orgNoStart=" + orgNoStart +
        ", orgNoEnd=" + orgNoEnd +
        ", legalPersonName=" + legalPersonName +
        ", legalPersonSex=" + legalPersonSex +
        ", legalPersonCertType=" + legalPersonCertType +
        ", legalPersonCertNo=" + legalPersonCertNo +
        ", legalPersonCertRegion=" + legalPersonCertRegion +
        ", legalPersonCertStart=" + legalPersonCertStart +
        ", legalPersonCertEnd=" + legalPersonCertEnd +
        ", legalPersonMobile=" + legalPersonMobile +
        ", legalPersonEmail=" + legalPersonEmail +
        ", legalPersonJob=" + legalPersonJob +
        ", legalPersonAddress=" + legalPersonAddress +
        ", accountType=" + accountType +
        ", accountBossType=" + accountBossType +
        ", accountBankName=" + accountBankName +
        ", accountName=" + accountName +
        ", accountNo=" + accountNo +
        ", accountMobile=" + accountMobile +
        ", accountCleanBankNo=" + accountCleanBankNo +
        ", licenceImgUrl=" + licenceImgUrl +
        ", openingPermitImgUrl=" + openingPermitImgUrl +
        ", idImgUrl=" + idImgUrl +
        ", idReverseImgUrl=" + idReverseImgUrl +
        ", taxImgUrl=" + taxImgUrl +
        ", orgImgUrl=" + orgImgUrl +
        ", authorizeImgUrl=" + authorizeImgUrl +
        ", status=" + status +
        ", logicDelete=" + logicDelete +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
