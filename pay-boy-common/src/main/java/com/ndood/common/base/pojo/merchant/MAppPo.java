package com.ndood.common.base.pojo.merchant;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author Bean
 * @since 2019-03-14
 */
@TableName("m_app")
public class MAppPo extends Model<MAppPo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 应用编码
     */
    private String appNo;

    /**
     * 应用名称
     */
    private String name;

    /**
     * 平台应用RSA2私钥
     */
    private String rsa2PlatPrivateKey;

    /**
     * 商户应用RSA2公钥
     */
    private String rsa2MchPublicKey;

    /**
     * 状态：0 禁用 1 可用
     */
    private Integer status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getMchId() {
        return mchId;
    }

    public void setMchId(Integer mchId) {
        this.mchId = mchId;
    }

    public String getAppNo() {
        return appNo;
    }

    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRsa2PlatPrivateKey() {
        return rsa2PlatPrivateKey;
    }

    public void setRsa2PlatPrivateKey(String rsa2PlatPrivateKey) {
        this.rsa2PlatPrivateKey = rsa2PlatPrivateKey;
    }

    public String getRsa2MchPublicKey() {
        return rsa2MchPublicKey;
    }

    public void setRsa2MchPublicKey(String rsa2MchPublicKey) {
        this.rsa2MchPublicKey = rsa2MchPublicKey;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MAppPo{" +
        ", id=" + id +
        ", agentId=" + agentId +
        ", mchId=" + mchId +
        ", appNo=" + appNo +
        ", name=" + name +
        ", rsa2PlatPrivateKey=" + rsa2PlatPrivateKey +
        ", rsa2MchPublicKey=" + rsa2MchPublicKey +
        ", status=" + status +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
