package com.ndood.common.base.util;

import java.math.BigDecimal;

/**
 * @author MCJ
 * @date 2018年7月30日
 * @description 提供精确的浮点数运算(包括加、减、乘、除、四舍五入)工具类
 * https://blog.csdn.net/mcj_2017/article/details/81939838 
 * 
 */
public class BigDecimalUtil {

    // 除法运算默认精度
    private static final int DEF_DIV_SCALE = 6;

    private BigDecimalUtil() {

    }

    /**
     * 精确加法
     */
    public static BigDecimal add(BigDecimal value1, BigDecimal value2) {
        return value1.add(value2);
    }

    /**
     * 精确加法
     */
    public static BigDecimal add(String value1, String value2) {
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.add(b2);
    }

    /**
     * 精确减法
     */
    public static BigDecimal sub(BigDecimal value1, BigDecimal value2) {
        return value1.subtract(value2);
    }

    /**
     * 精确减法
     */
    public static BigDecimal sub(String value1, String value2) {
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.subtract(b2);
    }

    /**
     * 精确乘法
     */
    public static BigDecimal mul(BigDecimal value1, BigDecimal value2) {
        return value1.multiply(value2);
    }

    /**
     * 精确乘法
     */
    public static BigDecimal mul(String value1, String value2) {
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.multiply(b2);
    }

    /**
     * 精确除法 使用默认精度
     */
    public static BigDecimal div(BigDecimal value1, BigDecimal value2) throws IllegalAccessException {
        return div(value1, value2, DEF_DIV_SCALE);
    }

    /**
     * 精确除法 使用默认精度
     */
    public static BigDecimal div(String value1, String value2) throws IllegalAccessException {
        return div(value1, value2, DEF_DIV_SCALE);
    }

    /**
     * 精确除法
     * 
     * @param scale
     *            精度
     */
    public static BigDecimal div(BigDecimal value1, BigDecimal value2, int scale) throws IllegalAccessException {
        if (scale < 0) {
            throw new IllegalAccessException("精确度不能小于0");
        }
        return value1.divide(value2, scale, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 精确除法
     * 
     * @param scale
     *            精度
     */
    public static BigDecimal div(String value1, String value2, int scale) throws IllegalAccessException {
        if (scale < 0) {
            throw new IllegalAccessException("精确度不能小于0");
        }
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 四舍五入
     * 
     * @param scale
     *            小数点后保留几位
     */
    public static BigDecimal round(BigDecimal v, int scale) throws IllegalAccessException {
        return div(v, new BigDecimal(1), scale);
    }

    /**
     * 四舍五入
     * 
     * @param scale
     *            小数点后保留几位
     */
    public static BigDecimal round(String v, int scale) throws IllegalAccessException {
        return div(v, "1", scale);
    }

    /**
     * 比较大小
     */
    public static boolean equalTo(BigDecimal b1, BigDecimal b2) {
        if (b1 == null || b2 == null) {
            return false;
        }
        return 0 == b1.compareTo(b2);
    }

}
