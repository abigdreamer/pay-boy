package com.ndood.api.app.sdk.wxpay.addons;

import org.springframework.stereotype.Component;

import com.ndood.api.app.sdk.wxpay.IWXPayDomain;
import com.ndood.api.app.sdk.wxpay.WXPayConfig;

/**
 * 微信支付域名信息
 */
@Component
public class LocalWXPayDomain implements IWXPayDomain{

	/**
	 * 打印报告
	 */
	@Override
	public void report(String domain, long elapsedTimeMillis, Exception ex) {
		System.out.println("report");
	}

	/**
	 * 获取主域名
	 */
	@Override
	public DomainInfo getDomain(WXPayConfig config) {
		DomainInfo domainInfo = new DomainInfo("api.mch.weixin.qq.com", true);
		return domainInfo;
	}

}
