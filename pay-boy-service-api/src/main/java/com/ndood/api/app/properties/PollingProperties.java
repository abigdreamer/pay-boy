package com.ndood.api.app.properties;

import lombok.Data;

/**
 * ndood polling配置主类
 */
@Data
public class PollingProperties {
	// 当面付最大查询次数和查询间隔（毫秒）
	private Integer max_query_retry = 5;
	private Integer query_duration = 5000;

	// 当面付最大撤销次数和撤销间隔（毫秒）
	private Integer max_cancel_retry = 3;
	private Integer cancel_duration = 2000;
	
}