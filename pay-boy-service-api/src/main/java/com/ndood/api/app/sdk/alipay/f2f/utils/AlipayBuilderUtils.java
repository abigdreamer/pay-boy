package com.ndood.api.app.sdk.alipay.f2f.utils;

import com.alipay.api.AlipayConstants;
import com.ndood.api.app.sdk.alipay.f2f.service.AlipayTradeService;
import com.ndood.api.app.sdk.alipay.f2f.service.impl.AlipayTradeServiceImpl;

public class AlipayBuilderUtils {
	/**
	 * 1 创建支付宝Service
	 */
	public static AlipayTradeService buildAlipayTradeService(String gatewayUrl, String alipayPublicKey, String appid, String privateKey) {
		AlipayTradeServiceImpl.ClientBuilder clientBuilder = new AlipayTradeServiceImpl.ClientBuilder();
		clientBuilder.setGatewayUrl(gatewayUrl);
		clientBuilder.setAppid(appid);
		clientBuilder.setPrivateKey(privateKey);
		clientBuilder.setFormat(AlipayConstants.FORMAT_JSON);
		clientBuilder.setCharset(AlipayConstants.CHARSET_UTF8);
		clientBuilder.setAlipayPublicKey(alipayPublicKey);
		clientBuilder.setSignType(AlipayConstants.SIGN_TYPE_RSA2);
		return clientBuilder.build();
	}
}
