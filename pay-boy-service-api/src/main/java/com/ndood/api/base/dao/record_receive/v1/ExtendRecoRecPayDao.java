package com.ndood.api.base.dao.record_receive.v1;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecPayDto;

public interface ExtendRecoRecPayDao {
	/**
	 * 1 根据订单号查询出订单支付信息
	 */
	@Select("SELECT "
			+ "o.`mch_id`, o.`agent_id`, o.`mch_name`, o.`agent_name`, p.`pay_order_no`, p.`order_no`, p.`pay_way`, p.`pay_type`, p.`contract_id`, p.`contract_type`, p.`contract_no`, "
			+ "p.`contract_name`, p.`product_no`, p.`product_name`, p.`channel_id`, p.`channel_no`, p.`channel_name`, "
			+ "p.`total_amount`, p.`free_amount`, p.`receipt_amount`, p.`third_free_amount`, p.`buyer_pay_amount`, "
			+ "p.`pay_time`, p.`status`, p.`param1`, p.`param2`, p.`param3`, p.`param4`, p.`param5`, p.`param6`, p.`create_time`, p.`update_time`"
			+ "FROM r_rec_pay p "
			+ "LEFT JOIN r_rec_offline_order o ON p.order_no = o.order_no "
			+ "WHERE p.`pay_order_no` = #{payOrderNo} ")
	public RecoRecPayDto extendFindReceivePayByPayOrderNo(@Param("payOrderNo") String payOrderNo);

	/**
	 * 2 查询出指定区间内所有APPID集合，去重
	 */
	@Select("SELECT DISTINCT app_id FROM r_rec_pay WHERE create_time >= #{startDate} and create_time < #{endDate} AND app_id IS NOT NULL AND pay_way = #{payWay} ")
	public List<String> extendDistinctFindAppidsByTimeZoneAndPayWay(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("payWay") Integer payway);

}
