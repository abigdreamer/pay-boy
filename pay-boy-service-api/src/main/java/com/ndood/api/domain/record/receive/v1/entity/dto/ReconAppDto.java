package com.ndood.api.domain.record.receive.v1.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconAppDto {
	private String mchId;
	private String appId;
	private String alipayPublicRsa2Secret;
	private String mchPrivateRsa2Secret;
}
