package com.ndood.api.domain.support.merchant.entity.dto;

import java.math.BigDecimal;

import com.ndood.api.domain.support.merchant.entity.SupportMchRecChannelDo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SupportMchRecChannelDto extends SupportMchRecChannelDo {
	/**
	 * ID
	 */
	private Integer id;
	/**
	 * 代理ID
	 */
	private Integer agentId;
	/**
	 * 商户ID
	 */
	private Integer mchId;
	/**
	 * 合同ID
	 */
	private Integer contractId;
	/**
	 * 合同编码
	 */
	private String contractNo;
	/**
	 * 合同类型： 1 二清 2 直清 3 子商户
	 */
	private Integer contractType;
	/**
	 * 合同名称
	 */
	private String contractName;
	/**
	 * 产品编码
	 */
	private String productNo;
	/**
	 * 产品名称
	 */
	private String productName;
	
	/**
	 * 结算类型： 1 按费率 2 按固定金额
	 */
	private Integer settle_type;
	/**
	 * 签约费率
	 */
	private BigDecimal rate;
	/**
	 * 平台分润费率
	 */
	private BigDecimal platformRate;
	/**
	 * 平台固定手续费
	 */
	private BigDecimal platformFee;
	/**
	 * 代理分润费率
	 */
	private BigDecimal agentRate;
	/**
	 * 代理固定手续费
	 */
	private BigDecimal agentFee;
}
