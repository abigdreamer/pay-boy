package com.ndood.api.domain.support.merchant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.api.base.dao.support_merchant.ExtendSupportMchRecConfigDao;
import com.ndood.api.domain.record.receive.v1.entity.dto.ReconAppDto;
import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecConfigDto;

/**
 * 支持能力-商户-收款支付服务
 */
@Service
public class SupportMchRecConfigService {
	
	@Autowired
	private ExtendSupportMchRecConfigDao extendSupportMchRecConfigDao;
	
	/**
	 * 1 根据商户ID查询出商户直清收款配置
	 * FIXME 加spring cache
	 */
	public SupportMchRecConfigDto getMerchantReceiveConfig(Integer merchantId){
		SupportMchRecConfigDto config = extendSupportMchRecConfigDao.extendFindMerchantReceiveConfigByMerchantId(merchantId);
		return config;
	}

	/**
	 * 2 查询出官方支付宝app信息
	 */
	public List<ReconAppDto> getMerchantOfficialAlipayReceiveAppList(String appIds) {
		List<ReconAppDto> configs = extendSupportMchRecConfigDao.extendFindMerchantOfficialAlipayReceiveConfigByAppIds(appIds);
		return configs;
	}
	
}
