package com.ndood.api.domain.risk.receive.v1.service;

import org.springframework.stereotype.Service;

import com.ndood.api.domain.support.merchant.entity.dto.SupportMchRecChannelDto;
import com.ndood.api.domain.support.staff.entity.dto.SupportStaffDto;

import lombok.extern.slf4j.Slf4j;

/**
 * 平台支付-风控-渠道风控
 * 风控的职责是对渠道等进行风险控制，防止被封号
 */
@Service
@Slf4j
public class RiskRecPlatformOfflineAlif2fService {

	/**
	 * 1 渠道风险控制
	 * @param channel 
	 */
	public Boolean checkRisk(String amount, SupportStaffDto staff, String product_no, SupportMchRecChannelDto channel) {
		log.info("模拟平台风控支付渠道： 没问题，老铁！！");
		return true;
	}
	
	/**
	 * 2 更新渠道风控统计
	 */
	public void updateRisk(Object dto) {
		// TODO Auto-generated method stub
		
	}

}
