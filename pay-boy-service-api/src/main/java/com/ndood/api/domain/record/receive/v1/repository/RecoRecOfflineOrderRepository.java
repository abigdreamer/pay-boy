package com.ndood.api.domain.record.receive.v1.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecOfflineOrderDto;
import com.ndood.common.base.dao.record.RRecOfflineOrderMapper;
import com.ndood.common.base.pojo.record.RRecOfflineOrderPo;

@Repository
public class RecoRecOfflineOrderRepository {

	@Autowired
	private RRecOfflineOrderMapper recOfflineOrderMapper;
	
	/**
	 * 1 插入线下订单
	 */
	public void insertRecOfflineOrder(RecoRecOfflineOrderDto dto) {
		RRecOfflineOrderPo po = new RRecOfflineOrderPo();
		po.setCanRefundAmount(dto.getCanRefundAmount());
		po.setRefundCashierId(dto.getRefundCashierId());
		po.setReceiptRefundAmount(dto.getReceiptRefundAmount());
		po.setRefundCashierName(dto.getRefundCashierName());
		po.setLastRefundTime(dto.getLastRefundTime());
		po.setPayCashierName(dto.getPayCashierName());
		po.setDeviceNo(dto.getDeviceNo());
		po.setAgentName(dto.getAgentName());
		po.setMchName(dto.getMchName());
		po.setShopId(dto.getShopId());
		po.setClientIp(dto.getClientIp());
		po.setAgentId(dto.getAgentId());
		po.setShopName(dto.getShopName());
		po.setOrderNo(dto.getOrderNo());
		po.setOutOrderNo(dto.getOutOrderNo());
		po.setExtra(dto.getExtra());
		po.setMchId(dto.getMchId());
		po.setBody(dto.getBody());
		po.setPayRemark(dto.getPayRemark());
		po.setTotalAmount(dto.getTotalAmount());
		po.setExpireTime(dto.getExpireTime());
		po.setLastPayTime(dto.getLastPayTime());
		po.setReceiptAmount(dto.getReceiptAmount());
		po.setStatus(dto.getStatus());
		po.setFreeAmount(dto.getFreeAmount());
		po.setPayCashierId(dto.getPayCashierId());
		po.setParam2(dto.getParam2());
		po.setParam4(dto.getParam4());
		po.setRefundAmount(dto.getRefundAmount());
		po.setParam3(dto.getParam3());
		po.setRefundRemark(dto.getRefundRemark());
		po.setParam1(dto.getParam1());
		po.setSubject(dto.getSubject());
		
		po.setCreateTime(new Date());
		po.setUpdateTime(new Date());
		if(dto.getCreateTime()!=null) {
			po.setCreateTime(dto.getCreateTime());
		}
		if(dto.getUpdateTime()!=null) {
			po.setUpdateTime(dto.getUpdateTime());
		}
		recOfflineOrderMapper.insert(po);
	}

	/**
	 * 2 更新线下订单
	 */
	public void updateRecOfflineOrder(RecoRecOfflineOrderDto dto) {
		RRecOfflineOrderPo po = new RRecOfflineOrderPo();
		po.setOrderNo(dto.getOrderNo());
		po.setCanRefundAmount(dto.getCanRefundAmount());
		po.setRefundCashierId(dto.getRefundCashierId());
		po.setReceiptRefundAmount(dto.getReceiptRefundAmount());
		po.setRefundCashierName(dto.getRefundCashierName());
		po.setLastRefundTime(dto.getLastRefundTime());
		po.setPayCashierName(dto.getPayCashierName());
		po.setDeviceNo(dto.getDeviceNo());
		po.setAgentName(dto.getAgentName());
		po.setMchName(dto.getMchName());
		po.setShopId(dto.getShopId());
		po.setClientIp(dto.getClientIp());
		po.setAgentId(dto.getAgentId());
		po.setShopName(dto.getShopName());
		po.setOrderNo(dto.getOrderNo());
		po.setOutOrderNo(dto.getOutOrderNo());
		po.setExtra(dto.getExtra());
		po.setMchId(dto.getMchId());
		po.setBody(dto.getBody());
		po.setPayRemark(dto.getPayRemark());
		po.setTotalAmount(dto.getTotalAmount());
		po.setExpireTime(dto.getExpireTime());
		po.setLastPayTime(dto.getLastPayTime());
		po.setReceiptAmount(dto.getReceiptAmount());
		po.setStatus(dto.getStatus());
		po.setFreeAmount(dto.getFreeAmount());
		po.setPayCashierId(dto.getPayCashierId());
		po.setParam2(dto.getParam2());
		po.setParam4(dto.getParam4());
		po.setRefundAmount(dto.getRefundAmount());
		po.setParam3(dto.getParam3());
		po.setRefundRemark(dto.getRefundRemark());
		po.setParam1(dto.getParam1());
		po.setSubject(dto.getSubject());
		UpdateWrapper<RRecOfflineOrderPo> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("order_no", po.getOrderNo());
		
		po.setUpdateTime(new Date());
		if(dto.getUpdateTime()!=null) {
			po.setUpdateTime(dto.getUpdateTime());
		}
		
		recOfflineOrderMapper.update(po, updateWrapper);
	}
	
}
