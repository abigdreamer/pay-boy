package com.ndood.api.domain.support.merchant.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SupportMchRecContractDo {
	private Integer id;
	private Integer settleType;
	private String name;
	private BigDecimal rate;
	private BigDecimal platformRate;
	private BigDecimal platformFee;
	private BigDecimal agentRate;
	private BigDecimal agentFee;
	private Date endTime;
}
