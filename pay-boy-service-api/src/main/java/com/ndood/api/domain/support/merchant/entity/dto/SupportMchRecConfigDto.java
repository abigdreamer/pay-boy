package com.ndood.api.domain.support.merchant.entity.dto;

import com.ndood.api.domain.support.merchant.entity.SupportMchRecConfigDo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SupportMchRecConfigDto extends SupportMchRecConfigDo{
	/**
	 * 代理ID
	 */
	private Integer agentId;
	/**
	 * 商户ID
	 */
	private Integer mchId;
}
