package com.ndood.api.domain.support.merchant.entity.dto;

import com.ndood.api.domain.support.merchant.entity.SupportMchRecContractDo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SupportMchRecContractDto extends SupportMchRecContractDo {
	/**
	 * 风险预存期
	 */
	private Integer riskDay; 
}
