package com.ndood.api.domain.support.staff.entity.dto;

import com.ndood.api.domain.support.staff.entity.SupportStaffDo;

import lombok.Getter;
import lombok.Setter;

/**
 * 支持服务员工领域对象 - 聚合根
 */
@Getter @Setter
public class SupportStaffDto extends SupportStaffDo {
	
	/**
	 * 代理ID
	 */
	private Integer agentId;
	/**
	 * 商户ID
	 */
	private Integer mchId;
	/**
	 * 门店ID
	 */
	private Integer shopId;
	/**
	 * 门店名
	 */
	private String shopName;
	/**
	 * 商户名
	 */
	private String merchantName;
	/**
	 * 代理名称
	 */
	private String agentName;
	
}
