package com.ndood.api.domain.record.receive.v1.entity.dto;

import com.ndood.api.domain.record.receive.v1.entity.RecoRecOfflineOrderDo;
import com.ndood.api.domain.record.receive.v1.entity.RecoRecPayDo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RecoRecOfflineOrderDto extends RecoRecOfflineOrderDo {
	
	private RecoRecPayDo pay;
}
