package com.ndood.api.domain.record.receive.v1.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecPayDto;
import com.ndood.common.base.dao.record.RRecPayMapper;
import com.ndood.common.base.pojo.record.RRecPayPo;
import com.ndood.common.base.util.CodeUtil;

@Repository
public class RecoRecPayRepository {

	@Autowired
	private RRecPayMapper rRecPayMapper;
	
	/**
	 * 1 插入支付记录
	 */
	public void insertRecPay(RecoRecPayDto dto) {
		RRecPayPo po = new RRecPayPo();
		po.setMchId(dto.getMchId());
		po.setMchName(dto.getMchName());
		po.setAgentId(dto.getAgentId());
		po.setAgentName(dto.getAgentName());
		po.setThirdFreeAmount(dto.getThirdFreeAmount());
		po.setBuyerPayAmount(dto.getBuyerPayAmount());
		po.setContractId(dto.getContractId());
		po.setParam4(dto.getParam4());
		po.setPayWay(dto.getPayWay());
		po.setProductNo(dto.getProductNo());
		po.setParam2(dto.getParam2());
		po.setProductName(dto.getProductName());
		po.setPayOrderNo(dto.getPayOrderNo());
		po.setContractNo(dto.getContractNo());
		po.setReceiptAmount(dto.getReceiptAmount());
		po.setPayTime(dto.getPayTime());
		po.setChannelNo(dto.getChannelNo());
		po.setContractName(dto.getContractName());
		po.setOrderNo(dto.getOrderNo());
		po.setTradeNo(dto.getTradeNo());
		po.setFee(dto.getFee());
		po.setParam1(dto.getParam1());
		po.setParam3(dto.getParam3());
		po.setPayType(dto.getPayType());
		po.setContractType(dto.getContractType());
		po.setChannelName(dto.getChannelName());
		po.setTotalAmount(dto.getTotalAmount());
		po.setParam5(dto.getParam5());
		po.setChannelId(dto.getChannelId());
		po.setParam6(dto.getParam6());
		po.setFreeAmount(dto.getFreeAmount());
		po.setStatus(dto.getStatus());
		
		po.setCreateTime(new Date());
		po.setUpdateTime(new Date());
		if(dto.getCreateTime()!=null) {
			po.setCreateTime(dto.getCreateTime());
		}
		if(dto.getUpdateTime()!=null) {
			po.setUpdateTime(dto.getUpdateTime());
		}
		rRecPayMapper.insert(po);
	}

	/**
	 * 2 更新支付记录
	 */
	public void updateRecPayByPayOrderNo(RecoRecPayDto dto) {
		RRecPayPo po = new RRecPayPo();
		po.setPayOrderNo(dto.getPayOrderNo());
		po.setMchId(dto.getMchId());
		po.setMchName(dto.getMchName());
		po.setAgentId(dto.getAgentId());
		po.setAgentName(dto.getAgentName());
		po.setThirdFreeAmount(dto.getThirdFreeAmount());
		po.setBuyerPayAmount(dto.getBuyerPayAmount());
		po.setContractId(dto.getContractId());
		po.setParam4(dto.getParam4());
		po.setPayWay(dto.getPayWay());
		po.setProductNo(dto.getProductNo());
		po.setParam2(dto.getParam2());
		po.setProductName(dto.getProductName());
		po.setPayOrderNo(dto.getPayOrderNo());
		po.setContractNo(dto.getContractNo());
		po.setReceiptAmount(dto.getReceiptAmount());
		po.setPayTime(dto.getPayTime());
		po.setChannelNo(dto.getChannelNo());
		po.setContractName(dto.getContractName());
		po.setOrderNo(dto.getOrderNo());
		po.setTradeNo(dto.getTradeNo());
		po.setFee(dto.getFee());
		po.setParam1(dto.getParam1());
		po.setParam3(dto.getParam3());
		po.setPayType(dto.getPayType());
		po.setContractType(dto.getContractType());
		po.setChannelName(dto.getChannelName());
		po.setTotalAmount(dto.getTotalAmount());
		po.setParam5(dto.getParam5());
		po.setChannelId(dto.getChannelId());
		po.setParam6(dto.getParam6());
		po.setFreeAmount(dto.getFreeAmount());
		po.setBuyerLogonId(dto.getBuyerLogonId());
		po.setBuyerUserId(dto.getBuyerUserId());
		po.setStatus(dto.getStatus());
		UpdateWrapper<RRecPayPo> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("pay_order_no", po.getPayOrderNo());
		
		po.setUpdateTime(new Date());
		if(dto.getUpdateTime()!=null) {
			po.setUpdateTime(dto.getUpdateTime());
		}
		rRecPayMapper.update(po, updateWrapper);
	}

	
	public static void main(String[] args) {
		CodeUtil.printPoGetterDto(RRecPayPo.class);
	}
}
