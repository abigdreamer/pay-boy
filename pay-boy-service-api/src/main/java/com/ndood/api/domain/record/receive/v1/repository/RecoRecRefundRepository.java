package com.ndood.api.domain.record.receive.v1.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ndood.api.domain.record.receive.v1.entity.dto.RecoRecRefundDto;
import com.ndood.common.base.dao.record.RRecRefundMapper;
import com.ndood.common.base.pojo.record.RRecRefundPo;

@Repository
public class RecoRecRefundRepository {

	@Autowired
	private RRecRefundMapper recRefundMapper;
	
	public void insertRecRefund(RecoRecRefundDto dto) {
		RRecRefundPo po = new RRecRefundPo();
		po.setMchId(dto.getMchId());
		po.setMchName(dto.getMchName());
		po.setAgentId(dto.getAgentId());
		po.setAgentName(dto.getAgentName());
		po.setRefundFreeAmount(dto.getRefundFreeAmount());
		po.setRefundBuyerPayAmount(dto.getRefundBuyerPayAmount());
		po.setRefundThirdFreeAmount(dto.getRefundThirdFreeAmount());
		po.setRefundReceiptAmount(dto.getRefundReceiptAmount());
		po.setProductName(dto.getProductName());
		po.setRefundOrderNo(dto.getRefundOrderNo());
		po.setContractType(dto.getContractType());
		po.setProductNo(dto.getProductNo());
		po.setOrderNo(dto.getOrderNo());
		po.setPayWay(dto.getPayWay());
		po.setContractId(dto.getContractId());
		po.setContractNo(dto.getContractNo());
		po.setPayType(dto.getPayType());
		po.setContractName(dto.getContractName());
		po.setChannelNo(dto.getChannelNo());
		po.setParam5(dto.getParam5());
		po.setRefundTime(dto.getRefundTime());
		po.setRefundAmount(dto.getRefundAmount());
		po.setParam1(dto.getParam1());
		po.setChannelId(dto.getChannelId());
		po.setParam3(dto.getParam3());
		po.setStatus(dto.getStatus());
		po.setParam4(dto.getParam4());
		po.setChannelName(dto.getChannelName());
		po.setParam2(dto.getParam2());
		po.setParam6(dto.getParam6());
		
		po.setCreateTime(new Date());
		po.setUpdateTime(new Date());
		if(dto.getCreateTime()!=null) {
			po.setCreateTime(dto.getCreateTime());
		}
		if(dto.getUpdateTime()!=null) {
			po.setUpdateTime(dto.getUpdateTime());
		}
		recRefundMapper.insert(po);
	}
	
	public void updateRecRefundById(RecoRecRefundDto dto) {
		RRecRefundPo po = new RRecRefundPo();
		po.setId(dto.getId());
		po.setMchId(dto.getMchId());
		po.setMchName(dto.getMchName());
		po.setAgentId(dto.getAgentId());
		po.setAgentName(dto.getAgentName());
		po.setRefundFreeAmount(dto.getRefundFreeAmount());
		po.setRefundBuyerPayAmount(dto.getRefundBuyerPayAmount());
		po.setRefundThirdFreeAmount(dto.getRefundThirdFreeAmount());
		po.setRefundReceiptAmount(dto.getRefundReceiptAmount());
		po.setProductName(dto.getProductName());
		po.setRefundOrderNo(dto.getRefundOrderNo());
		po.setContractType(dto.getContractType());
		po.setProductNo(dto.getProductNo());
		po.setOrderNo(dto.getOrderNo());
		po.setPayWay(dto.getPayWay());
		po.setContractId(dto.getContractId());
		po.setContractNo(dto.getContractNo());
		po.setPayType(dto.getPayType());
		po.setContractName(dto.getContractName());
		po.setChannelNo(dto.getChannelNo());
		po.setParam5(dto.getParam5());
		po.setRefundTime(dto.getRefundTime());
		po.setRefundAmount(dto.getRefundAmount());
		po.setParam1(dto.getParam1());
		po.setChannelId(dto.getChannelId());
		po.setParam3(dto.getParam3());
		po.setStatus(dto.getStatus());
		po.setParam4(dto.getParam4());
		po.setChannelName(dto.getChannelName());
		po.setParam2(dto.getParam2());
		po.setParam6(dto.getParam6());
		po.setUpdateTime(new Date());
		if(dto.getUpdateTime()!=null) {
			po.setUpdateTime(dto.getUpdateTime());
		}
		recRefundMapper.updateById(po);
	}
	
	public void updateRecRefundByRefundOrderNo(RecoRecRefundDto dto) {
		RRecRefundPo po = new RRecRefundPo();
		po.setId(dto.getId());
		po.setMchId(dto.getMchId());
		po.setMchName(dto.getMchName());
		po.setAgentId(dto.getAgentId());
		po.setAgentName(dto.getAgentName());
		po.setRefundFreeAmount(dto.getRefundFreeAmount());
		po.setRefundBuyerPayAmount(dto.getRefundBuyerPayAmount());
		po.setRefundThirdFreeAmount(dto.getRefundThirdFreeAmount());
		po.setRefundReceiptAmount(dto.getRefundReceiptAmount());
		po.setProductName(dto.getProductName());
		po.setRefundOrderNo(dto.getRefundOrderNo());
		po.setContractType(dto.getContractType());
		po.setProductNo(dto.getProductNo());
		po.setOrderNo(dto.getOrderNo());
		po.setPayWay(dto.getPayWay());
		po.setContractId(dto.getContractId());
		po.setContractNo(dto.getContractNo());
		po.setPayType(dto.getPayType());
		po.setContractName(dto.getContractName());
		po.setChannelNo(dto.getChannelNo());
		po.setParam5(dto.getParam5());
		po.setRefundTime(dto.getRefundTime());
		po.setRefundAmount(dto.getRefundAmount());
		po.setParam1(dto.getParam1());
		po.setChannelId(dto.getChannelId());
		po.setParam3(dto.getParam3());
		po.setStatus(dto.getStatus());
		po.setParam4(dto.getParam4());
		po.setChannelName(dto.getChannelName());
		po.setParam2(dto.getParam2());
		po.setParam6(dto.getParam6());
		po.setUpdateTime(new Date());
		if(dto.getUpdateTime()!=null) {
			po.setUpdateTime(dto.getUpdateTime());
		}
		UpdateWrapper<RRecRefundPo> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("refund_order_no", po.getRefundOrderNo());
		recRefundMapper.update(po, updateWrapper);
	}
	
}
