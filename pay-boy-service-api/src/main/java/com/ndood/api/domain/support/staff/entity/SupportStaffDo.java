package com.ndood.api.domain.support.staff.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * 支持服务员工领域对象
 */
@Getter
@Setter
public class SupportStaffDo {
	
	/**
	 * ID
	 */
	private Integer id;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 手机
	 */
	private String mobile;
	/**
	 * 昵称
	 */
	private String nickName;
	/**
	 * 是否收银员
	 */
	private Boolean isCashier;
}
