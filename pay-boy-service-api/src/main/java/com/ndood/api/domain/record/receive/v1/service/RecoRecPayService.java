package com.ndood.api.domain.record.receive.v1.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.api.app.properties.ApiProperties;
import com.ndood.api.base.dao.record_receive.v1.ExtendRecoRecPayDao;
import com.ndood.api.domain.record.receive.v1.entity.dto.ReconAppDto;
import com.ndood.api.domain.support.merchant.service.SupportMchRecConfigService;
import com.ndood.common.base.constaints.CommonConstaints;
import com.ndood.common.base.util.DateUtil;

@Service
public class RecoRecPayService {
	@Autowired
	private SupportMchRecConfigService supportMchRecConfigService;
	
	@Autowired
	private ApiProperties apiProperties;
	
	@Autowired
	private ExtendRecoRecPayDao extendRecoRecPayDao;
	
	/**
	 * 获取昨天交易流水中的所有APPID信息集合
	 */
	public List<ReconAppDto> getLastDayRecAppList() throws ParseException {
		// Step1: 获取支付宝交易流水中所有Appid
		Date endDate = DateUtil.getCurrentDay();
		Date startDate = DateUtil.getBeforeDate(endDate, 1, 0, 0, 0);
		List<String> appIdList = extendRecoRecPayDao.extendDistinctFindAppidsByTimeZoneAndPayWay(startDate, endDate, CommonConstaints.REC_PAY_WAY_ALI);
		if(appIdList.isEmpty()) {
			return new ArrayList<ReconAppDto>();
		}
		
		// Step2: 获取商户支付配置
		List<ReconAppDto> alipayAppList = supportMchRecConfigService.getMerchantOfficialAlipayReceiveAppList(StringUtils.join(appIdList,","));
		
		// Step3: 添加直清配置
		String appId = apiProperties.getPay().getAlipayAppId();
		String alipayPublicRsa2Secret = apiProperties.getPay().getAlipayRsa2PublicSecret();
		String mchPrivateRsa2Secret = apiProperties.getPay().getAlipayMchRsa2PrivateSecret();
		String mchId = apiProperties.getPay().getAlipayMchId();
		if(appIdList.contains(appId)) {
			ReconAppDto app = new ReconAppDto();
			app.setMchId(mchId);
			app.setAppId(appId);
			app.setAlipayPublicRsa2Secret(alipayPublicRsa2Secret);
			app.setMchPrivateRsa2Secret(mchPrivateRsa2Secret);
			alipayAppList.add(app);
		}
		return alipayAppList;
	}
}
