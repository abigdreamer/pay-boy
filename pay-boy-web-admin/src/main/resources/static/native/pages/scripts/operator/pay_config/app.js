/**
 * operator_pay_config_app_page -> operator_pap
 * operator_pay_config_app_update -> operator_pau
 * operator_pay_config_app_add -> operator_paa
 * @returns
 */
define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'bootstrap-table',
	'bootstrap-table-zh-CN',
	'bootstrap-table-mobile',
], function() {
	return {
		initUpdateDialog: function(){
			// 表单验证
			$("#operator_pau").find("._form").validate({
				rules: {
				},
				messages:{
				},
			    errorPlacement: function(error,element) {
			    	$(element).parent().addClass('has-error');
			        $(element).after(error);
			    },
				success:function(element) {
			        $(element).parent().removeClass('has-error');
			        $(element).parent().find('label').remove();
			    },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		},
		initAddDialog: function(){
			// 表单验证
			$("#operator_paa").find("._form").validate({
				rules: {
				},
				messages:{
				},
			    errorPlacement: function(error,element) {
			    	$(element).parent().addClass('has-error');
			        $(element).after(error);
			    },
				success:function(element) {
			        $(element).parent().removeClass('has-error');
			        $(element).parent().find('label').remove();
			    },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		},
		initPage: function(){
			// 1 初始化表单控件
			$("#operator_pap").find("._datatable").bootstrapTable({
				url : "/operator/pay_config/app/query", 
				method : 'post', 
				toolbar : '#operator_pap ._toolbar',
				uniqueId: "id",
				striped: true,
				cache: false,
				pagination : true, 
				sortable: false,
				singleSelect : false,
				search: true,
				showColumns: true,
				showToggle:false,
				cardView: false,
				showRefresh: true,
				clickToSelect: true,
				minimumCountColumns: 5,
				queryParams : function(params) {
					return {
						limit : params.limit,
						offset : params.offset,
						keywords: params.search,
						sortName:'sort'
					};
				},
				sidePagination : "server",
				pageNumber:1, 
				pageSize : 10,
				pageList: [10, 20, 30],
				columns : [
					{
						checkbox : true
					},
					{
						field : 'id',
						title : '编号'
					},
					{
						field : 'appNo',
						title : '编码'
					},
					{
						field : 'name',
						title : '应用名称'
					},
					{
						field : 'limit',
						title : '限制数量'
					},
					{
						field : 'sort',
						title : '排序',
					},
					{
						field : 'status',
						title : '状态',
						formatter: function(value, row, index){
							if(value==0){
								return '<span class="text-danger"><i class="fa fa-circle"></i> 禁用</span>';
							}else{
								return '<span class="text-success"><i class="fa fa-circle"></i> 启用</span>';
							}
						}
					},
					{
						visible: false,
						field : 'createTime',
						title : '创建时间'
					},
					{
						visible: false,
						field : 'updateTime',
						title : '最后修改时间'
					},
					{
						title : '操作',
						field : '',
						align : 'center',
						formatter : function(value, row, index) {
							var b1 = '<a href="javascript:void(0);" class="btn btn-xs btn-success btn-editone" onclick="javascript:operator_pap.open_update_dialog('+row['id']+')"><i class="fa fa-pencil"></i></a>';
							var b2 = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="javascript:operator_pap.delete('+row['id']+')"><i class="fa fa-trash"></i></a>';
							return b1 + b2;
						}
					} 
				]
			});
			
			// 初始化手机响应式
			var docW = window.innerWidth;
			if (docW < 768) {
				$("#operator_pap").find("._search_plus").show();
				$("#operator_pap").find("._search_plus_btn").hide();
				$("#operator_pap .bootstrap-table .fixed-table-toolbar input[type='text']").hide();
			}else{
				$("#operator_pap").find("._search_plus").hide();
			}
		},
		open_add_dialog: function(){
			var docW = window.innerWidth;
			var area = [ '100%', '100%' ];
			var maxmin = false;
			if (docW >= 768) {
				area = [ '800px', '600px' ];
				maxmin = true;
			}
			var i = layer.open({
				type : 2,
				title : '添加应用',
				maxmin : maxmin,
				shadeClose : false,
				area : area,
				content : '/operator/pay_config/app/add',
				btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
				btn1 : function(index) {
					var loadindex = layer.load(1, {time: 5*1000});	
					$("#layui-layer-iframe" + i)[0].contentWindow.operator_paa.add();
					layer.close(loadindex);
				},
				btn2 : function(index) {
					layer.close(i);
				}
			});
			if (docW >= 768) {
				layer.restore(i);
			}else{
				layer.full(i);
			}
		},
		delete: function(id){
			var that = this;
			layer.confirm('确定要删除吗？', {
				btn : [ '确定', '取消' ]
			}, function() {
				$.ajax({
					url : '/operator/pay_config/app/delete',
					type : "post",
					data : {'id' : id},
					success : function(data) {
						if (data.code != '10000') {
							layer.alert(data.msg,{icon: 2});
							return;
						}
						layer.alert(data.msg, {icon: 1});
						that.reload();
					},
			        error: function (response, ajaxOptions, thrownError) {
			        	errorCallBack(response, ajaxOptions, thrownError);                
			        }
				});
			})
		},
		batch_delete: function(){
			var that = this;
			var rows = $("#operator_pap").find("._datatable").bootstrapTable('getSelections'); 
			if (rows.length == 0) {
				layer.alert("请至少选择一条记录!",{icon: 7});
				return;
			}
			layer.confirm("确定要删除选中的'" + rows.length + "'条记录吗?", {
				btn : [ '确定', '取消' ]
			}, function() {
				var ids = [];
				$.each(rows, function(i, row) {
					ids.push(row['id']);
				});
				$.ajax({
					type : 'post',
					data : {ids:ids},
					dataType : 'json',
					url : '/operator/pay_config/app/batch_delete',
					success : function(data) {
						if (data.code != '10000') {
							layer.alert(data.msg,{icon: 2});
							return;
						}
						layer.alert(data.msg, {icon: 1});
						that.reload();
					},
			        error: function (response, ajaxOptions, thrownError) {
			        	errorCallBack(response, ajaxOptions, thrownError);                
			        }
				});
			}, function() {});
		},
		// 修改应用
		open_update_dialog: function (id){
			var docW = window.innerWidth;
			var area = [ '100%', '100%' ];
			var maxmin = false;
			if (docW >= 768) {
				area = [ '800px', '600px' ];
				maxmin = true;
			}
			var i = layer.open({
				type : 2,
				title : '修改应用',
				maxmin : maxmin,
				shadeClose : false,
				area : area,
				content : '/operator/pay_config/app/update?id='+id,
				btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
				btn1 : function(index) {
					var loadindex = layer.load(1, {time: 5*1000});
					$("#layui-layer-iframe" + i)[0].contentWindow.operator_pau.update();
					layer.close(loadindex);
				},
				btn2 : function(index) {
					layer.close(i);
				}
			});
			if (docW >= 768) {
				layer.restore(i);
			}else{
				layer.full(i);
			}
		},
		// 重新加载datatable
		reload: function (){
			$("#operator_pap").find("._datatable").bootstrapTable('refresh', null);
		},
		//保存地区
		add: function (){
			// Step1: 表单验证
			var valid = $("#operator_paa").find("._form").valid();
			if(!valid){
				return;
			}
			
			// Step2: 获取输入数据，拼装成请求数据
			var data = $("#operator_paa").find("._form").serializeJson();
			
			// Step3: 提交表单
		    $.ajax({
		    	url:"/operator/pay_config/app/add",
		        type:"post",
		        dataType:'json',
		        contentType:"application/json;charset=utf-8",
		        data:JSON.stringify(data),
		        async:false,
		        success:function(data){
		        	if(data.code!='10000'){
		        		parent.layer.alert(data.msg, {icon: 2});
		        		var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
		    			parent.layer.close(index);
		        		return;
		        	}
		        	parent.layer.alert(data.msg, {icon: 1});
		        	parent.operator_pap.reload();
		        	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
					parent.layer.close(index);
		        },
		        error: function (response, ajaxOptions, thrownError) {
		        	errorCallBack(response, ajaxOptions, thrownError);                
		        }
		    });
		},
		//保存应用
		update: function (){
			// Step1: 表单验证
			var valid =  $("#operator_pau").find("._form").valid();
			if(!valid){
				return;
			}
			
			// Step2: 获取输入数据，拼装成请求数据
			var data = $("#operator_pau").find("._form").serializeJson();
			
			// Step3: 提交表单
		    $.ajax({
		    	url:"/operator/pay_config/app/update",
		        type:"post",
		        dataType:'json',
		        contentType:"application/json;charset=utf-8",
		        data:JSON.stringify(data),
		        async:false,
		        success:function(data){
		        	if(data.code!='10000'){
		        		parent.layer.alert(data.msg, {icon: 2});
		        		var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
		    			parent.layer.close(index);
		        		return;
		        	}
		        	parent.layer.alert(data.msg, {icon: 1});
		        	parent.operator_pap.reload();
		        	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
					parent.layer.close(index);
		        },
		        error: function (response, ajaxOptions, thrownError) {
		        	errorCallBack(response, ajaxOptions, thrownError);                
		        }
		    });
		}
	}
});
