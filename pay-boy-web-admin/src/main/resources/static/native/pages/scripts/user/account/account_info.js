define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'bootstrap-datepicker',
	'bootstrap-datepicker-zh-CN',
	'bootstrap-imguploader',
	'jquery-distselector'
], function() {
	return {
		initPage: function(){
			// 图片上传插件
			$("#account_info_head_image").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				imgName: 'headImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 表单验证
			$("#account_info_form").validate({
				rules: {
					nickName:{
						required: true,
						rangelength:[2,18],
						isNickName:true,
					},
					mobile:{
						required: true,
						isMobile: true,
					},
					email:{
						required: true,
						isEmail: true,
					},
				},
				messages:{
					nickName:{
						required: "用户昵称不能为空.",
						rangelength: $.validator.format("昵称最小长度:{0}, 最大长度:{1}。"),
						isNickName:"用户名必须由字母、数字或特殊符号(@_.)组成.",
					},
					mobile:{
						required: "手机号不能为空.",
						isMobile: "请正确填写您的手机号码.",
					},
					email:{
						required: "邮箱不能为空.",
						isEmail: "请正确填写你的邮箱.",
					},
				},
				errorPlacement: function(error,element) {
					$(element).parent().addClass('has-error');
					$(element).after(error);
				},
				success:function(element) {
					$(element).parent().removeClass('has-error');
					$(element).parent().find('label.error').remove();
				},
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		}
	}
});

// 更新账户信息
function update_account_info(){
	// Step1: 表单验证
	var valid = $("#account_info_form").valid();
	if(!valid){
		return;
	}
	var data = $('#account_info_form').serializeJson();
	var arr = [];
	data['roleIds'] = arr;
	
	// Step2: 提交表单
    $.ajax({
    	url:"/user/account/info/update",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:true,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 1});
        		return;
        	}
        	Layout.reloadAjaxContent('/jump_success?url=/user/account/info');
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}