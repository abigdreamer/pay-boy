package com.ndood.admin.repository.system;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.StaffPo;

/**
 * 用户dao
 * @author ndood
 */
public interface StaffRepository extends JpaRepository<StaffPo, Integer>{
	
	Page<StaffPo> findAll(Specification<StaffPo> specification, Pageable pageable);

	StaffPo findByMobileAndStatus(String username, Integer status);

	StaffPo findByEmailAndStatus(String email, Integer status);
	
}
