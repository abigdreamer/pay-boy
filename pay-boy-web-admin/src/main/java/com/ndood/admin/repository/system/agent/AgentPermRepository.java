package com.ndood.admin.repository.system.agent;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.agent.AgentPermPo;

/**
 * https://blog.jooq.org/tag/sqlresultsetmapping/
 */
public interface AgentPermRepository extends JpaRepository<AgentPermPo, Integer> {

	List<AgentPermPo> findByOrderBySort();
	
	List<AgentPermPo> findByParent(AgentPermPo po);
}
