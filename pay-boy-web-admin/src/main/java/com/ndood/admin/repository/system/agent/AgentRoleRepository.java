package com.ndood.admin.repository.system.agent;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.agent.AgentRolePo;

/**
 * 角色dao
 */
public interface AgentRoleRepository extends JpaRepository<AgentRolePo, Integer>{

	Page<AgentRolePo> findAll(Specification<AgentRolePo> specification, Pageable pageable);

	List<AgentRolePo> findByStatusEqualsOrderBySort(Integer status);
	
}
