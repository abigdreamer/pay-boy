package com.ndood.admin.repository.operator.pay_config.app;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.operator.pay_config.app.AppPo;

/**
 * 应用dao
 */
public interface AppRepository extends JpaRepository<AppPo, Integer>{

	Page<AppPo> findAll(Specification<AppPo> specification, Pageable pageable);

	List<AppPo> findByStatusEqualsOrderBySort(Integer status);
	
}
