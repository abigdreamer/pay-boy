package com.ndood.admin.repository.system;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.RolePo;

/**
 * 角色dao
 */
public interface RoleRepository extends JpaRepository<RolePo, Integer>{

	Page<RolePo> findAll(Specification<RolePo> specification, Pageable pageable);

	List<RolePo> findByStatusEqualsOrderBySort(Integer status);
	
}
