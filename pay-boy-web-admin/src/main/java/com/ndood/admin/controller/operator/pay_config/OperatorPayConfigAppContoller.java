package com.ndood.admin.controller.operator.pay_config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.operator.pay_config.app.AppPo;
import com.ndood.admin.pojo.operator.pay_config.app.dto.AppDto;
import com.ndood.admin.pojo.operator.pay_config.app.query.AppQuery;
import com.ndood.admin.service.operator.OperatorPayConfigAppService;

/**
 * 运营子系统，支付设置，应用管理控制类
 * @author ndood
 *
 */
@Controller
public class OperatorPayConfigAppContoller {

	@Autowired
	private OperatorPayConfigAppService operatorPayConfigAppService;
	
	/**
	 * 显示应用页
	 */
	@GetMapping("/operator/pay_config/app")
	public String toAppPage(){
		return "operator/pay_config/app/app_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/operator/pay_config/app/add")
	public String toAddApp(){
		return "operator/pay_config/app/app_add";
	}
	
	/**
	 * 添加应用
	 */
	@PostMapping("/operator/pay_config/app/add")
	@ResponseBody
	public AdminResultVo addApp(@RequestBody AppDto app) throws Exception{
		operatorPayConfigAppService.addApp(app);
		return AdminResultVo.ok().setMsg("添加应用成功！");
	}
	
	/**
	 * 删除应用
	 */
	@PostMapping("/operator/pay_config/app/delete")
	@ResponseBody
	public AdminResultVo deleteApp(Integer id){
		Integer[] ids = new Integer[]{id};
		operatorPayConfigAppService.batchDeleteApp(ids);
		return AdminResultVo.ok().setMsg("删除应用成功！");
	}

	/**
	 * 添加应用
	 */
	@PostMapping("/operator/pay_config/app/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteApp(@RequestParam("ids[]") Integer[] ids){
		operatorPayConfigAppService.batchDeleteApp(ids);
		return AdminResultVo.ok().setMsg("批量删除应用成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/operator/pay_config/app/update")
	public String toUpdateApp(Integer id, Model model){
		AppPo app = operatorPayConfigAppService.getApp(id);
		model.addAttribute("app", app);
		return "operator/pay_config/app/app_update";
	}
	
	/**
	 * 修改应用
	 */
	@PostMapping("/operator/pay_config/app/update")
	@ResponseBody
	public AdminResultVo updateApp(@RequestBody AppDto app) throws Exception{
		operatorPayConfigAppService.updateApp(app);
		return AdminResultVo.ok().setMsg("修改应用成功！");
	}
	
	/**
	 * 应用列表
	 * @throws Exception 
	 */
	@PostMapping("/operator/pay_config/app/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody AppQuery query) throws Exception{
		DataTableDto page = operatorPayConfigAppService.pageAppList(query);
		return page;
	}
	
}
