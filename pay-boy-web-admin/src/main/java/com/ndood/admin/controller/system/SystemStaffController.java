package com.ndood.admin.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.RolePo;
import com.ndood.admin.pojo.system.StaffPo;
import com.ndood.admin.pojo.system.dto.RoleDto;
import com.ndood.admin.pojo.system.dto.StaffDto;
import com.ndood.admin.pojo.system.query.StaffQuery;
import com.ndood.admin.service.system.SystemRoleService;
import com.ndood.admin.service.system.SystemStaffService;

/**
 * 用户控制器类
 * @author ndood
 */
@Controller
public class SystemStaffController {
	@Autowired
	private SystemStaffService systemStaffService;
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	/**
	 * 显示用户页
	 * 坑，如果GetMapping不写value，有可能访问不到静态资源
	 */
	@GetMapping("/system/staff")
	public String toStaffPage(){
		return "system/staff/staff_page";
	}
	
	/**
	 * 跳转到添加页
	 * @throws Exception 
	 */
	@GetMapping("/system/staff/add")
	public String toAddStaff(Model model) throws Exception{
		// Step1: 查询出所有角色
		List<RoleDto> roles = systemRoleService.getRoles();
		model.addAttribute("roles", roles);
		return "system/staff/staff_add";
	}
	
	/**
	 * 添加用户
	 * @throws Exception 
	 */
	@PostMapping("/system/staff/add")
	@ResponseBody
	public AdminResultVo addStaff(@RequestBody StaffDto staff) throws Exception{
		systemStaffService.addStaff(staff);
		return AdminResultVo.ok().setMsg("添加用户成功！");
	}
	
	/**
	 * 删除用户
	 */
	@PostMapping("/system/staff/delete")
	@ResponseBody
	public AdminResultVo deleteStaff(Integer id){
		Integer[] ids = new Integer[]{id};
		systemStaffService.batchDeleteStaff(ids);
		return AdminResultVo.ok().setMsg("删除用户成功！");
	}

	/**
	 * 批量删除用户
	 */
	@PostMapping("/system/staff/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteStaff(@RequestParam("ids[]") Integer[] ids){
		systemStaffService.batchDeleteStaff(ids);
		return AdminResultVo.ok().setMsg("批量删除用户成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/staff/update")
	public String toUpdateStaff(Integer id, Model model) throws Exception{
		StaffPo staff = systemStaffService.getStaff(id);
		List<RoleDto> roles = systemRoleService.getRoles();
		for (RoleDto role : roles) {
			for (RolePo urole : staff.getRoles()) {
				if(urole.getId()==role.getId()) {
					role.setIsInChecked(true);
					break;
				}
			}
		}
		model.addAttribute("roles", roles);
		model.addAttribute("staff", staff);
		return "system/staff/staff_update";
	}
	
	/**
	 * 修改用户
	 * @throws Exception 
	 */
	@PostMapping("/system/staff/update")
	@ResponseBody
	public AdminResultVo updateStaff(@RequestBody StaffDto staff) throws Exception{
		systemStaffService.updateStaff(staff);
		return AdminResultVo.ok().setMsg("修改用户成功！");
	}
	
	/**
	 * 用户列表
	 * @throws Exception 
	 */
	@PostMapping("/system/staff/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody StaffQuery query) throws Exception{
		DataTableDto page = systemStaffService.pageStaffList(query);
		return page;
	}
}
