package com.ndood.admin.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.PermPo;
import com.ndood.admin.pojo.system.dto.PermDto;
import com.ndood.admin.service.system.SystemPermService;

/**
 * 资源控制器类
 * @author ndood
 */
@Controller
public class SystemPermController {
	
	@Autowired
	private SystemPermService systemPermService;

	/**
	 * 显示资源页
	 */
	/*@PreAuthorize("hasAuthority('aaa')")*/
	@GetMapping("/system/perm")
	public String toPermPage(){
		return "system/perm/perm_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/perm/add")
	public String toAddPerm(Integer parentId,String parentName, Model model){
		if(!StringUtils.isEmpty(parentId)&&!StringUtils.isEmpty(parentName)){
			model.addAttribute("parentId", parentId);
			model.addAttribute("parentName", parentName);
		}
		return "system/perm/perm_add";
	}
	
	/**
	 * 添加资源
	 */
	@PostMapping("/system/perm/add")
	@ResponseBody
	public AdminResultVo addPerm(@RequestBody PermDto perm) throws Exception{
		PermDto obj = systemPermService.addPerm(perm);
		return AdminResultVo.ok().setData(obj).setMsg("添加资源成功！");
	}
	
	/**
	 * 展示所有资源
	 */
	@GetMapping("/system/perm/treeview")
	@ResponseBody
	public List<PermDto> getPermTreeview() throws Exception{
		List<PermDto> permList = systemPermService.getPermList();
		return permList;
	}
	
	/**
	 * 删除资源
	 */
	@PostMapping("/system/perm/delete")
	@ResponseBody
	public AdminResultVo deletePerm(Integer id){
		Integer[] ids = new Integer[]{id};
		systemPermService.batchDeletePerm(ids);
		return AdminResultVo.ok().setMsg("删除资源成功！");
	}
	
	/**
	 * 批量删除资源
	 */
	@PostMapping("/system/perm/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeletePerm(@RequestParam("ids[]") Integer[] ids){
		systemPermService.batchDeletePerm(ids);
		return AdminResultVo.ok().setMsg("批量删除资源成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/perm/update")
	public String toUpdatePerm(Integer id, Model model) throws Exception{
		PermPo perm = systemPermService.getPerm(id);
		model.addAttribute("perm", perm);
		return "system/perm/perm_update";
	}
	
	/**
	 * 修改资源
	 */
	@PostMapping("/system/perm/update")
	@ResponseBody
	public AdminResultVo updatePerm(@RequestBody PermDto perm) throws Exception{
		PermDto obj = systemPermService.updatePerm(perm);
		return AdminResultVo.ok().setData(obj).setMsg("修改资源成功！");
	}
}
