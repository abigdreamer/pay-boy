package com.ndood.admin.controller.system.agent;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.agent.AgentRolePo;
import com.ndood.admin.pojo.system.agent.dto.AgentRoleDto;
import com.ndood.admin.pojo.system.agent.dto.AgentTreeDto;
import com.ndood.admin.pojo.system.agent.query.AgentRoleQuery;
import com.ndood.admin.service.system.agent.SystemAgentRoleService;

/**
 * 角色控制器类
 * @author ndood
 */
@Controller
public class SystemAgentRoleController {
	
	@Autowired
	private SystemAgentRoleService systemAgentRoleService;
	
	/**
	 * 显示角色页
	 */
	@GetMapping("/system/agent/role")
	public String toRolePage(){
		return "system/agent/role/role_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/agent/role/add")
	public String toAddRole(){
		return "system/agent/role/role_add";
	}
	
	/**
	 * 添加角色
	 */
	@PostMapping("/system/agent/role/add")
	@ResponseBody
	public AdminResultVo addRole(@RequestBody AgentRoleDto role) throws Exception{
		systemAgentRoleService.addRole(role);
		return AdminResultVo.ok().setMsg("添加角色成功！");
	}
	
	/**
	 * 删除角色
	 */
	@PostMapping("/system/agent/role/delete")
	@ResponseBody
	public AdminResultVo deleteRole(Integer id){
		Integer[] ids = new Integer[]{id};
		systemAgentRoleService.batchDeleteRole(ids);
		return AdminResultVo.ok().setMsg("删除角色成功！");
	}

	/**
	 * 添加角色
	 */
	@PostMapping("/system/agent/role/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteRole(@RequestParam("ids[]") Integer[] ids){
		systemAgentRoleService.batchDeleteRole(ids);
		return AdminResultVo.ok().setMsg("批量删除角色成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/agent/role/update")
	public String toUpdateRole(Integer id, Model model){
		AgentRolePo role = systemAgentRoleService.getRole(id);
		model.addAttribute("role", role);
		return "system/agent/role/role_update";
	}
	
	/**
	 * 修改角色
	 * @throws Exception 
	 */
	@PostMapping("/system/agent/role/update")
	@ResponseBody
	public AdminResultVo updateRole(@RequestBody AgentRoleDto role) throws Exception{
		systemAgentRoleService.updateRole(role);
		return AdminResultVo.ok().setMsg("修改角色成功！");
	}
	
	/**
	 * 角色列表
	 * @throws Exception 
	 */
	@PostMapping("/system/agent/role/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody AgentRoleQuery query) throws Exception{
		DataTableDto page = systemAgentRoleService.pageRoleList(query);
		return page;
	}
	
	/**
	 * 资源树
	 * @param roleId
	 * @return
	 */
	@PostMapping("/system/agent/role/role_permission_tree")
	@ResponseBody
	public AdminResultVo getPermissionTree(Integer roleId){
		List<AgentTreeDto> list = systemAgentRoleService.getPermissionTree(roleId);
		return AdminResultVo.ok().setData(list).setMsg("获取角色权限树成功！");
	}
}
