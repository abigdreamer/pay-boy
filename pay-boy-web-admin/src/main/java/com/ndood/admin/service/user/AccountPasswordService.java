package com.ndood.admin.service.user;

import com.ndood.admin.core.exception.AdminException;

public interface AccountPasswordService {

	/**
	 * 修改密码
	 */
	void changePassword(String username, String oldPassword, String newPassword) throws AdminException;

}
