package com.ndood.admin.service.system;

import java.util.List;

import com.ndood.admin.pojo.system.dto.PermDto;

/**
 * 资源管理业务接口
 * @author ndood
 */
public interface SystemPermService {

	/**
	 * 获取资源tree列表
	 * @param keywords 
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	List<PermDto> getPermList() throws Exception;

	/**
	 * 批量删除资源
	 * @param ids
	 */
	void batchDeletePerm(Integer[] ids);

	/**
	 * 添加资源
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	PermDto addPerm(PermDto dept) throws Exception;

	/**
	 * 获取资源信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	PermDto getPerm(Integer id) throws Exception;

	/**
	 * 更新资源信息
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	PermDto updatePerm(PermDto dept) throws Exception;

}
