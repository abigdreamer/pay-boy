package com.ndood.admin.service.system;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.dto.StaffDto;
import com.ndood.admin.pojo.system.query.StaffQuery;

/**
 * 用户管理业务接口
 * @author ndood
 */
public interface SystemStaffService {

	/**
	 * 添加用户
	 */
	String addStaff(StaffDto user) throws Exception;

	/**
	 * 批量删除用户
	 */
	void batchDeleteStaff(Integer[] ids);

	/**
	 * 更新用户
	 */
	void updateStaff(StaffDto user) throws Exception;

	/**
	 * 获取单个用户
	 */
	StaffDto getStaff(Integer id) throws Exception;

	/**
	 * 获取用户列表
	 */
	DataTableDto pageStaffList(StaffQuery query) throws Exception;

}
