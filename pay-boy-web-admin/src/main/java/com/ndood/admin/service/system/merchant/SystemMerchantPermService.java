package com.ndood.admin.service.system.merchant;

import java.util.List;

import com.ndood.admin.pojo.system.merchant.dto.MerchantPermDto;

/**
 * 资源管理业务接口
 * @author ndood
 */
public interface SystemMerchantPermService {

	/**
	 * 获取资源tree列表
	 * @param keywords 
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	List<MerchantPermDto> getPermList() throws Exception;

	/**
	 * 批量删除资源
	 * @param ids
	 */
	void batchDeletePerm(Integer[] ids);

	/**
	 * 添加资源
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	MerchantPermDto addPerm(MerchantPermDto dto) throws Exception;

	/**
	 * 获取资源信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	MerchantPermDto getPerm(Integer id) throws Exception;

	/**
	 * 更新资源信息
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	MerchantPermDto updatePerm(MerchantPermDto dto) throws Exception;

}
