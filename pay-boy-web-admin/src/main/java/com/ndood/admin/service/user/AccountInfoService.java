package com.ndood.admin.service.user;

import com.ndood.admin.pojo.system.dto.StaffDto;

public interface AccountInfoService {

	/**
	 * 获取账户信息
	 */
	StaffDto getAccountInfoById(String userId) throws Exception;
	/**
	 * 更新账户信息
	 */
	void updateAccountInfo(StaffDto user) throws Exception;

}
