package com.ndood.admin.service.system;
import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.dto.JobDto;
import com.ndood.admin.pojo.system.query.JobQuery;
/**
 * 定时任务
 */
public interface SystemJobService {
	/**
	 * 获取任务列表
	 */
	DataTableDto pageUserList(JobQuery query);
	/**
	 * 添加定时任务
	 */
	void addJob(JobDto job) throws Exception;
	/**
	 * 批量删除任务
	 */
	void batchDeleteJob(Integer[] ids);
	/**
	 * 获取定时任务
	 */
	JobDto getJob(Integer id) throws Exception;
	/**
	 * 更新定时任务
	 */
	void updateJob(JobDto job) throws Exception;
	/**
	 * 更新定时任务状态
	 */
	void changeJobStatus(Integer id, String status) throws Exception;
	/**
	 * 项目启动时启动状态为1的所有job
	 */
	void initJobs();
	/**
	 * 更新定时周期表达式
	 */
	void updateCrontabRule(JobDto dto) throws Exception;
	
}