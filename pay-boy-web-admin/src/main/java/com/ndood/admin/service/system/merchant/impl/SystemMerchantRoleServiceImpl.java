package com.ndood.admin.service.system.merchant.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ndood.admin.core.constaints.AdminConstaints;
import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.merchant.MerchantPermPo;
import com.ndood.admin.pojo.system.merchant.MerchantRolePo;
import com.ndood.admin.pojo.system.merchant.dto.MerchantRoleDto;
import com.ndood.admin.pojo.system.merchant.dto.MerchantTreeDto;
import com.ndood.admin.pojo.system.merchant.query.MerchantRoleQuery;
import com.ndood.admin.repository.system.merchant.MerchantPermRepository;
import com.ndood.admin.repository.system.merchant.MerchantRoleRepository;
import com.ndood.admin.service.system.merchant.SystemMerchantRoleService;
import com.ndood.common.base.util.JPAUtil;

/**
 * 角色模块业务类
 * @author ndood
 */
@Service
public class SystemMerchantRoleServiceImpl implements SystemMerchantRoleService {

	@Autowired
	private MerchantRoleRepository merchantRoleDao;
	
	@Autowired
	private MerchantPermRepository merchantPermissionDao;
	
	@Override
	public void addRole(MerchantRoleDto dto) throws Exception {
		// Step1: 补充相关默认字段
		dto.setCreateTime(new Date());
		dto.setUpdateTime(new Date());
		
		// Step2: 保存角色
		MerchantRolePo po = new MerchantRolePo();
		List<Integer> resourceIds = dto.getResourceIds();
		List<MerchantPermPo> list = new ArrayList<MerchantPermPo>();
		for (Integer resourceId : resourceIds) {
			MerchantPermPo temp = new MerchantPermPo();
			temp.setId(resourceId);
			list.add(temp);
		}
		JPAUtil.childToFather(dto, po);
		// 设置复杂属性
		po.setPerms(list);
		merchantRoleDao.save(po);
	}

	@Override
	public void batchDeleteRole(Integer[] ids) {
		// merchantRoleDao.deleteByIdByIds(Arrays.asList(ids));
		for (Integer id : ids) {
			merchantRoleDao.deleteById(id);
		}
	}

	@Override
	public void updateRole(MerchantRoleDto dto) throws Exception {
		// Step1: 删除旧角色值
		MerchantRolePo po = merchantRoleDao.findById(dto.getId()).get();
		
		// Step2: 保存新的值
		List<Integer> resourceIds = dto.getResourceIds();
		List<MerchantPermPo> list = new ArrayList<MerchantPermPo>();
		for (Integer resourceId : resourceIds) {
			MerchantPermPo temp = new MerchantPermPo();
			temp.setId(resourceId);
			list.add(temp);
		}
		JPAUtil.childToFather(dto, po);
		// 设置特殊属性
		po.setPerms(list);
		merchantRoleDao.save(po);
	}
	
	@Override
	public MerchantRolePo getRole(Integer id) {
		return merchantRoleDao.findById(id).get();
	}

	@Override
	public DataTableDto pageRoleList(MerchantRoleQuery query) throws Exception {
		// Step1: 封装查询参数
		Integer pageSize = query.getLimit();
		String keywords = query.getKeywords();
		Integer pageNo = query.getPageNo();
		
		List<Sort.Order> orders = new ArrayList<Sort.Order>();
		Sort.Order order = new Sort.Order(Sort.Direction.ASC, "sort");
		orders.add(order);
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(orders));

		// Step2: 进行复杂查询
		@SuppressWarnings("serial")
		Page<MerchantRolePo> page = merchantRoleDao.findAll(new Specification<MerchantRolePo>() {
			@Override
			public Predicate toPredicate(Root<MerchantRolePo> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				Predicate p = cb.conjunction();
				if (!StringUtils.isEmpty(keywords)) {
					Predicate temp = cb.or(
						cb.like(root.get("name"), "%" + keywords + "%"),
						cb.like(root.get("desc"), "%" + keywords + "%"));
					p = cb.and(p,temp);
				}
				return p;
			}
		}, pageable);
		
		// Step3: 转换成dto
		List<MerchantRoleDto> list = new ArrayList<MerchantRoleDto>();
		for (MerchantRolePo po : page.getContent()) {
			MerchantRoleDto dto = new MerchantRoleDto();
			JPAUtil.fatherToChild(po, dto);
			list.add(dto);
		}
		return new DataTableDto(list, page.getTotalElements());
	}

	@Override
	public List<MerchantTreeDto> getPermissionTree(Integer roleId) {
		// Step1: 查询出满足条件的资源，并封装成树
		List<MerchantPermPo> poList = merchantPermissionDao.findByOrderBySort();
		List<MerchantTreeDto> dtoList = new ArrayList<MerchantTreeDto>();
		for (MerchantPermPo po : poList) {
			MerchantTreeDto dto = new MerchantTreeDto();
			dto.setId(po.getId());
			String pid = po.getParent() == null ? "#" : po.getParent().getId()+"";
			dto.setParent(pid);
			dto.setText(po.getName());
			dto.setType("menu");
			Map<String,Object> state = new HashMap<String,Object>();
			state.put("selected", false);
			dto.setState(state);
			dtoList.add(dto);
		}
		// 如果角色id不存在（添加角色），则直接返回
		if(roleId==null){
			return dtoList;
		}
		
		// Step2: 将所有角色所拥有的资源选中
		MerchantRolePo role = merchantRoleDao.findById(roleId).get();
		List<MerchantPermPo> roList = role.getPerms();
		for (MerchantPermPo ro : roList) {
			for (MerchantTreeDto dto : dtoList) {
				// 按钮类型才勾选
				if(ro.getId()==dto.getId()&&ro.getType()==3){
					dto.getState().put("selected", true);
				}
			}
		}
		return dtoList;
	}

	@Override
	public List<MerchantRoleDto> getRoles() throws Exception {
		// Step1: 查询出满足条件的资源，并封装成树
		List<MerchantRolePo> poList = merchantRoleDao.findByStatusEqualsOrderBySort(AdminConstaints.STATUS_OK);
		List<MerchantRoleDto> dtoList = new ArrayList<MerchantRoleDto>();
		for (MerchantRolePo po : poList) {
			MerchantRoleDto dto = new MerchantRoleDto();
			JPAUtil.fatherToChild(po, dto);
			dtoList.add(dto);
		}
		return dtoList;
	}

}
