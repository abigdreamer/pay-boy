package com.ndood.admin.service.system.agent;

import java.util.List;

import com.ndood.admin.pojo.system.agent.dto.AgentPermDto;

/**
 * 资源管理业务接口
 * @author ndood
 */
public interface SystemAgentPermService {

	/**
	 * 获取资源tree列表
	 * @param keywords 
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	List<AgentPermDto> getPermList() throws Exception;

	/**
	 * 批量删除资源
	 * @param ids
	 */
	void batchDeletePerm(Integer[] ids);

	/**
	 * 添加资源
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	AgentPermDto addPerm(AgentPermDto dto) throws Exception;

	/**
	 * 获取资源信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	AgentPermDto getPerm(Integer id) throws Exception;

	/**
	 * 更新资源信息
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	AgentPermDto updatePerm(AgentPermDto dto) throws Exception;

}
