package com.ndood.admin.service.system;

import java.util.List;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.dto.TreeDto;
import com.ndood.admin.pojo.system.RolePo;
import com.ndood.admin.pojo.system.dto.RoleDto;
import com.ndood.admin.pojo.system.query.RoleQuery;

/**
 * 角色管理业接口
 * @author ndood
 */
public interface SystemRoleService {

	/**
	 * 角色列表
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	DataTableDto pageRoleList(RoleQuery query) throws Exception;

	/**
	 * 添加角色
	 * @param role
	 * @throws Exception 
	 */
	void addRole(RoleDto role) throws Exception;

	/**
	 * 批量删除角色
	 * @param ids
	 */
	void batchDeleteRole(Integer[] ids);

	/**
	 * 更新角色
	 * @param role
	 * @throws Exception 
	 */
	void updateRole(RoleDto dto) throws Exception;

	/**
	 * 获取单个角色
	 * @param id
	 * @return
	 */
	RolePo getRole(Integer id);

	/**
	 * 获取角色资源树
	 * @param id
	 * @param pid
	 * @return
	 */
	List<TreeDto> getPermissionTree(Integer roleId);

	/**
	 * 获取所有角色
	 * @throws Exception 
	 */
	List<RoleDto> getRoles() throws Exception;

}
