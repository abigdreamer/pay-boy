package com.ndood.admin.service.system.merchant;

import java.util.List;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.merchant.MerchantRolePo;
import com.ndood.admin.pojo.system.merchant.dto.MerchantRoleDto;
import com.ndood.admin.pojo.system.merchant.dto.MerchantTreeDto;
import com.ndood.admin.pojo.system.merchant.query.MerchantRoleQuery;

/**
 * 角色管理业接口
 * @author ndood
 */
public interface SystemMerchantRoleService {

	/**
	 * 角色列表
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	DataTableDto pageRoleList(MerchantRoleQuery query) throws Exception;

	/**
	 * 添加角色
	 * @param role
	 * @throws Exception 
	 */
	void addRole(MerchantRoleDto role) throws Exception;

	/**
	 * 批量删除角色
	 * @param ids
	 */
	void batchDeleteRole(Integer[] ids);

	/**
	 * 更新角色
	 * @param role
	 * @throws Exception 
	 */
	void updateRole(MerchantRoleDto dto) throws Exception;

	/**
	 * 获取单个角色
	 * @param id
	 * @return
	 */
	MerchantRolePo getRole(Integer id);

	/**
	 * 获取角色资源树
	 * @param id
	 * @param pid
	 * @return
	 */
	List<MerchantTreeDto> getPermissionTree(Integer roleId);

	/**
	 * 获取所有角色
	 * @throws Exception 
	 */
	List<MerchantRoleDto> getRoles() throws Exception;

}
