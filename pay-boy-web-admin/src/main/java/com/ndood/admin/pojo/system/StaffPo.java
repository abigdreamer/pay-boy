package com.ndood.admin.pojo.system;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户bean
 * @author ndood
 */
@Entity
@Table(name="o_staff")
@Cacheable(true)
@Getter @Setter
@JsonIgnoreProperties(value={"handler","fieldHandler"})
public class StaffPo implements Serializable{
	private static final long serialVersionUID = 4009471135416449773L;

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 密码
	 */
	@Column(length = 100, nullable = false)
	private String password;
	
	/**
	 * 昵称
	 */
	@Column(name="nick_name", length = 255)
	private String nickName;
	
	/**
	 * 头像
	 */
	@Column(name="head_img_url", length = 255)
	private String headImgUrl;
	
	/**
	 * 邮箱
	 */
	@Column(length = 32)
	private String email;
	
	/**
	 * 手机
	 */
	@Column(length = 32, nullable = false, unique = true)
	private String mobile;
	
	/**
	 * 状态 0：未启用 1：启用
	 */
	@Column(nullable = false)
	private Integer status;
	
	/**
	 * 上次登录IP
	 */
	@Column(name="`last_login_ip`")
	private String lastLoginIp;
	
	/**
	 * 上次登录时间
	 */
	@Column(name="`last_login_time`")
	private String lastLoginTime;
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time", nullable = false, columnDefinition="datetime", updatable=false)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time", nullable = false, columnDefinition="datetime")
	private Date updateTime;
	
	/**
	 * 角色
	 */
	@ManyToMany
    @JoinTable(name = "o_staff_role", joinColumns = @JoinColumn(name = "staff_id", referencedColumnName = "id"), 
	inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<RolePo> roles;
	
}
