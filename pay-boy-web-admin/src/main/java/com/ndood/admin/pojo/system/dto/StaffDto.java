package com.ndood.admin.pojo.system.dto;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.security.SocialUserDetails;

import com.google.api.client.util.Maps;
import com.ndood.admin.pojo.system.StaffPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class StaffDto extends StaffPo implements UserDetails, SocialUserDetails{
	private static final long serialVersionUID = 7069166002396750862L;

	private Integer[] roleIds;
	
	public StaffDto() {
	}

	/**
	 * 带星号的邮箱
	 */
	public String getEncryptEmail() {
		String email = getEmail();
		if(email==null) {
			return null;
		}
		email = email.replaceAll("(\\w?)(\\w+)(\\w)(@\\w+\\.[a-z]+(\\.[a-z]+)?)", "$1****$3$4");
		return email;
	}
	
	/**
	 * 带星号的手机
	 */
	public String getEncryptMobile() {
		String mobile = getMobile();
		if(mobile==null) {
			return null;
		}
		mobile = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
		return mobile;
	}

	public StaffDto(Integer id,String nickName, String headImgUrl,String email, String mobile, Integer status, 
			Date createTime, Date updateTime, String lastLoginIp, String lastLoginTime) {
		super();
		this.setId(id);
		this.setNickName(nickName);
		this.setHeadImgUrl(headImgUrl);
		this.setEmail(email);
		this.setMobile(mobile);
		this.setStatus(status);
		this.setCreateTime(createTime);
		this.setUpdateTime(updateTime);
		this.setLastLoginIp(lastLoginIp);
		this.setLastLoginTime(lastLoginTime);
	}
	
	/**
	 * 用户有权访问的所有url，不持久化到数据库
	 */
	@Transient
	private Map<String,GrantedAuthority> urlMap = Maps.newHashMap();
	
	/**
	 * 将urls的东西全都放到authorities中，看看角色是否都要加ROLE_
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return urlMap.values();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getUserId() {
		return String.valueOf(super.getId());
	}

	@Override
	public String getUsername() {
		return String.valueOf(super.getId());
	}
	
}
