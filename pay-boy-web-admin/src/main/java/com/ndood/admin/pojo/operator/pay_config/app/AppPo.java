package com.ndood.admin.pojo.operator.pay_config.app;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * 应用bean
 * @author ndood
 */
@Entity
@Table(name="c_app")
@Cacheable(true)
@Getter @Setter
@JsonIgnoreProperties(value={"handler","fieldHandler"})
public class AppPo implements Serializable{
	private static final long serialVersionUID = 7148771564181945122L;
	
	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 应用编码
	 */
	@Column(name="app_no")
	private String appNo;
	
	/**
	 * 应用名称
	 */
	@Column(name="`name`")
	private String name;

	/**
	 * 应用创建限制
	 */
	@Column(name="`limit`")
	private Integer limit;
	
	/**
	 * 状态
	 */
	@Column(name="`status`")
	private Integer status;
	
	/**
	 * 排序
	 */
	@Column(name="`sort`")
	private Integer sort;
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time")
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time")
	private Date updateTime;

}
