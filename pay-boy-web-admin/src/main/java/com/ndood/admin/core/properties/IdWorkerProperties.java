package com.ndood.admin.core.properties;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class IdWorkerProperties {

	/**
	 * 工作空间
	 */
	private Integer workspaceId = 0;
	/**
	 * 机器ID，暂时弄成静态
	 */
	private Integer machineId = 0;
}
