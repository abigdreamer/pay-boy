package com.ndood.admin.core.jpa.dialect;
import org.hibernate.dialect.MySQL55Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StringType;

/**
 * 支持中文GBK排序
 * https://www.cnblogs.com/amCharlie/p/9328658.html
 * https://www.cnblogs.com/s648667069/p/6478559.html
 */
public class MySQLChineseDialect extends MySQL55Dialect {
 
    public MySQLChineseDialect(){
        super();
        registerFunction("convert",new SQLFunctionTemplate(StringType.INSTANCE, "convert(?1 using ?2)") );
    }
}