package com.ndood.admin.core.properties;

import lombok.Data;

/**
 * 邮件相关配置
 */
@Data
public class AdminEmailProperties {

	/**
	 * 邮件aes加密的key
	 */
	private String encryptKey = "jeepupilplus";
	/**
	 * 发件人
	 */
	private String from = "jeepupilplus@126.com";
}
