package com.ndood.admin.core.web.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
// @Scope("singleton")
public class MockQueue {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	private String placeOlder;
	
	private String completeOrder;

	public String getPlaceOlder() {
		return placeOlder;
	}

	/**
	 * 模拟处理下单
	 * @param placeOlder
	 * @throws InterruptedException 
	 */
	public void setPlaceOlder(String placeOrder) throws Exception {
		new Thread(() -> {
			logger.info("接到下单请求, " + placeOrder);
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.completeOrder = placeOrder;
			logger.info("下单请求处理完毕," + placeOrder);
		}).start();
	}

	public String getCompleteOrder() {
		return completeOrder;
	}

	public void setCompleteOrder(String completeOrder) {
		this.completeOrder = completeOrder;
	}
}
