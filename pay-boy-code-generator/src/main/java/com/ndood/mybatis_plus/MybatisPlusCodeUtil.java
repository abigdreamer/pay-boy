package com.ndood.mybatis_plus;

import java.sql.SQLException;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class MybatisPlusCodeUtil {

	public static void main(String[] args) throws SQLException {

		// 1. 全局配置
		GlobalConfig config = new GlobalConfig();
		config.setActiveRecord(true) // 是否支持AR模式
				.setAuthor("Bean") // 作者
				.setOutputDir("D:\\mybatis-plus-out") // 生成路径
				.setFileOverride(true) // 文件覆盖
				.setIdType(IdType.AUTO) // 主键策略
				.setServiceName("%sService") // 设置生成的service接口的名字的首字母是否为I
				.setEntityName("%sPo")
				// IEmployeeService
				.setBaseResultMap(true)// 生成基本的resultMap
				.setBaseColumnList(true);// 生成基本的SQL片段

		// 2. 数据源配置
		DataSourceConfig dsConfig = new DataSourceConfig();
		dsConfig.setDbType(DbType.MYSQL) // 设置数据库类型
				.setDriverName("com.mysql.jdbc.Driver")
				.setUrl("jdbc:mysql://121.196.208.113:3306/pay-boy-db")
				.setUsername("root")
				.setPassword("pay-boy-cyp");

		// 3. 策略配置globalConfiguration中
		StrategyConfig stConfig = new StrategyConfig();
		stConfig.setCapitalMode(true) // 全局大写命名
				//.setDbColumnUnderline(true) // 指定表名 字段名是否使用下划线
				.setNaming(NamingStrategy.underline_to_camel) // 数据库表映射到实体的命名策略
				.setTablePrefix("")
				/*.setInclude("m_account","m_app","m_bank_account","m_perm","m_rec_channel","m_rec_config","m_rec_contract",
						"m_rec_device","m_role","m_role_perm","m_shop","m_staff","m_staff_role","m_userconnection","m_merchant")*/
				.setInclude("r_agent_account","r_agent_log","r_agent_notice","r_mch_account","r_mch_log","r_mch_notice","r_mch_rec_recon_batch","r_mch_rec_recon_mistake","r_mch_rec_recon_scratch_pool","r_operator_log",
						"r_rec_flow","r_rec_notify","r_rec_notify_history","r_rec_offline_order","r_rec_online_order","r_rec_pay","r_rec_refund","r_risk_rec_agent_month","r_risk_rec_channel_daily",
						"r_risk_rec_mch_month","r_settle_rec","r_settle_rec_agent_order","r_settle_rec_annex","r_settle_rec_mch_order");
				/*.setInclude("c_app","c_bank","c_bank_branch","c_mch_category","c_rec_channel","c_rec_contract","c_rec_product","c_region")
				.setInclude("s_agent","s_merchant","s_rec_channel","s_rec_pay_type","s_rec_pay_way","s_rec_product","s_rec_shop")
				.setInclude("a_agent","a_account","a_bank_account","a_perm","a_role","a_role_perm","a_staff","a_staff_role")
				.setInclude("o_config","o_job","o_notice","o_perm","o_role","o_role_perm","o_staff","o_staff_role");*/
				
		// 4. 包名策略配置
		PackageConfig pkConfig = new PackageConfig();
		pkConfig.setParent("com.ndood.api.base.pojo.operator")
				.setMapper("mapper")// dao
				.setService("service")// servcie
				.setController("controller")// controller
				.setEntity("entity").setXml("mapper");// mapper.xml

		// 5. 整合配置
		AutoGenerator ag = new AutoGenerator();
		ag.setGlobalConfig(config).setDataSource(dsConfig).setStrategy(stConfig).setPackageInfo(pkConfig);

		// 6. 执行
		ag.execute();
	}

}
