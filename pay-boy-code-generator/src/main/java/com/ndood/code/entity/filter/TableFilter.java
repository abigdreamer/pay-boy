package com.ndood.code.entity.filter;

import java.util.HashMap;

/**
 * 表格过滤器，决定生成哪些表，并给这些表添加额外属性，决定代码生成的行为
 * @author ndood
 */
public class TableFilter {
	/**
	 * 表名称
	 */
	private String name;
	/**
	 * 表的子包路径
	 */
	private String subpackage="";
	/**
	 * 表对应的模块导航路径
	 */
	private String title="";
	/**
	 * 数据列过滤map
	 */
	private HashMap<String, ColumnFilter> columnsMap = new HashMap<String, ColumnFilter>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubpackage() {
		return subpackage;
	}

	public void setSubpackage(String subpackage) {
		this.subpackage = subpackage;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public HashMap<String, ColumnFilter> getColumnsMap() {
		return columnsMap;
	}

	public void setColumnsMap(HashMap<String, ColumnFilter> columnsMap) {
		this.columnsMap = columnsMap;
	}
	
}
