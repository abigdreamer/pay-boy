package com.ndood.code.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 表实体
 * 
 * @author Administrator
 *
 */
public class Table {

	private String name;// 表名称
	private String name2;// 处理后的表名称
	private String comment;// 介绍
	private String key;// 主键列

	private String subpackage; // 子包名 aa.b.c
	private String subpackagePath; // /aa/b/c
	private String subpackageDot; // .aa.b.c
	private String subpackageHump; // aaBC
	
	private String prefixSubpackage; // aa.b
	private String prefixSubpackagePath; // /aa/b
	private String prefixSubpackageDot; // .aa.b
	private String prefixSubpackageHump; // aaB
	
	/**
	 * 导航条标题内容
	 * 运营子系统,支付设置,应用管理 -> 运营子系统->支付设置->应用管理
	 */
	private List<String> titles = new ArrayList<>();
	/**
	 * 表格中的搜索字段的个数
	 */
	private Integer searchCount = 0;
	/**
	 * 表格中的所有列
	 */
	private List<Column> columns = new ArrayList<>();// 列集合

	public Integer getSearchCount() {
		return searchCount;
	}

	public void setSearchCount(Integer searchCount) {
		this.searchCount = searchCount;
	}

	public List<String> getTitles() {
		return titles;
	}

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}

	public String getKey() {
		return key;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<Column> getColumns() {
		return columns;
	}
	
	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getSubpackage() {
		return subpackage;
	}

	public void setSubpackage(String subpackage) {
		this.subpackage = subpackage;
	}

	public String getSubpackagePath() {
		return subpackagePath;
	}

	public void setSubpackagePath(String subpackagePath) {
		this.subpackagePath = subpackagePath;
	}

	public String getSubpackageDot() {
		return subpackageDot;
	}

	public void setSubpackageDot(String subpackageDot) {
		this.subpackageDot = subpackageDot;
	}

	public String getPrefixSubpackage() {
		return prefixSubpackage;
	}

	public void setPrefixSubpackage(String prefixSubpackage) {
		this.prefixSubpackage = prefixSubpackage;
	}

	public String getPrefixSubpackagePath() {
		return prefixSubpackagePath;
	}

	public void setPrefixSubpackagePath(String prefixSubpackagePath) {
		this.prefixSubpackagePath = prefixSubpackagePath;
	}

	public String getPrefixSubpackageDot() {
		return prefixSubpackageDot;
	}

	public void setPrefixSubpackageDot(String prefixSubpackageDot) {
		this.prefixSubpackageDot = prefixSubpackageDot;
	}

	public String getSubpackageHump() {
		return subpackageHump;
	}

	public void setSubpackageHump(String subpackageHump) {
		this.subpackageHump = subpackageHump;
	}

	public String getPrefixSubpackageHump() {
		return prefixSubpackageHump;
	}

	public void setPrefixSubpackageHump(String prefixSubpackageHump) {
		this.prefixSubpackageHump = prefixSubpackageHump;
	}

}
