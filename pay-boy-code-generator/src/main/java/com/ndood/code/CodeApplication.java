package com.ndood.code;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ndood.code.ui.DatabaseUtil;

@SpringBootApplication
public class CodeApplication {

	public static void main(String[] args) {

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new DatabaseUtil().setVisible(true);
			}
		});

	}

}
