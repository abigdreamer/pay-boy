package com.ndood.ddd.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.springframework.util.ResourceUtils;

import com.google.gson.Gson;
import com.ndood.ddd.entity.DDDDomain;

/**
 * 读取json配置文件，加载到内存
 */
public class DDDParser {

	public static String readToString(File file) {  
        Long filelength = file.length();  
        byte[] filecontent = new byte[filelength.intValue()];  
        try {  
            FileInputStream in = new FileInputStream(file);  
            in.read(filecontent);  
            in.close();  
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        try {  
            return new String(filecontent, "UTF-8");  
        } catch (UnsupportedEncodingException e) {  
            System.err.println("The OS does not support " + "UTF-8");  
            e.printStackTrace();  
            return null;  
        }  
    }
	
	public static DDDDomain parseDomain(String fileName) throws FileNotFoundException {
		File file = ResourceUtils.getFile("classpath:ddd/"+fileName);
		String temp = readToString(file);
		DDDDomain domain = new Gson().fromJson(temp, DDDDomain.class);
		return domain;
	}
}
