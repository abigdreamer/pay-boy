package com.ndood.ddd.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TemplateUtil {

	/**
	 * 根据目录查找所有模板文件
	 */
	public static List<ClientTemplate> getTemplateList(String basePath) {
		List<ClientTemplate> list = null;

		// 递归显示C盘下所有文件夹及其中文件
		File root = new File(basePath);
		try {
			list = showAllFiles(basePath, root);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	final static List<ClientTemplate> showAllFiles(String basePath, File dir) throws Exception {
		List<ClientTemplate> list = new ArrayList<ClientTemplate>();

		if(!dir.exists()) {
			return list;
		}
		
		File[] fs = dir.listFiles();

		if(fs==null) {
			return list;
		}
		for (int i = 0; i < fs.length; i++) {

			ClientTemplate templet = new ClientTemplate();
			templet.setAllPath(fs[i].getAbsolutePath());// 原目录

			File file = new File(fs[i].getAbsolutePath());

			templet.setPath(file.getParent().replace(basePath, "")); // 相对目录
			templet.setFileName(file.getName());// 文件名

			if (fs[i].isDirectory()) {
				try {
					list.addAll(showAllFiles(basePath, fs[i]));
				} catch (Exception e) {

				}
			} else {
				list.add(templet);
			}
		}
		return list;
	}
}
