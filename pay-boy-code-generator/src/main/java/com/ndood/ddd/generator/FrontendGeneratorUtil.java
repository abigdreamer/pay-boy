package com.ndood.ddd.generator;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import com.ndood.ddd.entity.DDDAggregate;
import com.ndood.ddd.entity.DDDDomain;
import com.ndood.ddd.parser.FileUtil;
import com.ndood.ddd.parser.StringUtil;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

public class FrontendGeneratorUtil {
	/**
	 * 根据目录查找所有子模板 aa.txt=xxx
	 */
	public static Map<String, String> getTemplateList(String basePath) {
		Map<String, String> map = new HashMap<String, String>();

		// 递归显示C盘下所有文件夹及其中文件
		File root = new File(basePath);
		try {
			map = showAllFiles(basePath, root);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}

	public static Map<String, String> showAllFiles(String basePath, File dir) throws Exception {
		Map<String, String> map = new HashMap<String, String>();

		File[] fs = dir.listFiles();
		if(fs==null) {
			return map;
		}
		for (int i = 0; i < fs.length; i++) {
			File file = new File(fs[i].getAbsolutePath());
			// 将读取的子模板的内容放在map集合中

			if (fs[i].isDirectory()) {
				try {
					map.putAll(showAllFiles(basePath, fs[i]));
				} catch (Exception e) {

				}
			} else {
				map.put(file.getName(), FileUtil.getContent(fs[i].getAbsolutePath()));
			}
		}
		return map;
	}

	/**
	 * 创建聚合根内容
	 */
	public static String createContent(String fileName, String filePath, DDDDomain domain, DDDAggregate aggregate) {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
		
		String content = "";
		try {

			cfg.setDirectoryForTemplateLoading(new File(filePath).getParentFile());
			cfg.setDefaultEncoding("UTF-8");
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			
			Map<String,Object> dataModel = new HashMap<String,Object>();
			dataModel.put("aggregate", aggregate);
			dataModel.put("domain", domain);
			
			Template template = cfg.getTemplate(fileName);
			Writer out = new CharArrayWriter();
			template.process(dataModel, out);
			content = out.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return content;
	}
	
	public static String createSingleContent(String fileName, String filePath, DDDDomain domain) {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
		
		String content = "";
		try {

			cfg.setDirectoryForTemplateLoading(new File(filePath).getParentFile());
			cfg.setDefaultEncoding("UTF-8");
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			
			Map<String,Object> dataModel = new HashMap<String,Object>();
			dataModel.put("domain", domain);
			
			Template template = cfg.getTemplate(fileName);
			Writer out = new CharArrayWriter();
			template.process(dataModel, out);
			content = out.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return content;
	}
	
	/**
	 * 创建文件保存路径
	 */
	public static String createOutPath(String path, String fileName, DDDDomain domain, DDDAggregate aggregate) {
		// Step1: 替换文件名
		String outFile = fileName
				.replace("[aggregate]", aggregate.getName());
		path = path + "\\" + outFile;
		path = path.replace("[system]", domain.getSystem());
		path = path.replace("[domain]", domain.getName());
		path = path.replace("[aggregate]", aggregate.getName());
		return path;
	}

	public static String createOutPath(String path, String fileName, DDDDomain domain) {
		// Step1: 替换文件名
		path = path + "\\" + fileName;
		// Step2: 替换文件路径
		path = path.replace("[system]", domain.getSystem()
				.replace("[domain]", domain.getName()));
		return path;
	}

}
