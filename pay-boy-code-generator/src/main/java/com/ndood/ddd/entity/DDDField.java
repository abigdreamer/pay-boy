package com.ndood.ddd.entity;

import org.apache.commons.lang3.StringUtils;

import com.ndood.ddd.parser.StringUtil;

public class DDDField {
	String name;
	String type;
	String desc;
	boolean required;

	public String getName() {
		return name;
	}

	public String getCamelName() {
		return StringUtil.underlineToCamel(this.name);
	}
	
	public String getLargeCamelName() {
		return StringUtil.underlineToCamelLarge(this.name);
	}
	
	public String getLargeType() {
		if(StringUtils.isBlank(type)|| type.length()<1) {
			return type;
		}
		if("bigdecimal".equals(type)) {
			return "BigDecimal";
		}
		return type.replaceFirst(type.substring(0,1), type.substring(0,1).toUpperCase());
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
}
