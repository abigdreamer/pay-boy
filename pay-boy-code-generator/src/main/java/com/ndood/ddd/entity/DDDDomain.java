package com.ndood.ddd.entity;

import java.util.ArrayList;
import java.util.List;

import com.ndood.ddd.parser.StringUtil;

public class DDDDomain {
	String name;
	String base;
	String system;
	String desc;
	
	List<DDDAggregate> aggregates = new ArrayList<>();

	public String getName() {
		return name;
	}
	
	public String getCamelName() {
		return StringUtil.underlineToCamel(this.name);
	}
	
	public String getLargeCamelName() {
		return StringUtil.underlineToCamelLarge(this.name);
	}
	
	public String getCamelSystem() {
		return StringUtil.underlineToCamel(this.system);
	}
	
	public String getLargeCamelSystem() {
		return StringUtil.underlineToCamelLarge(this.system);
	}
	
	public String getUnderlineBase() {
		return base.replaceAll("\\.", "_");
	}
	
	public String getPathBase() {
		return base.replaceAll("\\.", "\\\\");
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public List<DDDAggregate> getAggregates() {
		return aggregates;
	}

	public void setAggregates(List<DDDAggregate> aggregates) {
		this.aggregates = aggregates;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
