package com.ndood.ddd.entity;

import java.util.ArrayList;
import java.util.List;

import com.ndood.ddd.parser.StringUtil;

public class DDDAggregate {
	String name;
	String tableName;
	String desc;
	
	List<DDDField> fields = new ArrayList<>();

	List<DDDSearchField> searchFields = new ArrayList<>();
	
	List<DDDField> statisticFields = new ArrayList<>();
	
	public String getName() {
		return name;
	}
	
	public String getMiddlelineName() {
		return this.name.replace("_", "-");
	}
	
	public String getCamelName() {
		return StringUtil.underlineToCamel(this.name);
	}
	
	public String getLargeCamelName() {
		return StringUtil.underlineToCamelLarge(this.name);
	}
	
	public String getCamelTableName() {
		return StringUtil.underlineToCamel(this.tableName);
	}
	
	public String getLargeCamelTableName() {
		return StringUtil.underlineToCamelLarge(this.tableName);
	}
	

	public void setName(String name) {
		this.name = name;
	}

	public List<DDDField> getFields() {
		return fields;
	}

	public void setFields(List<DDDField> fields) {
		this.fields = fields;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<DDDSearchField> getSearchFields() {
		return searchFields;
	}

	public void setSearchFields(List<DDDSearchField> searchFields) {
		this.searchFields = searchFields;
	}

	public List<DDDField> getStatisticFields() {
		return statisticFields;
	}

	public void setStatisticFields(List<DDDField> statisticFields) {
		this.statisticFields = statisticFields;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
