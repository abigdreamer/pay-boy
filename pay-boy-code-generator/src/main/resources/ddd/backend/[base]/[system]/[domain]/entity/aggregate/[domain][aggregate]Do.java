package ${domain.base}.${domain.system}.${domain.name}.entity.aggregate;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ${domain.largeCamelName}${aggregate.largeCamelName}Do {
	
<#list aggregate.fields as field>
<#if field.desc??>
	/**
     * ${field.desc}
     */
</#if>
    private ${field.largeType} ${field.camelName};
    
</#list>
}