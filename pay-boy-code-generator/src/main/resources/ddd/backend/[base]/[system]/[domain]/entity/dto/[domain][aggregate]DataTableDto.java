<#if (aggregate.statisticFields?size>0)>
package ${domain.base}.${domain.system}.${domain.name}.entity.dto;

import java.math.BigDecimal;
import java.util.List;

import ${domain.base}.support.DataTableDto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ${domain.largeCamelName}${aggregate.largeCamelName}DataTableDto extends DataTableDto {
	private static final long serialVersionUID = -5935734278073807700L;

<#list aggregate.statisticFields as field>
<#if field.desc??>
	/**
	 * ${field.desc}
	 */
</#if>
	private ${field.largeType} total${field.largeCamelName};
</#list>
	
	public ${domain.largeCamelName}${aggregate.largeCamelName}DataTableDto(List<${domain.largeCamelName}${aggregate.largeCamelName}Dto> list,
			${domain.largeCamelName}${aggregate.largeCamelName}TotalDto total) {

		super(list, total.getTotal());
<#list aggregate.statisticFields as field>
		this.total${field.largeCamelName} = total.getTotal${field.largeCamelName}; 
</#list>

	}

}
</#if>