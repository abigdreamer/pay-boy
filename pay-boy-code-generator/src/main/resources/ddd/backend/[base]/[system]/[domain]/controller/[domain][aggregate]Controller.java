package ${domain.base}.${domain.system}.${domain.name}.contoller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${domain.base}.support.AdminResultVo;
import ${domain.base}.support.AssertUtil;

import ${domain.base}.${domain.system}.${domain.name}.entity.query.${domain.largeCamelName}${aggregate.largeCamelName}Query;
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}Dto;
<#if (aggregate.statisticFields?size>0)>
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}TotalDto;
import ${domain.base}.${domain.system}.${domain.name}.entity.dto.${domain.largeCamelName}${aggregate.largeCamelName}DataTableDto;
<#else>
import ${domain.base}.support.DataTableDto;
</#if>
import ${domain.base}.${domain.system}.${domain.name}.service.${domain.largeCamelName}Service;

@RestController
@RequestMapping("/${domain.system}/${domain.name}/${aggregate.name}")
public class ${domain.largeCamelName}${aggregate.largeCamelName}Controller {
	
	@Autowired
	private ${domain.largeCamelName}Service ${domain.camelName}Service;
	
	/**
	 * 1 查询出<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>列表
	 */
	@RequestMapping("/query_list")
	public AdminResultVo query${aggregate.largeCamelName}List(@RequestBody ${domain.largeCamelName}${aggregate.largeCamelName}Query query) {
		
		// Step1: 校验请求参数
<#list aggregate.searchFields as field>
<#if field.required??&& field.required==true>
		AssertUtil.notNull(query.get${field.largeCamelName}(), "<#if field.desc??>${field.desc}<#else>请求参数</#if>不存在！");
</#if>
</#list>

		// Step2: 获取<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>列表
<#if (aggregate.statisticFields?size>0)>
		${domain.largeCamelName}${aggregate.largeCamelName}DataTableDto page = ${domain.camelName}Service.query${aggregate.largeCamelName}List(query);
<#else>
		DataTableDto page = ${domain.camelName}Service.query${aggregate.largeCamelName}List(query);
</#if>
		return AdminResultVo.ok().setData(page).setMsg("操作成功");
	
	}

	/**
	 * 2 添加<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>
	 */
	@RequestMapping("/add")
	public AdminResultVo add(@RequestBody ${domain.largeCamelName}${aggregate.largeCamelName}Dto dto) {
		// Step1: 参数校验
<#list aggregate.fields as field>
<#if field.required??&& field.required==true>
		AssertUtil.notNull(dto.get${field.largeCamelName}(), "<#if field.desc??>${field.desc}<#else>请求参数</#if>不存在！");
</#if>
</#list>
		
		// Step2: 添加<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>
		${domain.camelName}Service.add${aggregate.largeCamelName}(dto);
		
		// Step3: 返回结果
		return AdminResultVo.ok().setMsg("操作成功");
	}
	
	/**
	 * 3 修改<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>
	 */
	@RequestMapping("/modify")
	public AdminResultVo modify(@RequestBody ${domain.largeCamelName}${aggregate.largeCamelName}Dto dto) {
		// Step1: 参数校验
		AssertUtil.notNull(dto.getId(), "ID不能为空！");
<#list aggregate.fields as field>
<#if field.required??&& field.required==true>
		AssertUtil.notNull(dto.get${field.largeCamelName}(), "<#if field.desc??>${field.desc}<#else>请求参数</#if>不存在！");
</#if>
</#list>
		
		// Step2: 修改<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>
		${domain.camelName}Service.modify${aggregate.largeCamelName}(dto);
		
		// Step3: 返回结果
		return AdminResultVo.ok().setMsg("操作成功");
	
	}
	
	/**
	 * 4 删除<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>
	 */
	@RequestMapping("/remove")
	public AdminResultVo remove(@RequestBody ${domain.largeCamelName}${aggregate.largeCamelName}Dto dto) {
		// Step1: 参数校验
		AssertUtil.notNull(dto.getId(), "ID不能为空！");
		
		// Step2: 修改<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>
		${domain.camelName}Service.remove${aggregate.largeCamelName}(dto);
		
		// Step3: 返回结果
		return AdminResultVo.ok().setMsg("操作成功");
	
	}
	
	/**
	 * 5 获取<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>信息
	 */
	@RequestMapping("/query")
	public AdminResultVo query(@RequestBody ${domain.largeCamelName}${aggregate.largeCamelName}Query query) {
		// Step1: 参数校验
		AssertUtil.notNull(query.getId(), "ID不能为空！");
		
		// Step2: 修改<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>
		${domain.largeCamelName}${aggregate.largeCamelName}Dto dto = ${domain.camelName}Service.query${aggregate.largeCamelName}(query);
		AssertUtil.notNull(dto, "获取<#if aggregate.desc??>${aggregate.desc}<#else>数据</#if>信息失败！");
		
		// Step3: 返回结果
		return AdminResultVo.ok().setData(dto).setMsg("操作成功");
	
	}

}
