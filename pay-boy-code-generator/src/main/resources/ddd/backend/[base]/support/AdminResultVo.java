package ${domain.base}.support;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.taiyi.service.base.constaints.CommonCode;

/**
 * 普通对象返回数据
 */
public class AdminResultVo implements Serializable {
	private static final long serialVersionUID = 3765308055181715685L;

	public final static AdminResultVo ok() {
		return new AdminResultVo(CommonCode.SUCCESS);
	}
	
	public final static AdminResultVo error() {
		return new AdminResultVo(CommonCode.ERR_SERVER);
	}
	
	private CommonCode code;
	private String msg;
	private Object data;
	
	public Long getRequestId() {
		return new Date().getTime();
	}
	
	private AdminResultVo(CommonCode code) {
		this.code = code;
		this.msg = code.getValue();
	}

	@JSONField(name = "code")
	public Integer getCode() {
		return code.getCode();
	}
	
	@JSONField(name = "code")
	public AdminResultVo setCode(Integer code) {
		this.code = CommonCode.getEnum(code);
		return this;
	}

	public String getMsg() {
		return msg;
	}
	
	public AdminResultVo setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public Object getData() {
		return data;
	}

	public AdminResultVo setData(Object data) {
		this.data = data;
		return this;
	}
}
