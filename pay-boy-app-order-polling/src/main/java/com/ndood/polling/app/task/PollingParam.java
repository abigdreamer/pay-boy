package com.ndood.polling.app.task;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 通知次数
 */
@Getter
@Setter
public class PollingParam {

	/**
	 * 渠道订单号
	 */
	private String payOrderNo;
	/**
	 * 轮询任务标记
	 */
	private Integer flag = 1; // 1 查询  2 撤销
	/**
	 * 已通知次数
	 */
	private Integer count;
	/**
	 * 每次时间间隔
	 */
	private Integer duration;
	/**
	 * 最大次数
	 */
	private Integer maxCount;
	/**
	 * 上次执行时间
	 */
	private Date createTime;
}
