package com.ndood.polling.app.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.ndood.PollingApplication;
import com.ndood.api.app.properties.PollingProperties;
import com.ndood.polling.app.task.PollingDelayTask;
import com.ndood.polling.app.task.PollingParam;
import com.ndood.polling.domain.polling.v1.service.PollingService;

import lombok.extern.slf4j.Slf4j;

@Component
@RabbitListener(queues = "polling")
@Slf4j
public class PollingReceiver {
	
	@Autowired
	private PollingProperties pollingProperties;
	
	@Autowired
	private PollingService pollingService;
	
    @RabbitHandler
    public void process(String msg) {
    	// Step1: 获取渠道订单号
    	String payOrderNo = msg;
    	Assert.notNull(payOrderNo,"收款渠道订单号不存在！");
    	log.info("开启轮询任务==>"+payOrderNo);
    	
    	// Step2: 创建轮询任务
    	PollingParam param = new PollingParam();
    	param.setPayOrderNo(payOrderNo);
    	param.setFlag(1); // 查询任务 vs 撤销任务
    	param.setCount(1);
    	param.setDuration(pollingProperties.getQuery_duration());
    	param.setMaxCount(pollingProperties.getMax_query_retry());
    	
    	// Step3: 加入到延时队列
    	PollingDelayTask task = new PollingDelayTask(param,pollingService);
    	PollingApplication.tasks.put(task);
    }
 
}
