package com.ndood.polling.domain.polling.v1.entity.dto;

import com.ndood.polling.domain.polling.v1.entity.PollingRecPayDo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PollingRecPayDto extends PollingRecPayDo {

	/**
	 * 商户ID
	 */
	private Integer mchId;
}
