user  nginx;
worker_processes  4;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  debug;
error_log  logs/error.log  crit;

pid        logs/nginx.pid;

worker_rlimit_nofile 65535;

events {
    use epoll; 
    worker_connections  20480;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;
    server_names_hash_bucket_size 128;
    client_header_buffer_size 128k;
    large_client_header_buffers 4 32k;
    client_max_body_size 300m;

    sendfile        on;
    tcp_nopush     on;
    
    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Headers X-Requested-With;
    add_header Access-Control-Allow-Methods GET,POST,OPTIONS;

    #keepalive_timeout  0;
    keepalive_timeout  180;
    tcp_nodelay on; 
    client_body_buffer_size  512k;
    proxy_connect_timeout    5;
    proxy_read_timeout       1800;
    proxy_send_timeout       1800;
    proxy_buffer_size        128k;
    proxy_buffers            4 64k;
    proxy_busy_buffers_size 128k;
    proxy_temp_file_write_size 512k;
    #gzip on;
    #gzip_min_length  1k;
    #gzip_buffers     4 16k;
    #gzip_http_version 1.1;
    #gzip_comp_level 2;
    #gzip_types       text/plain application/x-javascript text/css application/xml;
    #gzip_vary on;
    proxy_temp_path   logs/proxy_temp_dir;
    proxy_cache_path  logs/proxy_cache_dir  levels=1:2   keys_zone=cache_one:10m inactive=1d max_size=5g;
    #limit_req_zone $binary_remote_addr zone=one:20m rate=5r/s;
    #limit_conn_zone $binary_remote_addr zone=TotalConnLimitZone:20m;
    #limit_conn  TotalConnLimitZone  10;  

    fastcgi_intercept_errors on;
    proxy_intercept_errors on;

    upstream gateway_pool {
    	server 192.168.249.33:9999;
    }

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
		 	#limit_req zone=one burst=5 nodelay;
		 	#limit_rate 1024k;
	        proxy_redirect off;
			proxy_set_header        X-Real-IP $remote_addr;
	        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
	        proxy_set_header        X-Forwarded-Host $host;
	        proxy_set_header        X-Forwarded-Proto $http_x_forwarded_proto;
	        proxy_set_header        Host $host;
	        if ($request_filename ~* .*\.(?:htm|html|js|css|jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm)$)
	        {
	            add_header Cache-Control "private, no-store, no-cache, must-revalidate, proxy-revalidate";
	        }
	        proxy_pass http://gateway_pool;
        }

        #微信客户端鉴权文件！！！
        location ^~/MP_verify_zElZqFIC0xeffkL0.txt  {
        	root html;
        }
        location ^~/MP_verify_SyoRgyfoB5Tw5OnM.txt  {
        	root html;
        }
		location ^~/test/simulator {
	        proxy_pass http://192.168.249.39:8088/simulator;
	    }
		location ^~/test/services {
            proxy_pass http://192.168.249.39:8086/services;
        }
        location ^~/test/check {
            proxy_pass http://192.168.249.39:8081/check;
        }
        location ^~/test/business {
            proxy_pass http://192.168.249.39:8083/business;
        }
        #减轻nginx负担，基本将所有的8070的测试指向全部改成走网关 且不校验token
        location ^~/test {
            proxy_pass http://192.168.249.39:9999/THIRD;
        }
        location ^~/test/ServiceIntegratedPlatform {
            proxy_pass http://192.168.249.33:8012/test/ServiceIntegratedPlatform;
        }
        location ^~/test/iauth-sandbox.wecity.qq.com/new/cgi-bin/getdetectinfo.php {
            proxy_pass https://iauth-sandbox.wecity.qq.com/new/cgi-bin/getdetectinfo.php;
		}   
        location ^~/test/sso {
            proxy_pass http://192.168.249.39:9999/sso;
        }
        location ^~/test/SFExpress_SF {
            proxy_pass http://bsp-ois.sit.sf-express.com:9080/bsp-ois/sfexpressService;
        }
        location ^~/SFExpress_SF {
            proxy_pass http://bsp-oisp.sf-express.com/bsp-oisp/sfexpressService;
        }

		#支付宝测试地址
        location ^~/openapi.alipaydev.com/gateway.do {
            proxy_pass https://openapi.alipaydev.com/gateway.do;
        }
        location ^~/dwbillcenter.alipaydev.com/downloadBillFile.resource {
            proxy_pass https://dwbillcenter.alipaydev.com/downloadBillFile.resource;
        }
		#支付宝生产地址
         location ^~/openapi.alipay.com/gateway.do {
            proxy_pass https://openapi.alipay.com/gateway.do;
        }
         location ^~/dwbillcenter.alipay.com/downloadBillFile.resource {
            proxy_pass https://dwbillcenter.alipay.com/downloadBillFile.resource;
        }
		#微信生产地址 无测试环境
        location ^~/api.mch.weixin.qq.com/ {
            proxy_ssl_certificate       /app/nginx/client_cert/wechat/apiclient_cert.pem;
            proxy_ssl_certificate_key   /app/nginx/client_cert/wechat/apiclient_key.pem;
            proxy_pass https://api.mch.weixin.qq.com/;
        }
		location ^~/test/ordercentre {
            proxy_pass http://192.168.249.39:9999/ordercentre;
        }
		###wechat address###
		location ^~/api.weixin.qq.com/ {
		    proxy_pass https://api.weixin.qq.com/;
		}
        location ^~/Tencent {
            proxy_pass http://192.168.249.33:8070/Tencent;
        }
        location ^~/iauth.wecity.qq.com/new/cgi-bin/getdetectinfo.php {
            proxy_pass https://iauth.wecity.qq.com/new/cgi-bin/getdetectinfo.php;
        }
		##去微信公众号获取token
        location ^~/weixinjx/WeiXinJxApi/ {
            proxy_pass http://118.212.184.125/weixinjx/WeiXinJxApi/;
        }
        location ^~/thirdwx.qlogo.cn/ {
            proxy_pass http://thirdwx.qlogo.cn/;
        }
        location ^~/Y/ {
         proxy_pass   http://www.jx116114.com/concent/pages/contactCentrePhone/global/shortUrl.html?x=;
        }
        # 一信通短信接口
        location ^~/jiangxi.ums86.com/sms/Api/Send.do {
            proxy_pass http://jiangxi.ums86.com:8899/sms/Api/Send.do;
        }
        #华盛接口生产
        location ^~/hs.zxpt.vsens.com/ {
            proxy_pass https://hs.zxpt.vsens.com/;
        }
        #华盛接口-测试
        location ^~/cs.zxpt.vsens.com {
            proxy_pass https://cs.zxpt.vsens.com/;
        }
        location ^~/test/oauth/customerInfo {
            proxy_pass http://192.168.249.39:8070/oauth/customerInfo;
        }
        location ^~/oauth/customerInfo {
            proxy_pass http://192.168.249.33:8070/oauth/customerInfo;
        }
		#中通测试
        location ^~/zto_test/ {
            proxy_pass http://58.40.16.120:9001/;
        }
        location ^~/zto_zs/ {
            proxy_pass http://japi.zto.cn/;
        }
		#EMS测试 1
		location ^~/EMS_test1/ {
            proxy_pass http://60.205.8.187:18001/;
		}
		#Ems测试 2
		location ^~/EMS_test2/ {
			proxy_pass http://os.ems.com.cn:8081/;
		}
		#权益中心鉴权
		location ^~/qy.chinaunicom.cn/ {
			proxy_pass http://qy.chinaunicom.cn/;
		}
		#通用对外网的转发
		location ^~/dog.jxhtxx.com/ {
			proxy_pass http://dog.jxhtxx.com/;
		}
       
		#	location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
		#	{
		#		expires      30d;
		#	}
		#        location ~ .*\.(js|css)?$
		#	{
		#		expires      30d;
		#	}	
       
        error_page   500  http://www.jx116114.com/50x_new.html; 
        location = /50x_new.html {
            root   html;
        } 
        location = /1111sjbg.jpg {
            root   html;
        }

        # redirect server error pages to the static page /50x.html
        #
        error_page   404 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

    }

    # HTTPS server
    #
    #server {
    #    listen       443 ssl;
    #    server_name  localhost;

    #    ssl_certificate      cert.pem;
    #    ssl_certificate_key  cert.key;

    #    ssl_session_cache    shared:SSL:1m;
    #    ssl_session_timeout  5m;

    #    ssl_ciphers  HIGH:!aNULL:!MD5;
    #    ssl_prefer_server_ciphers  on;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}

}

stream {

    log_format proxy '$remote_addr [$time_local] '
	     '$protocol $status $bytes_sent $bytes_received '
	     '$session_time "$upstream_addr" '
	     '"$upstream_bytes_sent" "$upstream_bytes_received" "$upstream_connect_time"';

    access_log logs/tcp-access.log proxy ;
    open_log_file_cache off;
    
    upstream TCP_MYSQL_3306 {
        hash $remote_addr consistent;
        server 192.168.249.35:3306 weight=5 max_fails=3 fail_timeout=30s;
	}
    server {
        listen 13306;
        proxy_connect_timeout 5s;
        proxy_timeout 30s;
        proxy_pass TCP_MYSQL_3306;
    }

}
