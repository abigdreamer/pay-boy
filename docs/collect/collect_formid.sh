#!/bin/bash

timeStr=`date +%Y-%m-%d`
timeStr_1=`date --date "1 days ago" +%Y-%m-%d`
timeStr_2=`date --date "2 days ago" +%Y-%m-%d`
timeStr_3=`date --date "3 days ago" +%Y-%m-%d`
timeStr_4=`date --date "4 days ago" +%Y-%m-%d`
timeStr_5=`date --date "5 days ago" +%Y-%m-%d`
timeStr_6=`date --date "6 days ago" +%Y-%m-%d`

grep -a formid ../test.log.20180824_02_42_38 > formids.total.log
#grep -a formid ../test.log.20180822_05_54_10 >> formids.total.log
grep -a formid ../test.log >> formids.total.log
#grep -a formid ../test.log > formids.$timeStr.log
#grep $timeStr formids.total.log|grep '::::' > formids.$timeStr.log
grep $timeStr_1 formids.total.log|grep '::::' > formids.$timeStr.log
grep $timeStr_2 formids.total.log|grep '::::' >> formids.$timeStr.log
grep $timeStr_3 formids.total.log|grep '::::' >> formids.$timeStr.log
grep $timeStr_4 formids.total.log|grep '::::' >> formids.$timeStr.log
grep $timeStr_5 formids.total.log|grep '::::' >> formids.$timeStr.log
grep $timeStr_6 formids.total.log|grep '::::' >> formids.$timeStr.log
#grep '2018-08-19 22' formids.total.log|grep '::::' >> formids.$timeStr.log
#grep '2018-08-19 23' formids.total.log|grep '::::' >> formids.$timeStr.log
awk -F' formid:' '{print $2}' formids.$timeStr.log |awk -F' |:' '{print $12"\t"$1}' >  res.$timeStr.log
awk  '{a[$1]++} END{for(i in a){print i,a[i] | "sort -nrk 2"}}' res.$timeStr.log  > res.stats.$timeStr.log