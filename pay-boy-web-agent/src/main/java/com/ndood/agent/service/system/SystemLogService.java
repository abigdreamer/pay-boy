package com.ndood.agent.service.system;

import com.ndood.agent.pojo.comm.dto.DataTableDto;
import com.ndood.agent.pojo.system.AgentLogPo;
import com.ndood.agent.pojo.system.query.AgentLogQo;

/**
 * 日志管理业接口
 */
public interface SystemLogService {

	/**
	 * 生成日志信息
	 */
	void makeLog(AgentLogPo log);
	
	/**
	 * 查询日志列表
	 */
	DataTableDto pageLogList(AgentLogQo query) throws Exception;
	
}
