package com.ndood.agent.service.user;

import com.ndood.agent.core.exception.AgentException;

public interface AccountPasswordService {

	/**
	 * 修改密码
	 */
	void changePassword(String username, String oldPassword, String newPassword) throws AgentException;

}
