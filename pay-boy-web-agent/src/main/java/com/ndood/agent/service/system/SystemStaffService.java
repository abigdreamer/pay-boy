package com.ndood.agent.service.system;

import com.ndood.agent.pojo.comm.dto.DataTableDto;
import com.ndood.agent.pojo.system.dto.StaffDto;
import com.ndood.agent.pojo.system.query.StaffQo;

/**
 * 用户管理业务接口
 * @author ndood
 */
public interface SystemStaffService {

	/**
	 * 添加用户
	 */
	String addStaff(StaffDto user) throws Exception;

	/**
	 * 批量删除用户
	 */
	void batchDeleteStaff(Integer[] ids);

	/**
	 * 更新用户
	 */
	void updateStaff(StaffDto user) throws Exception;

	/**
	 * 获取单个用户
	 */
	StaffDto getStaff(Integer id) throws Exception;

	/**
	 * 获取用户列表
	 */
	DataTableDto pageStaffList(StaffQo query) throws Exception;

}
