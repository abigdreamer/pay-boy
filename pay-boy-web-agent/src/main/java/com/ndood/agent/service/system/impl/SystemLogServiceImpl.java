package com.ndood.agent.service.system.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ndood.agent.core.web.tools.IdUtils;
import com.ndood.agent.dao.system.AgentLogDao;
import com.ndood.agent.dao.system.StaffDao;
import com.ndood.agent.dao.system.extend.ExtendAgentLogDao;
import com.ndood.agent.pojo.comm.dto.DataTableDto;
import com.ndood.agent.pojo.system.AgentLogPo;
import com.ndood.agent.pojo.system.StaffPo;
import com.ndood.agent.pojo.system.dto.AgentLogDto;
import com.ndood.agent.pojo.system.query.AgentLogQo;
import com.ndood.agent.service.system.SystemLogService;

/**
 * 日志模块业务类
 */
@Service
public class SystemLogServiceImpl implements SystemLogService {

	@Autowired
	private ExtendAgentLogDao extendAgentLogDao;
	
	@Autowired
	private AgentLogDao agentLogDao;
	
	@Autowired
	private StaffDao staffDao;
	
	@Autowired
	private IdUtils idUtils;
	
	@Override
	public void makeLog(AgentLogPo log) {
		
		// Step1: id信息
		log.setRecordNo(idUtils.getNextId());
		log.setAgentId(0);
		
		// Step2: 设置用户名
		if(log.getStaffId()!=null) {
			Integer staffId = log.getStaffId();
			StaffPo staff = staffDao.selectByPrimaryKey(staffId);
			if(staff!=null) {
				log.setStaffName(staff.getMobile());
				if(StringUtils.isBlank(log.getStaffName())) {
					log.setStaffName(staff.getEmail());
				}
				log.setAgentId(staff.getAgentId());
			}
		}
		
		agentLogDao.insertSelective(log);
	}
	
	@Override
	public DataTableDto pageLogList(AgentLogQo query) throws Exception {
	
		// Step1: 分页查询
		PageHelper.startPage(query.getPageNo(), query.getLimit());
		List<AgentLogDto> list = extendAgentLogDao.extendFindByAgentLogQo(query);
		
		// Step2: 封装DTO
		PageInfo<AgentLogDto> page = new PageInfo<>(list);
		return new DataTableDto(page.getList(), page.getTotal());
	
	}
	
}
