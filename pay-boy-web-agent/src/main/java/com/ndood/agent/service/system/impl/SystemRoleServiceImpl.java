package com.ndood.agent.service.system.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.agent.dao.system.extend.ExtendRoleDao;
import com.ndood.agent.pojo.system.dto.RoleDto;
import com.ndood.agent.service.system.SystemRoleService;

@Service
public class SystemRoleServiceImpl implements SystemRoleService{

	@Autowired
	private ExtendRoleDao extendRoleDao;
	
	@Override
	public List<RoleDto> getRoles() {
		// 查询出所有的角色列表
		List<RoleDto> list = extendRoleDao.findAll();
		return list;
	}

}
