package com.ndood.agent.pojo.system.dto;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ndood.agent.pojo.system.StaffPo;

public class StaffDto extends StaffPo implements UserDetails{
	private static final long serialVersionUID = 5936975965093261518L;

	private Map<String,GrantedAuthority> urlMap = Maps.newHashMap();
	
	List<RoleDto> roles = Lists.newArrayList();
	
	public List<RoleDto> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleDto> roles) {
		this.roles = roles;
	}

	/**
	 * 角色IDs
	 */
	private List<Integer> roleIds = Lists.newArrayList();
	
	public List<Integer> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Integer> roleIds) {
		this.roleIds = roleIds;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return urlMap.values();
	}

	@Override
	public String getUsername() {
		return String.valueOf(super.getId());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public Map<String, GrantedAuthority> getUrlMap() {
		return urlMap;
	}

	public void setUrlMap(Map<String, GrantedAuthority> urlMap) {
		this.urlMap = urlMap;
	}

	/**
	 * 获取带*号的手机号
	 */
	public String getEncryptMobile() {
		String mobile = getMobile();
		if(mobile==null) {
			return null;
		}
		mobile = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
		return mobile;
	}

}