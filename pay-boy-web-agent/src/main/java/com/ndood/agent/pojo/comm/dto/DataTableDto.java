package com.ndood.agent.pojo.comm.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Datalist返回数据
 */
@Getter @Setter
public class DataTableDto implements Serializable {
	private static final long serialVersionUID = -5313964509984582927L;

	/**
	 *  总记录数
	 */
	private Long total;
	
	/**
	 *  列表数据
	 */
	private List<?> rows;

	/**
	 * 分页
	 */
	public DataTableDto(List<?> list, Long total) {
		this.rows = list;
		this.total = total;
	}

}
