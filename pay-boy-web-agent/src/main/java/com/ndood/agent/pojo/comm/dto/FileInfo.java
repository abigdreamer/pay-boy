package com.ndood.agent.pojo.comm.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 创建FileInfo
 * @author ndood
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class FileInfo {

	private String path;

	public FileInfo(String path) {
		this.path = path;
	}
	
}
