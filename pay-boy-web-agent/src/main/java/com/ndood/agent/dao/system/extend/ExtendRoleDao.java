package com.ndood.agent.dao.system.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.agent.pojo.system.RolePo;
import com.ndood.agent.pojo.system.dto.RoleDto;

public interface ExtendRoleDao {

	/**
	 * 查询staff下的所有角色
	 */
	@Select("SELECT `id`, `name`, `desc`, `sort`, `status`, `create_time`, `update_time` FROM a_role "
			+ " WHERE `id` IN ( SELECT `role_id` from a_staff_role WHERE `staff_id` = #{staffId} and `status` = 1)")
	List<RolePo> findByStaffId(@Param("staffId") Integer staffId);

	@Select("SELECT `id`, `name`, `desc`, `sort`, `status`, `create_time`, `update_time` FROM a_role WHERE `status` = 1")
	List<RoleDto> findAll();
	
}
