package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.StaffPo;
import com.ndood.agent.pojo.system.StaffQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface StaffDao {
    int countByExample(StaffQuery example);

    int deleteByExample(StaffQuery example);

    int deleteByPrimaryKey(Integer id);

    int insert(StaffPo record);

    int insertSelective(StaffPo record);

    List<StaffPo> selectByExample(StaffQuery example);

    StaffPo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") StaffPo record, @Param("example") StaffQuery example);

    int updateByExample(@Param("record") StaffPo record, @Param("example") StaffQuery example);

    int updateByPrimaryKeySelective(StaffPo record);

    int updateByPrimaryKey(StaffPo record);
}