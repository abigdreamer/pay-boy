package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.StaffRolePo;
import com.ndood.agent.pojo.system.StaffRoleQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface StaffRoleDao {
    int countByExample(StaffRoleQuery example);

    int deleteByExample(StaffRoleQuery example);

    int insert(StaffRolePo record);

    int insertSelective(StaffRolePo record);

    List<StaffRolePo> selectByExample(StaffRoleQuery example);

    int updateByExampleSelective(@Param("record") StaffRolePo record, @Param("example") StaffRoleQuery example);

    int updateByExample(@Param("record") StaffRolePo record, @Param("example") StaffRoleQuery example);
}