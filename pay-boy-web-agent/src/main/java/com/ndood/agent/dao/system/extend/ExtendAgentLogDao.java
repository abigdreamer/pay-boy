package com.ndood.agent.dao.system.extend;

import java.util.List;

import com.ndood.agent.pojo.system.dto.AgentLogDto;
import com.ndood.agent.pojo.system.query.AgentLogQo;

public interface ExtendAgentLogDao {

	/**
	 * 按时间查询出
	 */
	public List<AgentLogDto> extendFindByAgentLogQo(AgentLogQo agentLogQo);
	
}
