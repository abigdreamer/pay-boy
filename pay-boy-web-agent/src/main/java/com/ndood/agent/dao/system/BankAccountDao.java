package com.ndood.agent.dao.system;

import com.ndood.agent.pojo.system.BankAccountPo;
import com.ndood.agent.pojo.system.BankAccountQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BankAccountDao {
    int countByExample(BankAccountQuery example);

    int deleteByExample(BankAccountQuery example);

    int deleteByPrimaryKey(Integer id);

    int insert(BankAccountPo record);

    int insertSelective(BankAccountPo record);

    List<BankAccountPo> selectByExample(BankAccountQuery example);

    BankAccountPo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BankAccountPo record, @Param("example") BankAccountQuery example);

    int updateByExample(@Param("record") BankAccountPo record, @Param("example") BankAccountQuery example);

    int updateByPrimaryKeySelective(BankAccountPo record);

    int updateByPrimaryKey(BankAccountPo record);
}