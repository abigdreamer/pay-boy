package com.ndood.agent.dao.system.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ndood.agent.pojo.system.dto.StaffDto;
import com.ndood.agent.pojo.system.query.StaffQo;

/**
 * 扩展的staffDao
 */
public interface ExtendStaffDao {

	/**
	 * 查询出满足条件email和email_status的用户记录
	 */
	@Select("SELECT `id`, `agent_id`, `status`, `email`, `head_img_url`, `mobile`, `nick_name`, `password`, `trade_password`, `last_login_ip`, `last_login_time`, `create_time`, `update_time` FROM a_staff "
			+ " WHERE `email` = #{email} AND `status` = #{status} LIMIT 1")
	public StaffDto extendFindByEmailAndStatus(@Param("email") String email,@Param("status")  Integer status);

	/**
	 * 查询出满足条件mobile的用户记录
	 */
	@Select("SELECT `id`, `agent_id`, `status`, `email`, `head_img_url`, `mobile`, `nick_name`, `password`, `trade_password`, `last_login_ip`, `last_login_time`, `create_time`, `update_time` FROM a_staff "
			+ " WHERE `mobile` = #{mobile} AND `status` = #{status} LIMIT 1")
	public StaffDto extendFindByMobileAndStatus(@Param("mobile") String mobile,@Param("status") Integer status);

	/**
	 * 分页查询出用户列表
	 */
	public List<StaffDto> extendFindByStaffQo(StaffQo query);
	
	/**
	 * 根据id查询用户信息
	 */
	public StaffDto extendFindById(@Param("id") Integer id);

}