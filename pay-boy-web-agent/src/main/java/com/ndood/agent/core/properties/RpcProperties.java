package com.ndood.agent.core.properties;

import lombok.Data;

@Data
public class RpcProperties {

	private String baseUrl = "http://localhost:83/";
	
}
