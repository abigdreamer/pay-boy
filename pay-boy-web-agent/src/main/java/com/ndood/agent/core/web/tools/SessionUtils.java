package com.ndood.agent.core.web.tools;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SessionUtils {

	/**
	 * 判断该url是否允许用户访问
	 */
	public boolean allowGuests(HttpServletRequest request) {
		String uri = request.getRequestURI();
		log.debug(uri + "允许游客访问");
		return true;
	}

	/**
	 * 判断当前用户是否登录
	 */
	public Boolean isLogin() {
		return SecurityContextHolder.getContext().getAuthentication() != null
				&& SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
	}

	/**
	 * 获取当前登录的用户
	 */
	public UserDetails getLoginUserInfo() {
		if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails) {
			UserDetails userInfo = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return userInfo;
		}
		return null;
	}

}
