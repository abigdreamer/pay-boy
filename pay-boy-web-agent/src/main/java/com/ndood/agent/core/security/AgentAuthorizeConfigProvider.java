package com.ndood.agent.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

import com.ndood.authenticate.core.authorize.AuthorizeConfigProvider;
import com.ndood.authenticate.core.properties.SecurityProperties;

/**
 * Admin模块相关的访问权限配置 只有demo模块才知道有这个配置；AuthorizeConfigManager会把所有的provider都装载起来
 * 确保rbac配置在最后生效
 */
@Component
@Order(Integer.MAX_VALUE)
public class AgentAuthorizeConfigProvider implements AuthorizeConfigProvider{

	@Autowired
	private SecurityProperties securityProperties;
	
	/**
	 * demo 模块的专属权限配置
	 */
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		
		// druid貌似不用配置就可以访问
		config
		.antMatchers("/static/**","/favicon**").permitAll().anyRequest().permitAll();
//		.antMatchers("/error/**").permitAll()
//		
//		.antMatchers(securityProperties.getBrowser().getSignInPage()).permitAll()
//		.antMatchers(securityProperties.getBrowser().getSignUpPage()).permitAll()
//		.antMatchers(securityProperties.getBrowser().getSignOutSuccessUrl()).permitAll()
//		
//		.antMatchers("/").authenticated()
//		.antMatchers("/index").authenticated()
//		.antMatchers("/center").authenticated()
//		.antMatchers("/jump_**").authenticated()
//		.antMatchers("/**/**/query").authenticated()
//		.antMatchers("/**/**/treeview").authenticated()
//		.antMatchers("/oss/aliyun/upload").authenticated();
		
		// 自定义授权表达式，处理rbac请求
//		config.anyRequest().access("@rbacPermissionHandler.hasPermission(request, authentication)");

		//.anyRequest().authenticated();
	}

}