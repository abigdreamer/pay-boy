package com.ndood.agent.core.security.handler;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ndood.agent.core.util.IpInfoUtil;
import com.ndood.agent.core.web.tools.SessionUtils;
import com.ndood.agent.dao.system.StaffDao;
import com.ndood.agent.pojo.system.StaffPo;
import com.ndood.agent.pojo.system.dto.StaffDto;
import com.ndood.authenticate.core.properties.CommonConstants;
import com.ndood.authenticate.core.support.SimpleResponse;
import com.ndood.common.base.util.DateUtil;

/**
 * 添加认证成功处理类，将日志插入数据库
 * @author ndood
 */
@Component("agentAuthenticationSuccessHandler")
public class AgentAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler{
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	//@Autowired
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Autowired
	private StaffDao staffDao;
	
	@Autowired
	private SessionUtils sessionUtils;
	
	/*@Autowired
	private SecurityProperties securityProperties;*/
	
	/**
	 * 认证成功处理
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		logger.info("登录成功");
		// Step1: 记录用户登录信息
		StaffDto staff = (StaffDto) sessionUtils.getLoginUserInfo();
		if(staff!=null) {
			StaffPo temp = new StaffPo();
			temp.setId(staff.getId());
			temp.setLastLoginIp(IpInfoUtil.getIpAddr(request));
			temp.setLastLoginTime(DateUtil.format(new Date(), "yyyy/MM/dd HH:mm:ss"));
			staffDao.updateByPrimaryKeySelective(temp);
		}
		
		// Step2: 处理登录成功业务
		if(isJson(request)||isAjax(request)) {
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(objectMapper.writeValueAsString(new SimpleResponse(CommonConstants.SUCCESS,"登录成功")));
			response.getWriter().flush();
			return;
		}
		// 单页应用不需要登录会跳到原来的地址，便于社交登录处理
		super.setRequestCache(new NullRequestCache());
		super.onAuthenticationSuccess(request, response, authentication);
	}
	
	/**
	 * 判断是否是json请求
	 */
	public Boolean isJson(HttpServletRequest request) {
		boolean isJson = request.getHeader("content-type") != null
				&& request.getHeader("content-type").contains("json");
		return isJson;
	}

	/**
	 * 判断是否是ajax请求
	 */
	public static boolean isAjax(HttpServletRequest httpRequest) {
		boolean isAjax = httpRequest.getHeader("X-Requested-With") != null
				&& "XMLHttpRequest".equals(httpRequest.getHeader("X-Requested-With").toString());
		return isAjax;
	}
}