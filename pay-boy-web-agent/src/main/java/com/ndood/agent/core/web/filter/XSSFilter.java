package com.ndood.agent.core.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.ndood.agent.core.security.xss.XSSRequestWrapper;

/**
 * xss过滤器
 */
/*@Component*/
public class XSSFilter implements Filter{
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("XSS过滤器启动...");
	}

	@Override
	public void destroy() {
	
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest)request;
		String method = httpServletRequest.getMethod();

		// 只对post请求进行过滤
		if("GET".equals(method)) {
			chain.doFilter(request, response);
		}else {
			chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
		}
		
	}
	
}