package com.ndood.agent.core.security.rbac;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.ndood.agent.pojo.system.dto.StaffDto;

/**
 * 实现RbacService
 * https://www.cnblogs.com/fenglan/p/5913463.html
 * 通用权限模块的2种模式：1 硬编码 2 动态加载
 * 硬编码方式（适用于bootstrap模式）：加载permission，权限注解拦截，权限标签控制显示
 * 动态加载模式（适用于前后端分离模式）：加载url, RbacServiceImpl拦截
 */
@Component("rbacPermissionHandler")
public class RbacPermissionHandler{

	/**
	 * 判断是否有权限
	 */
	public boolean hasPermission(HttpServletRequest request, Authentication authentication) {
		Object principal = authentication.getPrincipal();
		if(principal instanceof StaffDto){
			StaffDto staff = (StaffDto)principal;
			Map<String, GrantedAuthority> urlMap = staff.getUrlMap();
			String key = request.getRequestURI();
			if(StringUtils.isEmpty(key)) {
				return false;
			}
			if(urlMap.get(key)!=null) {
				return true;
			}
		}
		return false;
	}
	
}