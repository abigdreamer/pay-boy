package com.ndood.agent.core.exception;

import com.ndood.agent.core.constaints.AgentErrCode;

public class AgentException extends Exception {
	private static final long serialVersionUID = 13168926815908268L;

	private AgentErrCode code;

	public AgentErrCode getCode() {
		return code;
	}

	public void setCode(AgentErrCode code) {
		this.code = code;
	}

	public AgentException(String message) {
		super(message);
	}

	public AgentException(AgentErrCode code) {
        super(code.getValue());
        this.code = code;
    }
	
	public AgentException(AgentErrCode code, String message) {
        super(message);
        this.code = code;
    }

}