package com.ndood.agent.core.properties;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义属性配置入口 
 * @author ndood
 */
@Configuration
@EnableConfigurationProperties(AgentProperties.class)
public class AgentConfig {
	
}
