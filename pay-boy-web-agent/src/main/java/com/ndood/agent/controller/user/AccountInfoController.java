package com.ndood.agent.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.agent.pojo.comm.vo.AgentResultVo;
import com.ndood.agent.pojo.system.dto.StaffDto;
import com.ndood.agent.service.user.AccountInfoService;

/**
 * 账户信息管理
 */
@Controller
public class AccountInfoController {
	
	@Autowired
	private AccountInfoService accountInfoService;

	@GetMapping("/user/account/info")
	public String toAccountInfo(Model model) throws Exception{
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userId = userDetails.getUsername();
		StaffDto accountInfo = accountInfoService.getAccountInfoById(userId);
		model.addAttribute("accountInfo",accountInfo);
		return "user/account/info/account_info";
	}
	
	/**
	 * 修改用户
	 */
	@PostMapping("/user/account/info/update")
	@ResponseBody
	public AgentResultVo updateUser(@RequestBody StaffDto staff) throws Exception{
		accountInfoService.updateAccountInfo(staff);
		return AgentResultVo.ok().setMsg("修改账户成功！");
	}
	
	/**
	 * 添加用户
	 * @throws Exception 
	 */
	@PostMapping("/user/account/info/get")
	@ResponseBody
	public AgentResultVo getAccountInfo() throws Exception{
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userId = userDetails.getUsername();
		StaffDto accountInfo = accountInfoService.getAccountInfoById(userId);
		return AgentResultVo.ok().setData(accountInfo).setMsg("获取账户信息成功！");
	}
}
