define([
	'jquery',
	'jquery-extension',
	'layui',
	'bootstrap-datetimepicker',
	'bootstrap-datetimepicker-zh-CN',
	'bootstrap-table',
	'bootstrap-table-zh-CN',
	'bootstrap-table-mobile',
], function() {
	return {
		initPage: function(){
			
			// 1 初始化日期控件
			$('#log_start_date').datetimepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd hh:ii:00',
				autoclose: true
			});
			
			$('#log_end_date').datetimepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd hh:ii:00',
				autoclose: true
			});
			
			// 2 初始化表单控件
			$('#log_datatable').bootstrapTable({
				url : "/system/log/query", 
				method : 'post', 
				toolbar : '#log_toolbar',
				uniqueId: "id",
				striped: true,
				cache: false,
				pagination : true, 
				sortable: false,
				singleSelect : false,
				search: true,
				showColumns: true,
				showToggle:false,
				cardView: false,
				showRefresh: true,
				clickToSelect: true,
				minimumCountColumns: 5,
				queryParams : function(params) {
					return {
						limit : params.limit,
						offset : params.offset,
						keywords: params.search,
						startTime: $("#log_content").find("form").find("input[name='startTime']").val(),
						endTime: $("#log_content").find("form").find("input[name='endTime']").val(),
					};
				},
				sidePagination : "server",
				pageNumber:1, 
				pageSize : 10,
				pageList: [10, 20, 50],
				columns : [
					{
						field : 'recordNo',
						title : '编号'
					},
					{
						field : 'ip',
						title : 'IP地址'
					},
					{
						field : 'ipInfo',
						title : 'IP信息',
					},
					{
						field : 'requestType',
						title : '请求类型',
					},
					{
						field : 'requestUrl',
						title : '请求URL',
					},
					{
						visible: false,
						field : 'requestParam',
						title : '请求参数',
					},
					{
						field : 'staffName',
						title : '账户名',
					},
					{
						field : 'createTime',
						title : '创建时间'
					},
				]
			});
			
			// 3 初始化手机响应式
			var docW = window.innerWidth;
			if (docW < 768) {
				$("#log_search_plus").show();
				$("#log_search_plus_btn").hide();
				$("#log_content .bootstrap-table .fixed-table-toolbar input[type='text']").hide();
			}else{
				$("#log_search_plus").hide();
			}
		}
	}
});

//显示高级搜索
function log_search_plus(){
	var search = $('#log_datatable').parents('.bootstrap-table').find('.fixed-table-toolbar').find("input[type='text']");
	if(!$(search).is(":hidden")){
		// 隐藏普通搜索
		$(search).val('').hide();
		// 显示高级搜索
		$("#log_search_plus").show();
	}else{
		$("#log_search_plus").find("form")[0].reset();
		$("#log_search_plus").hide();
		$(search).show();
	}
}

//高级查询https://blog.csdn.net/lanyang123456/article/details/55805478
function log_search_submit(){
	var keywords = $("#log_content").find("form").find("input[name='username']").val();
	$('#log_datatable').bootstrapTable('refresh', {query:{keywords: keywords}});
}
