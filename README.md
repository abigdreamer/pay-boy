# 产品介绍

贝帛吱付 社区版 的目标是打造一款开源的、轻量级的、微服务化的、可公私有云部署的、可定制化的集成聚合支付和资金清结算于一体的四方支付系统，满足互联网企业业务系统的收款和资金处理等需求。

普通商户分支：平台二清模式，代理商分润，商户结算；商户直清模式，商户充值，平台手续费，代理商手续费

ISV服务商分支：平台即服务商，服务商分润，代理商分润，商户返佣

社区版：简化的功能和数据结构，单机部署的应用；包含普通商户分支

企业版(待定)：完整的功能和数据结构，单机+微服务部署的应用，免费的技术服务；包含普通商户分支，ISV服务商分支

<hr>

# 说明文档 

查看文档可以快速上手, [传送门](https://gitee.com/ndood/pay-boy/wikis/pages) <br>
企业版[前端闭源], [传送门](https://gitee.com/BABA-EC-F/pay-boy-cloud)

<hr>

# 近期开发计划

支付网关 :white_check_mark: <br>
渠道路由 :white_check_mark: <br>
收银台支付模拟 :white_check_mark: <br>
订单轮询 :white_check_mark: <br>
阶梯消息通知 :clock1: <br>
商城支付模拟 :clock1: <br>
记账 :white_check_mark: <br>
对账 :clock1: <br>
分润清算 :clock1: <br>
结算 :clock1: <br>
风控 :clock1: <br>

<hr>

# 业务结构

## 支付接口

支付 退款 查询 撤销 异步通知 充值 充值查询 充值撤销 

## 运营平台

配置 权限 支付流水 清算 结算 对账 商户 风控 渠道路由

## 商户平台

配置 权限 支付流水 清算 结算 对账 

## 商城后台

配置 权限 商品 库存 订单 充值订单 统计 

## 商城前台

商品展示 充值商品 购物车 我的账户 我的订单 

## 收银台

被扫 扫码 门店码 订单查询 退款 收银统计 

<hr>

# 技术选型

## 后端技术

springboot2.0，
springdata，
spring-security，
spring-oauth，
spring-social，
spring-cloud，
jooq，
mybatis，
redis，
mysql，
rabbitmq， 
docker

## 前端技术

bootstrap，
thymeleaf，
requirejs，
layui，
vue，
vuex，
vue-router，
vue-resource

## 其它技术
rsa2，
aes，
md5，
https，
jwt，
jsonrpc，
netty，
websocket

<hr>

# 效果展示

![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233234_5ef59f3c_754149.png "screen10.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/105904_35823032_754149.png "微信截图_20181227105702.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1109/111338_20005491_754149.png "screen11.png")

<hr>

# 交流、反馈、参与贡献

* QQ群：460395575
* E-mail：908304389@qq.com
* gitee地址：https://gitee.com/ndood/pay-boy