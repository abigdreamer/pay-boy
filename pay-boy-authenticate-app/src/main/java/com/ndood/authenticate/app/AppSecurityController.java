package com.ndood.authenticate.app;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import com.ndood.authenticate.app.social.AppSignUpUtils;
import com.ndood.authenticate.core.social.SocialController;
import com.ndood.authenticate.core.social.SocialUserInfo;

/**
 * app社交登录后的自定义singUpUrl处理。区别于静默方式注册
 */
@RestController
public class AppSecurityController extends SocialController {
	
	@Autowired
	private ProviderSignInUtils providerSignInUtils;
	
	@Autowired
	private AppSignUpUtils appSingUpUtils;
	
	/**
	 * 需要注册时跳到这里，返回401和用户信息给前端
	 */
	@GetMapping("/social/user")
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public SocialUserInfo getSocialUserInfo(HttpServletRequest request) {
		// 思考：首次注册的时候授权码模式和openid模式都是怎么把微信的信息放到session中的
		Connection<?> connection = providerSignInUtils.getConnectionFromSession(new ServletWebRequest(request));
		appSingUpUtils.saveConnectionData(new ServletWebRequest(request), connection.createData());
		return buildSocialUserInfo(connection);
	}

}