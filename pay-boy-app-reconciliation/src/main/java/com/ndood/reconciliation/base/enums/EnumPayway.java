package com.ndood.reconciliation.base.enums;

public enum EnumPayway {
	ALIPAY(1, "alipay"), 
	WECHAT(2, "wechat"), 
	UNION(3, "union"),
	INVALID(0, "invalid");
	
	// 成员变量
	private String name;
	private int code;

	// 构造方法
	private EnumPayway(int code, String name) {
		this.code = code;
		this.name = name;
	}

	// 普通方法
	public static String getName(int code) {
		for (EnumPayway c : EnumPayway.values()) {
			if (c.getCode() == code) {
				return c.name;
			}
		}
		return INVALID.name;
	}

	// get set 方法
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}