package com.ndood.reconciliation.base.dao.recon;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.ndood.reconciliation.domain.recon.entity.dto.ReconTradeRecordDto;

public interface ExtendReconTradeRecordDao {

	/**
	 * 1 根据时间区间和支付方式和appId获取所有的交易记录：支付/退款
	 * 模拟之前尽可能造好数据
	 */
	@Select("SELECT a.pay_way as payWay, a.mch_id as mchId, a.agentId as agentId, a.pay_order_no as tradeOrderNo, a.order_no as orderNo, a.trade_no as tradeNo, a.fee, "
			+ "1 as tradeType, a.app_id as appId, a.receipt_amount as amount, a.receipt_amount as realAmount, a.pay_time as tradeTime, a.status as tradeStatus, "
			+ "a.contract_id, a.contract_no, a.contract_type, a.contract_name, a.channel_id, a.channel_no, a.channel_name, a.product_id, a.product_name, "
			+ "a.mch_name, a.agent_name "
			+ "FROM r_rec_pay a "
			+ "WHERE a.create_time >= #{startTime} AND a.update_time < #{updateTime} AND a.pay_way = #{payWay} AND a.app_id = #{appId} "
			+ "UNION ALL "
			+ "SELECT b.pay_way as payWay, b.mch_id as mchId, b.agent_id as agentId, b.refund_order_no as tradeOrderNo, b.order_no as orderNo, b.trade_no as tradeNo, b.fee, "
			+ "b.contract_id, b.contract_no, b.contract_type, b.contract_name, b.channel_id, b.channel_no, b.channel_name, b.product_id, b.product_name, "
			+ "b.mch_name, b.agent_name "
			+ "2 as tradeType, b.app_id as appId, b.refund_amount as amount, b.receipt_refund_amount as realAmount, b.refund_time as tradeTime, b.status as tradeStatus "
			+ "FROM r_rec_refund b "
			+ "WHERE b.create_time >= #{startTime} AND b.update_time < #{updateTime} AND b.pay_way = #{payWay} AND b.app_id = #{appId} ")
	public List<ReconTradeRecordDto> extendFindAllTradeRecordListByTimeZoneAndPaywayAndAppid(Date startTime, Date endTime, Integer payWay, String appId);

	/**	
	 * 2 根据时间区间和支付方式获取所有的交易记录：支付/退款
	 */
	@Select("SELECT a.pay_way as payWay, a.mch_id as mchId, a.agentId as agentId, a.pay_order_no as tradeOrderNo, a.order_no as orderNo, a.trade_no as tradeNo, a.fee, "
			+ "1 as tradeType, a.app_id as appId, a.receipt_amount as amount, a.receipt_amount as realAmount, a.pay_time as tradeTime, a.status as tradeStatus, "
			+ "a.contract_id, a.contract_no, a.contract_type, a.contract_name, a.channel_id, a.channel_no, a.channel_name, a.product_id, a.product_name, "
			+ "a.mch_name, a.agent_name "
			+ "FROM r_rec_pay a "
			+ "WHERE a.create_time >= #{startTime} AND a.update_time < #{updateTime} AND a.pay_way = #{payWay} "
			+ "UNION ALL "
			+ "SELECT b.pay_way as payWay, b.mch_id as mchId, b.agent_id as agentId, b.refund_order_no as tradeOrderNo, b.order_no as orderNo, b.trade_no as tradeNo, b.fee, "
			+ "2 as tradeType, b.app_id as appId, b.refund_amount as amount, b.receipt_refund_amount as realAmount, b.refund_time as tradeTime, b.status as tradeStatus, "
			+ "b.contract_id, b.contract_no, b.contract_type, b.contract_name, b.channel_id, b.channel_no, b.channel_name, b.product_id, b.product_name, "
			+ "b.mch_name, b.agent_name "
			+ "FROM r_rec_refund b "
			+ "WHERE b.create_time >= #{startTime} AND b.update_time < #{updateTime} AND b.pay_way = #{payWay} ")
	public List<ReconTradeRecordDto> extendFindAllTradeRecordListByTimeZoneAndPayway(Date startTime, Date endTime,
			Integer payWay);

	/**
	 * 3 根据时间区间和支付方式获取所有的交易记录：支付/退款
	 */
	@Select("SELECT a.pay_way as payWay, a.mch_id as mchId, a.agentId as agentId, a.pay_order_no as tradeOrderNo, a.order_no as orderNo, a.trade_no as tradeNo, a.fee, "
			+ "1 as tradeType, a.app_id as appId, a.receipt_amount as amount, a.receipt_amount as realAmount, a.pay_time as tradeTime, a.status as tradeStatus, "
			+ "a.contract_id, a.contract_no, a.contract_type, a.contract_name, a.channel_id, a.channel_no, a.channel_name, a.product_id, a.product_name, "
			+ "a.mch_name, a.agent_name "
			+ "FROM r_rec_pay a "
			+ "WHERE a.status = 2 AND a.create_time >= #{startTime} AND a.update_time < #{updateTime} AND a.pay_way = #{payWay} ")
	public List<ReconTradeRecordDto> extendFindSuccessTradeRecordListByTimeZoneAndPayway(Date startTime, Date endTime,
			Integer payWay);

	
}
