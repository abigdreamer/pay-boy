package com.ndood.reconciliation.base.constaints;

public interface ReconConstaints {

	/**
	 * 对账批次状态
	 */
	Integer RECON_BATCH_STATUS_INIT = 1;
	Integer RECON_BATCH_STATUS_OK = 2;
	Integer RECON_BATCH_STATUS_FAILED = 3;
	
	/**
	 * 本地金额多
	 */
	String PLATFORM_OVER_CASH_MISMATCH = "p001";
	String PLATFORM_SHORT_CASH_MISMATCH = "p002";
	String PLATFORM_FEE_MISMATCH = "p003";
	String PLATFORM_MISS = "p004";
	
	/**
	 * 远程金额多
	 */
	String REMOTE_OVER_CASH_MISMATCH = "r001";
	String REMOTE_SHORT_CASH_MISMATCH = "r002";
	String REMOTE_FEE_MISMATCH = "r003";
	String REMOTE_STATUS_MISMATCH = "r004";
}
