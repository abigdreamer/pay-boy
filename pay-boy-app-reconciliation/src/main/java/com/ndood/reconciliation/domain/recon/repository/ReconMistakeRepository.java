package com.ndood.reconciliation.domain.recon.repository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ndood.common.base.dao.record.RMchRecReconMistakeMapper;
import com.ndood.common.base.pojo.record.RMchRecReconMistakePo;
import com.ndood.common.base.util.CodeUtil;
import com.ndood.reconciliation.domain.recon.entity.dto.ReconMistakeDto;

@Repository
public class ReconMistakeRepository {

	@Autowired
	private RMchRecReconMistakeMapper rMchRecReconMistakeMapper;
	
	/**
	 * 1 添加差错记录
	 */
	public void addMistake(ReconMistakeDto dto) {
		RMchRecReconMistakePo po = new RMchRecReconMistakePo();
		po.setId(dto.getId());
		po.setRemoteReceiptAmount(dto.getRemoteReceiptAmount());
		po.setRemoteTradeTime(dto.getRemoteTradeTime());
		po.setRemoteTradeStatus(dto.getRemoteTradeStatus());
		po.setRemoteTotalAmount(dto.getRemoteTotalAmount());
		po.setProductName(dto.getProductName());
		po.setTotalAmount(dto.getTotalAmount());
		po.setTradeTime(dto.getTradeTime());
		po.setRemoteFee(dto.getRemoteFee());
		po.setMchName(dto.getMchName());
		po.setTradeOrderNo(dto.getTradeOrderNo());
		po.setAgentId(dto.getAgentId());
		po.setRecordNo(dto.getRecordNo());
		po.setOrderNo(dto.getOrderNo());
		po.setPayWay(dto.getPayWay());
		po.setTradeNo(dto.getTradeNo());
		po.setTradeStatus(dto.getTradeStatus());
		po.setChannelNo(dto.getChannelNo());
		po.setAgentName(dto.getAgentName());
		po.setFee(dto.getFee());
		po.setMchId(dto.getMchId());
		po.setAppId(dto.getAppId());
		po.setPayType(dto.getPayType());
		po.setTradeType(dto.getTradeType());
		po.setReceiptAmount(dto.getReceiptAmount());
		po.setContractNo(dto.getContractNo());
		po.setProductNo(dto.getProductNo());
		po.setContractName(dto.getContractName());
		po.setChannelName(dto.getChannelName());
		po.setContractType(dto.getContractType());
		po.setChannelId(dto.getChannelId());
		po.setContractId(dto.getContractId());
		po.setCreateTime(new Date());
		po.setUpdateTime(new Date());
		rMchRecReconMistakeMapper.insert(po);
	}

	public void batchAddMistakes(List<ReconMistakeDto> mistakeList) {
		for (ReconMistakeDto dto : mistakeList) {
			addMistake(dto);
		}
	}
	
	public static void main(String[] args) {
		CodeUtil.printPoGetterDto(RMchRecReconMistakePo.class);
	}
	
}
