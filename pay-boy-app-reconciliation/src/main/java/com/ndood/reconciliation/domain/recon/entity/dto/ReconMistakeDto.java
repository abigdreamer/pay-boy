package com.ndood.reconciliation.domain.recon.entity.dto;

import com.ndood.reconciliation.domain.recon.entity.ReconMistakeDo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconMistakeDto extends ReconMistakeDo {

}
