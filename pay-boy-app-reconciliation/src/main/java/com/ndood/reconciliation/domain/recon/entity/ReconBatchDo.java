package com.ndood.reconciliation.domain.recon.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconBatchDo {
	private Long id;

    /**
     * 对账批次号
     */
    private String batchNo;

    /**
     * 对账日 2019-02-28
     */
    private String reconDay;

    /**
     * 对账类型：1 离线对账 2 在线对账
     */
    private Integer reconType;

    /**
     * 支付方式
     */
    private Integer payWay;

    /**
     * 交易金额
     */
    private BigDecimal tradeAmount;

    /**
     * 交易数量
     */
    private Integer tradeCount;

    /**
     * 退款金额
     */
    private BigDecimal refundAmount;

    /**
     * 退款数量
     */
    private Integer refundCount;

    /**
     * 远程交易额
     */
    private BigDecimal remoteTradeAmount;

    /**
     * 远程交易数量
     */
    private Integer remoteTradeCount;

    /**
     * 远程退款金额
     */
    private BigDecimal remoteRefundAmount;

    /**
     * 远程退款数量
     */
    private Integer remoteRefundCount;

    /**
     * 错误金额
     */
    private BigDecimal mistakeAmount;

    /**
     * 错误数
     */
    private Integer mistakeCount;

    /**
     * 远程不一致金额
     */
    private BigDecimal remoteMistakeAmount;

    /**
     * 远程不一致数
     */
    private Integer remoteMistakeCount;

    /**
     * 缓冲金额
     */
    private BigDecimal scratchAmount;

    /**
     * 缓冲数
     */
    private Integer scratchCount;

    /**
     * 对账单地址
     */
    private String orgCheckFilePath;

    /**
     * 对账文件存放地址
     */
    private String releaseCheckFilePath;

    /**
     * 文件解析状态： 1 待解析 2 已解析
     */
    private Integer releaseStatus;

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误描述
     */
    private String errorMsg;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态：1 已轧账 2 已对账 3 已平账
     */
    private Integer status;

    private Date createTime;

    private Date updateTime;
}
