package com.ndood.reconciliation.domain.recon.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ndood.common.base.dao.record.RMchRecReconBatchMapper;
import com.ndood.common.base.pojo.record.RMchRecReconBatchPo;
import com.ndood.common.base.util.CodeUtil;
import com.ndood.reconciliation.domain.recon.entity.dto.ReconBatchDto;

@Repository
public class ReconBatchRepository {
	
	@Autowired
	private RMchRecReconBatchMapper rMchRecReconBatchMapper;

	public void insertReconBatch(ReconBatchDto dto) {
		RMchRecReconBatchPo po = new RMchRecReconBatchPo();
		po.setRemoteTradeCount(dto.getRemoteTradeCount());
		po.setRemoteTradeAmount(dto.getRemoteTradeAmount());
		po.setRemoteRefundCount(dto.getRemoteRefundCount());
		po.setOrgCheckFilePath(dto.getOrgCheckFilePath());
		po.setRemoteRefundAmount(dto.getRemoteRefundAmount());
		po.setReleaseCheckFilePath(dto.getReleaseCheckFilePath());
		po.setMistakeAmount(dto.getMistakeAmount());
		po.setTradeAmount(dto.getTradeAmount());
		po.setRefundAmount(dto.getRefundAmount());
		po.setReconDay(dto.getReconDay());
		po.setTradeCount(dto.getTradeCount());
		po.setRefundCount(dto.getRefundCount());
		po.setBatchNo(dto.getBatchNo());
		po.setReconType(dto.getReconType());
		po.setPayWay(dto.getPayWay());
		po.setErrorMsg(dto.getErrorMsg());
		po.setMistakeCount(dto.getMistakeCount());
		po.setRemark(dto.getRemark());
		po.setScratchAmount(dto.getScratchAmount());
		po.setStatus(dto.getStatus());
		po.setErrorCode(dto.getErrorCode());
		po.setReleaseStatus(dto.getReleaseStatus());
		po.setScratchCount(dto.getScratchCount());

		po.setCreateTime(new Date());
		po.setUpdateTime(new Date());
		rMchRecReconBatchMapper.insert(po);
	}
	
	public void updateReconBatchById(ReconBatchDto dto) {
		RMchRecReconBatchPo po = new RMchRecReconBatchPo();
		po.setId(dto.getId());
		po.setRemoteTradeCount(dto.getRemoteTradeCount());
		po.setRemoteTradeAmount(dto.getRemoteTradeAmount());
		po.setRemoteRefundCount(dto.getRemoteRefundCount());
		po.setOrgCheckFilePath(dto.getOrgCheckFilePath());
		po.setRemoteRefundAmount(dto.getRemoteRefundAmount());
		po.setReleaseCheckFilePath(dto.getReleaseCheckFilePath());
		po.setMistakeAmount(dto.getMistakeAmount());
		po.setTradeAmount(dto.getTradeAmount());
		po.setRefundAmount(dto.getRefundAmount());
		po.setReconDay(dto.getReconDay());
		po.setTradeCount(dto.getTradeCount());
		po.setRefundCount(dto.getRefundCount());
		po.setBatchNo(dto.getBatchNo());
		po.setReconType(dto.getReconType());
		po.setPayWay(dto.getPayWay());
		po.setErrorMsg(dto.getErrorMsg());
		po.setMistakeCount(dto.getMistakeCount());
		po.setRemark(dto.getRemark());
		po.setScratchAmount(dto.getScratchAmount());
		po.setStatus(dto.getStatus());
		po.setErrorCode(dto.getErrorCode());
		po.setReleaseStatus(dto.getReleaseStatus());
		po.setScratchCount(dto.getScratchCount());
		
		po.setUpdateTime(new Date());
		rMchRecReconBatchMapper.insert(po);
	}
	
	public void updateReconBatchByBatchNo(ReconBatchDto dto) {
		RMchRecReconBatchPo po = new RMchRecReconBatchPo();
		po.setId(dto.getId());
		po.setRemoteTradeCount(dto.getRemoteTradeCount());
		po.setRemoteTradeAmount(dto.getRemoteTradeAmount());
		po.setRemoteRefundCount(dto.getRemoteRefundCount());
		po.setOrgCheckFilePath(dto.getOrgCheckFilePath());
		po.setRemoteRefundAmount(dto.getRemoteRefundAmount());
		po.setReleaseCheckFilePath(dto.getReleaseCheckFilePath());
		po.setMistakeAmount(dto.getMistakeAmount());
		po.setTradeAmount(dto.getTradeAmount());
		po.setRefundAmount(dto.getRefundAmount());
		po.setReconDay(dto.getReconDay());
		po.setTradeCount(dto.getTradeCount());
		po.setRefundCount(dto.getRefundCount());
		po.setBatchNo(dto.getBatchNo());
		po.setReconType(dto.getReconType());
		po.setPayWay(dto.getPayWay());
		po.setErrorMsg(dto.getErrorMsg());
		po.setMistakeCount(dto.getMistakeCount());
		po.setRemark(dto.getRemark());
		po.setScratchAmount(dto.getScratchAmount());
		po.setStatus(dto.getStatus());
		po.setErrorCode(dto.getErrorCode());
		po.setReleaseStatus(dto.getReleaseStatus());
		po.setScratchCount(dto.getScratchCount());
		
		po.setUpdateTime(new Date());
		UpdateWrapper<RMchRecReconBatchPo> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("batch_no", po.getBatchNo());
		rMchRecReconBatchMapper.update(po, updateWrapper);
	}
	
	public static void main(String[] args) {
		CodeUtil.printPoGetterDto(RMchRecReconBatchPo.class);
	}
	
}
