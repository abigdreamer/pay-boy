package com.ndood.reconciliation.domain.recon.repository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ndood.common.base.dao.record.RMchRecReconScratchPoolMapper;
import com.ndood.common.base.pojo.record.RMchRecReconScratchPoolPo;
import com.ndood.reconciliation.domain.recon.entity.dto.ReconScratchDto;

@Repository
public class ReconScratchRepository {
	
	@Autowired
	private RMchRecReconScratchPoolMapper rMchRecReconScratchPoolMapper;

	/**
	 * 删除某个时间前的数据
	 */
	public void deleteScratchBeforeDate(Date beforeDate) {
		UpdateWrapper<RMchRecReconScratchPoolPo> wrapper = new UpdateWrapper<>();
		wrapper.lt("gmt_created", beforeDate);
		rMchRecReconScratchPoolMapper.delete(wrapper);
	}

	/**
	 * 添加缓冲池记录
	 */
	public void addScratch(ReconScratchDto dto) {
		RMchRecReconScratchPoolPo po = new RMchRecReconScratchPoolPo();
		po.setReceiptAmount(dto.getReceiptAmount());
		po.setChannelName(dto.getChannelName());
		po.setTradeNo(dto.getTradeNo());
		po.setTradeTime(dto.getTradeTime());
		po.setPayWay(dto.getPayWay());
		po.setContractType(dto.getContractType());
		po.setProductNo(dto.getProductNo());
		po.setOrderNo(dto.getOrderNo());
		po.setTradeType(dto.getTradeType());
		po.setChannelNo(dto.getChannelNo());
		po.setContractName(dto.getContractName());
		po.setTotalAmount(dto.getTotalAmount());
		po.setAppId(dto.getAppId());
		po.setMchId(dto.getMchId());
		po.setFee(dto.getFee());
		po.setContractNo(dto.getContractNo());
		po.setProductName(dto.getProductName());
		po.setPayType(dto.getPayType());
		po.setChannelId(dto.getChannelId());
		po.setAgentName(dto.getAgentName());
		po.setRecordNo(dto.getRecordNo());
		po.setAgentId(dto.getAgentId());
		po.setContractId(dto.getContractId());
		po.setMchName(dto.getMchName());
		po.setTradeOrderNo(dto.getTradeOrderNo());
		po.setTradeStatus(dto.getTradeStatus());
		rMchRecReconScratchPoolMapper.insert(po);
	}
	
	/**
	 * 批量添加缓冲数据
	 */
	public void batchAddScratchs(List<ReconScratchDto> insertScratchList) {
		for (ReconScratchDto dto : insertScratchList) {
			addScratch(dto);
		}
	}

	/**
	 * 批量删除缓冲数据
	 */
	public void batchDeleteScratchs(List<ReconScratchDto> removeScratchList) {
		for (ReconScratchDto dto : removeScratchList) {
			UpdateWrapper<RMchRecReconScratchPoolPo> wrapper = new UpdateWrapper<>();
			wrapper.eq("record_no", dto.getRecordNo());
			rMchRecReconScratchPoolMapper.delete(wrapper);
		}
	}
	
}
