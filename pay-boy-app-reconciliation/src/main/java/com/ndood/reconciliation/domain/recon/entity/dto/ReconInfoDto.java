package com.ndood.reconciliation.domain.recon.entity.dto;
import com.ndood.reconciliation.base.enums.EnumPayway;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconInfoDto {
    
	/**
     * 打款类型，batch和realtime
     */
    private EnumPayway payway;
    
    /**
     * outBizId，外部业务id
     *          当paymentType是batch的时候，取batchNo
     *          当paymentType是realtime的时候，取dealerId
     */
    private String date;
}