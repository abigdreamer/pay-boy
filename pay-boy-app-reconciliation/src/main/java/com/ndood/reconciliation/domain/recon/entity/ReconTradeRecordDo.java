package com.ndood.reconciliation.domain.recon.entity;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReconTradeRecordDo {
	
	/**
	 * 支付方式
	 */
	private Integer payWay;
	/**
	 * 商户ID
	 */
	private Integer mchId;
	/**
	 * 代理ID
	 */
	private Integer agentId;
	/**
	 * 订单编号
	 */
	private String orderNo;
	/**
	 * 支付订单号或者退款订单号
	 */
	private String tradeOrderNo;
	
	/**
     * 交易号
     */
    private String tradeNo;

    /**
	 * 交易类型
	 */
	private Integer tradeType;
	/**
	 * appid
	 */
	private String appId;
	/**
	 * 金额
	 */
	private BigDecimal amount;
	/**
	 * 实收支付金额，退款金额
	 */
	private BigDecimal realAmount;
	/**
	 * 交易时间，退款时间
	 */
	private String tradeTime;
	/**
	 * 交易状态： 支付和退款不同
	 */
	private Integer tradeStatus;
	/**
	 * 合同类型
	 */
	private Integer contractType;
	/**
	 * 合同ID
	 */
	private Integer contractId;
	/**
	 * 合同编码
	 */
	private String contractNo;
	/**
	 * 合同名
	 */
	private String contractName;
	/**
	 * 渠道ID
	 */
	private Integer channelId;
	/**
	 * 渠道编码
	 */
	private String channelNo;
	/**
	 * 渠道名
	 */
	private String channelName;
	/**
	 * 支付类型： 支付 退款
	 */
	private Integer payType;
	/**
	 * 产品编码
	 */
	private String productNo;
	/**
	 * 产品名称
	 */
	private String productName;
	/**
	 * 商户名称
	 */
	private String mchName;
	/**
	 * 代理名称
	 */
	private String agentName;
	/**
	 * 服务费
	 */
	private BigDecimal Fee;
}
