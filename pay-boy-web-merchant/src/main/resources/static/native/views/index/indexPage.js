// https://www.kancloud.cn/yunye/axios/234845
define([
	'require',
	'jquery',
	'bootstrap',
	'text!./indexPage.html',
	'css!./indexPage.css',
	'layui',
	'scripts/utils/api',
	'scripts/utils/urls',
	'scripts/utils/cookie',
	'scripts/router/lazyload',
],function(require){

	var Template = require('text!./indexPage.html');
	var layer = require('layui');
	var api = require('scripts/utils/api');
	var urls = require('scripts/utils/urls');
	var cookie = require('scripts/utils/cookie');
	var lazyload = require('scripts/router/lazyload');
	
	var App = {
		data: function(){
			return {
				menuItemList:[
					{
						name: '主页',
						type: '0',
						url: '#',
						subList:[]
					},
					{
						name: '交易中心',
						type: '0',
						url: '#',
						subList:[
							{
								name: '资金账户',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '交易订单',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '对账管理',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '结算管理',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '二维码收款',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '用户列表',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '交易统计',
				    			type: '1',
				    			url: '#',
							},
						]
					},
					{
						name: '账户中心',
						type: '0',
						url: '#',
						subList:[
							{
								name: '商户资料',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '秘钥设置',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '支付渠道设置',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '基本信息',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '账号绑定',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '修改密码',
				    			type: '1',
				    			url: '#',
							},
						]
					},
					{
						name: '系统管理',
						type: '0',
						url: '#',
						subList:[
							{
								name: '角色管理',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '权限管理',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '门店管理',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '店员管理',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '在线店员',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '操作日志',
				    			type: '1',
				    			url: '#',
							},
						]
					},
					{
						name: '消息通知',
						type: '0',
						url: '#',
						subList:[
							{
								name: '平台维护公告',
				    			type: '1',
				    			url: '#',
							},
							{
								name: '商户通知',
				    			type: '1',
				    			url: '#',
							},
						]
					}
				]
			}
		},
		template: Template,
		mounted: function(){
			// Step1: 初始化jquery插件
			require(['require','domReady!', 'app', 'layout', 'quick-nav','scripts/utils/mutation']);
			cookie.setCookie('aaa',1);
			//alert(cookie.getCookie('aaa'));

			// vuex
			this.$store.commit('vApp/updateTest',{a:333});
			//alert(JSON.stringify(this.$store.state.vApp.test));
			
			// ajax請求獲取數據
			api.postHttp({
				url: urls.test,
				params: {id:1}, 
				success: function(res){
					alert(JSON.stringify(res.data))
				},
				fail: function(res){
					alert(JSON.stringify(res))
				}
			});
        },
        methods: {
	    	showMsgFromChild:function(msg){
	    		layer.alert(msg);
	    	}
	    },
	};
	return App;
});