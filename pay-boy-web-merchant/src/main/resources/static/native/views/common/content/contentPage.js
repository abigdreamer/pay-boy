// https://blog.csdn.net/aitangyong/article/details/40708025
define([
	'require',
	'jquery',
	'bootstrap',
	'text!./contentPage.html',
	'css!./contentPage.css'
],function(require){
	var Template = require('text!./contentPage.html');
	var App = {
		data: function(){
			return {};
		},
		template: Template,
		mounted: function(){},
        methods: {},
	};
	return App
});