/*
 * https://segmentfault.com/a/1190000002403806
 * https://requirejs.org/docs/api.html
 * https://www.cnblogs.com/lhb25/p/requirejs-ptimizer-using.html
 * https://www.cnblogs.com/taoquns/p/5960415.html
 * http://www.51xuediannao.com/javascript/vuejs_vuex_requirejs.html
 * https://www.cnblogs.com/cqhaibin/p/6823286.html
 * https://www.cnblogs.com/anrainie/p/8521085.html
*/
({
	appDir: './', // app顶级目录
	baseUrl: './',
	dir: '../build', // 文件输出顶级目录
	// optimize: "uglify",
	optimizeCss: "standard",
	optimizeJs: "standard",
	mainConfigFile: './require.config.js',
	removeCombined: false,
	preserveLicenseComments: true,
	inlineText: true,
	fileExclusionRegExp: /^(r|c|build)\.js|run.bat$/,
	modules: [],
	// 配置cdn支持
	paths:{
		'jquery':'empty:',
		'jquery-lazyload':'empty:',
		'jquery-validation':'empty:',
		'bootstrap':'empty:',
		'bootstrap-datepicker':'empty:',
		'bootstrap-datepicker-zh-CN':'empty:',
		'bootstrap-datetimepicker':'empty:',
		'bootstrap-datetimepicker-zh-CN':'empty:',
		'bootstrap-table':'empty:',
		'bootstrap-table-mobile':'empty:',
		'bootstrap-table-zh-CN':'empty:',
		'vue': 'empty:',
		'vue-router': 'empty:',
		'vue-resource': 'empty:',
		'vuex': 'empty:',
		'domReady':'empty:',
		'text': 'empty:',
		'vue-i18n':'empty:'
	},
})