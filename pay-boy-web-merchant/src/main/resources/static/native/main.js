//3 UI主入口,加载各种依赖
define([
	'require',
	'vue',
	'scripts/router/router',
	'scripts/vuex/store',
	'vue-resource',
	'scripts/utils/api',
	'component/menu',
	'scripts/router/lazyload',
],function (require) {
	var Vue = require('vue');
	var VueRouter = require('scripts/router/router');
	var VueStore = require('scripts/vuex/store');
	var api = require('scripts/utils/api');
	var menu = require('component/menu');
	var lazyload = require('scripts/router/lazyload');
	
	// https://cn.vuejs.org/v2/guide/components-registration.html
	Vue.component('menu-item',menu)
	
	//初始化layui ext.js 使用requirejs扩展需要加代码
	// https://www.cnblogs.com/jiangxiaobo/p/5276482.html
	require(['layui'],function(){
		layer.config({
		    path: '/static/lib/layer/'
		});
		layer.config({extend: 'extend/layer.ext.js'});
	})
	
	var App = new Vue({
		router: VueRouter,   //路由挂载
		store: VueStore,     //状态挂载
		el: "#app",       //挂载点是index.html 中的 id="#app"的元素
		created: function(){
			
			lazyload.loadRoute({
				obj: this,
				routes: [
					{
						path:'/', 
						pValue: 'views/index/indexPage', 
						redirect: 'content',
						children:[
							{
								path:'/content', 
								pValue: 'views/common/content/contentPage',
							},
						],
					},
					{
						path: '/login',
			    		pValue: 'views/login/loginPage',
					}
				],
				callback: function(obj){}
			});
			
		}
	});
	
    return App;
});