// https://blog.csdn.net/yiifaa/article/details/53396274
// https://blog.csdn.net/aitangyong/article/details/40708025
define(['require'],function(require){

	// 递归获取require组件列表
	var func1 = function(routes,paths){
		for(var i=0; i < routes.length; i++){
			if(routes[i].pValue){
				paths.push(routes[i].pValue);
			}
			if(routes[i].children){
				func1(routes[i].children, paths);
			}
		}
	}
	
	// 递归装载组件
	var func2 = function(routes){
		for(var i=0; i < routes.length; i++){
			if(routes[i].pValue){
				routes[i].component = require(routes[i].pValue);
			}
			if(routes[i].children){
				func2(routes[i].children);
			}
		}
	}
	
	// 懒加载路由
	var loadRoute = function(option){
		var {obj, routes, callback} = option;
		// Step1: 从树装结构中提取所有的 pValue,组成数组
		var paths = [];
		func1(routes, paths);
		
		// Step2: 同步加载路由
		require(paths, function(){
			func2(routes);
			obj.$router.addRoutes(routes);
			callback(obj);
		});
	}
	
	return {
		loadRoute: loadRoute,
	};
	
})