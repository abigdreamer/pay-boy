define(['vue',
	'vue-resource',
	'scripts/utils/cookie',
	'scripts/utils/storage',
	'layui',
],function(Vue,VueResource,VueRouter,cookie,storage,layui){
	// 全局使用vueVueResource
	Vue.use(VueResource);  
	
	// vue-resource请求拦截
	Vue.http.interceptors.push(function (req, next) {
	   next(function (res) {

		   switch (res.status) {
		   case 401:
			   // 未登录 清除已登录状态
			   cookie.setCookie('userInfo', '');
			   storage.setStore('accessToken', '');
			   if (VueRouter.history.current.name != "login") {
				   if (res.data.msg !== null) {
					   layer.alert(res.data.msg, {icon: 2});
				   } else {
					   layer.alert("未知错误，请重新登录");
				   }
				   VueRouter.push('/login');
			   }
			   break;
		   case 403:
			   // 没有权限
			   if (res.data.msg !== null) {
				   layer.alert(res.data.msg, {icon: 2});
			   } else {
				   layer.alert("未知错误");
			   }
			   break;
		   case 500:
			   // 错误
			   if (res.data.msg !== null) {
				   layer.alert(res.data.msg, {icon: 2});
			   } else {
				   layer.alert("未知错误", {icon: 2});
			   }
			   break;
		   default:
			   return res;
		   }
	   })
	});
	
	// https://www.cnblogs.com/chenhuichao/p/8308993.html
	var App = {
		getHttp: function(options){
			var {url,params,success,fail} = options;
			Vue.http.get(url,
				{
					params: param,
				}
			).then(success,fail);
		},
		postHttp: function(options){
			var {url,params,success,fail} = options;
			Vue.http.post(
				url,
				JSON.stringify(params),
				{}
			).then(success,fail);
		}
	}
	return App;
})