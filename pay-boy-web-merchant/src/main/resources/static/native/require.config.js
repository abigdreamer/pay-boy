/**
 * require.js总配置类
 */
requirejs.config({
	baseUrl: './static',
    waitSeconds: 15,
    charset: 'utf-8',
	//urlArgs: "bust=" + (new Date()).getTime(), // 记得部署到生产环境的时候移除掉
	urlArgs: 'v=20181127',
	map: {
        '*': {
            'css': 'lib/require/css'
        }
    },
	paths: {
		'jquery': [
			'https://cdn.bootcss.com/jquery/1.12.4/jquery.min',
			'lib/jquery.min',
		],
		'jquery-lazyload': [
			'https://cdnjs.cloudflare.com/ajax/libs/jquery_lazyload/1.9.3/jquery.lazyload',
			'lib/jquery.lazyload.min',
		],
		'jquery-extension': 'lib/jquery.extension',
		'jquery-validation': [
			'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min',
			'lib/jquery-validation/js/jquery.validate.min',
		],
		'jquery-distselector': 'lib/jquery-distselector/jquery-distselector',
		'bootstrap': [
			'https://cdn.bootcss.com/twitter-bootstrap/3.3.6/js/bootstrap.min',
			'lib/bootstrap/js/bootstrap.min',
		],
		'bootstrap-datepicker': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min',
			'lib/bootstrap-datepicker/js/bootstrap-datepicker.min',
		],
		'bootstrap-datepicker-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.zh-CN.min',
			'lib/bootstrap-datepicker/js/bootstrap-datepicker.zh-CN',
		],
		'bootstrap-datetimepicker': [
			'https://cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.4.4/js/bootstrap-datetimepicker.min',
			'lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
		],
		'bootstrap-datetimepicker-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.4.4/js/locales/bootstrap-datetimepicker.zh-CN',
			'lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.zh-CN',
		],
		'bootstrap-table': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min',
			'lib/bootstrap-table/js/bootstrap-table',
		],
		'bootstrap-table-mobile': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/extensions/mobile/bootstrap-table-mobile.min',
			'lib/bootstrap-table/js/bootstrap-table-mobile',
		],
		'bootstrap-table-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min',
			'lib/bootstrap-table/js/bootstrap-table-zh-CN',
		],
		'bootstrap-imguploader': 'lib/bootstrap-imguploader/bootstrap-imguploader',
		'layui': 'lib/layer/layer',
		'app': 'scripts/global/app',
		'layout': 'scripts/global/layout',
		'quick-nav': 'scripts/global/quick-nav',
		'vue': [
			'https://cdn.bootcss.com/vue/2.5.17-beta.0/vue.min',
			'lib/vue/vue.min'
		],
		'vue-router':[
			'https://cdn.bootcss.com/vue-router/3.0.1/vue-router.min',
			'lib/vue/vue-router.min'
		],
		'vue-resource':[
			'https://cdn.bootcss.com/vue-resource/1.5.1/vue-resource.min',
			'lib/vue/vue-resource.min'
		],
		'vuex':[
			'https://cdn.bootcss.com/vuex/3.1.0/vuex.min',
			'lib/vue/vuex.min'
		],
		'vue-i18n':[
			'lib/vue/vue-i18n',
		],
		'domReady':[
			'https://cdn.bootcss.com/require-domReady/2.0.1/domReady.min',
			'lib/domReady.min',
		],
		'text':[
			'https://cdn.bootcss.com/require-text/2.0.12/text.min',
			'lib/text.min',
		],
		// babel
		'es6':[
			'lib/es6-babel/es6',
		],
		'babel':[
			'lib/es6-babel/babel-5.8.34.min',
		]
	},
	// babel
	config: {
		'es6': {
			'resolveModuleSource': function(source) {
				return 'es6!'+source;
			}
		}
	},
	pragmasOnSave: {
        'excludeBabel': true
    },
	exclude: ['babel'],
	
	shim: {
		'jquery-lazyload': ['jquery'],
		'jquery-validation': {
			deps: ['jquery'],
			exports: '$.validator'
		},
		'jquery-extension': {
			deps: ['jquery','jquery-validation'],
		},
		'bootstrap': ['jquery'],
		'bootstrap-table': {
			deps: ['jquery','bootstrap'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-table-zh-CN': {
			deps: ['jquery','bootstrap-table'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-table-mobile': {
			deps: ['jquery','bootstrap-table'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-datepicker': ['jquery','bootstrap'],
		'bootstrap-datepicker-zh-CN': ['jquery','bootstrap-datepicker'],
		'bootstrap-datetimepicker': ['jquery','bootstrap'],
		'bootstrap-datetimepicker-zh-CN': ['jquery','bootstrap-datetimepicker'],
		'bootstrap-imguploader':['jquery'],
		'layui':['jquery'],
		'app':['jquery','bootstrap','css!asserts/css/index.css'],
		'layout':['jquery','app'],
		'quick-nav':['jquery','app'],
		'vue': {
			exports: 'Vue'
		},
	},
	packages: [
    	{
    		name: 'asserts',
    		location: './asserts',
    	},
    	{
    		name : "component",
    		location :"./components",
    	},
    	{
            name: 'lib',
            location: './lib',
        },
        {
            name: 'scripts',
            location: './scripts',
        },
        {
    		name: 'views',
    		location: './views',
    	}
    ],
    // 默认要加载的js
    deps: ['jquery','jquery-validation','jquery-extension','layui','bootstrap-table','vue','vuex','vue-router','vue-resource','vue-i18n',
		'bootstrap-table-zh-CN','bootstrap-table-mobile','bootstrap-datepicker','bootstrap-datetimepicker'],
});
