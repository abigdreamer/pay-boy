package com.ndood.merchant.core.security;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.ndood.common.base.util.JPAUtil;
import com.ndood.common.base.util.RegexUtils;
import com.ndood.merchant.dao.PermissionRepository;
import com.ndood.merchant.dao.RoleRepository;
import com.ndood.merchant.dao.UserRepository;
import com.ndood.merchant.jooq.tables.pojos.TPermission;
import com.ndood.merchant.jooq.tables.pojos.TRole;
import com.ndood.merchant.jooq.tables.pojos.TUser;
import com.ndood.merchant.pojo.merchant.dto.UserDto;

/**
 * 自定义UserDetailsService
 * @author ndood
 */
@Transactional
@Component
public class MerchantUserDetailsService implements UserDetailsService, SocialUserDetailsService {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PermissionRepository permissionRepository;
	
	/**
	 * 根据username获取用户信息
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		logger.debug("Step1: 进行登录，表单登录用户名:" + username);

		TUser user = null;
		if (RegexUtils.checkEmail(username)) {
			
			// 查看是否存在未激活邮箱
			user = userRepository.findByEmailAndEmailStatus(username, 0);
			if(user!=null) {
				throw new UsernameNotFoundException("邮箱未激活，请先到收件箱激活后再试!");
			}
			
			user = userRepository.findByEmailAndEmailStatus(username, 1);
			
		} else if (RegexUtils.checkMobile(username)) {
			user = userRepository.findByMobile(username);
			
		} else {
			// 适用于rememeberMe的调用
			user = userRepository.findById(Integer.parseInt(username));
			if(user==null) {
				throw new UsernameNotFoundException("用户不存在!");
			}
		}

		if (user == null) {
			throw new UsernameNotFoundException("用户不存在!");
		}

		logger.debug("Step2: 为登录用户设置权限");
		return buildUserAuthorities(user);
		
	}

	/**
	 * 根据userId获取社交用户信息 静默方式注册的时候 findBySocialUserId TUser getUserId socialUserId
	 * 非静默方式注册的时候findByUsername TUser getUserId username
	 */
	@Override
	public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
		logger.info("社交登录用户ID:" + userId);
		TUser user = userRepository.findById(Integer.valueOf(userId));
		if (user==null) {
			throw new UsernameNotFoundException("用户不存在!");
		}
		return buildUserAuthorities(user);
	}

	/**
	 * 创建用户，并给用户分配角色
	 */
	private SocialUserDetails buildUserAuthorities(TUser user) {
		Map<String, GrantedAuthority> urlMap = Maps.newHashMap();
		// 将权限url信息加入到urls
		List<TRole> roles = roleRepository.findRolesByUserId(user.getId());
		if (roles != null) {
			for (TRole role : roles) {
				List<TPermission> permissions = permissionRepository.findPermissionsByRoleId(role.getId());
				for (TPermission TPermission : permissions) {
					String url = TPermission.getUrl();
					if (StringUtils.isEmpty(url)) {
						continue;
					}
					url = url.trim();
					urlMap.put(url, new SimpleGrantedAuthority(url));
				}
			}
		}
		try {
			UserDto dto = new UserDto();
			JPAUtil.fatherToChild(user, dto);
			return dto;
		} catch (Exception e) {
			logger.error("字段映射失败，返回空值",e);
			return null;
		}
	}
}
