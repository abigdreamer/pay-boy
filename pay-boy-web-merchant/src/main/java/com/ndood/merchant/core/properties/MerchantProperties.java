package com.ndood.merchant.core.properties;
/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * ndood admin配置主类
 * @author ndood
 */
@Data
@ConfigurationProperties(prefix = "ndood.merchant")
public class MerchantProperties {
	/**
	 * 系统默认域名
	 */
	private String domain = "http://pay.jeepupil.top";
	/**
	 * cookie默认的sessionid名称，防止同一个域名下多个应用jsessionid冲突问题
	 */
	private String defaultCookieName = "JSESSIONID";
	/**
	 * 阿里云通信相关配置
	 */
	private AliyunSmsProperties dayu = new AliyunSmsProperties();
	/**
	 * 阿里云OSS相关配置
	 */
	private AliyunOssProperties oss = new AliyunOssProperties();
	/**
	 * 静态资源相关配置
	 */
	private StaticResourceProperties stat = new StaticResourceProperties();
	/**
	 * 邮件相关配置
	 */
	private MerchantEmailProperties email = new MerchantEmailProperties();
}