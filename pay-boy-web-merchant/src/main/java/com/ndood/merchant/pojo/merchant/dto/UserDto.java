package com.ndood.merchant.pojo.merchant.dto;

import java.util.Collection;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.security.SocialUserDetails;

import com.ndood.merchant.jooq.tables.pojos.TUser;

public class UserDto extends TUser implements UserDetails, SocialUserDetails{

	private static final long serialVersionUID = -4445043922693965480L;

	@Override
	public String getUserId() {
		return String.valueOf(super.getId());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getUsername() {
		return String.valueOf(super.getId());
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return false;
	}

	public Map<String, GrantedAuthority> getUrlMap() {
		// TODO Auto-generated method stub
		return null;
	}

}
