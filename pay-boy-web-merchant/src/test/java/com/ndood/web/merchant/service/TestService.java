package com.ndood.web.merchant.service;

import java.util.List;

import javax.transaction.Transactional;

import org.jooq.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.ndood.merchant.jooq.tables.daos.TJobDao;
import com.ndood.merchant.jooq.tables.pojos.TJob;

/**
 * 
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestService {

	/*
    @Autowired
    private DSLContext dsl;*/
	
    @Autowired
    Configuration jooqConfig;
	
    @Autowired
    private TJobDao jbDao;

    @Test
    @Rollback
    @Transactional
    public void testFind() {
    	System.out.println(jooqConfig);
    	List<TJob> list = jbDao.fetchByJobName("welcomJob");
    	if(list.isEmpty()) {
    		System.out.println("return null");
    	}
    	System.out.println(new Gson().toJson(list.get(0)));
    	// jbDao.insert(list);
    }
}
